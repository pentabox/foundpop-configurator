const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");
const appName = "foundpop-configurator.js";

module.exports = {
    mode: "production",
    entry: "./src/index.ts",

    output: {
        path: path.resolve(__dirname, "dist"),
        filename: appName
    },

    resolve: {
        extensions: [".ts", ".tsx", ".js"]
    },

    module: {
        rules: [
            {
                test: /\.tsx?/,
                use: "ts-loader",
                exclude: /node_modules/
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    "style-loader",
                    "css-loader",
                    "sass-loader"
                ]
            }
        ]
    },

    context: path.join(__dirname, "."),
    plugins: [
        new HtmlWebpackPlugin({
            template: 'public/template.html'
        }),
        new webpack.ProvidePlugin({
            'earcut': 'earcut'
        }),
        new webpack.ProvidePlugin({
            Buffer: ['buffer', 'Buffer']
        }),
    ]
};