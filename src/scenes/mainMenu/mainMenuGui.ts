import {SceneGUI} from "../../gui/sceneGui";
import {Button, Container} from "@babylonjs/gui";
import {SceneBase} from "../sceneBase";
import {MainMenuScene} from "./mainMenuScene";
import {FoundPopConfigurator} from "../../index";
import { Strings } from "../../utils/strings";

export class MainMenuGui extends SceneGUI {
    private _btn: Button;

    constructor(scene: SceneBase, _app: FoundPopConfigurator) {
        super(scene, _app);

        this._container.left = "0";
        this._container.width = "100%";
        this._container.height = "100%";
        this._container.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_CENTER;
        this._container.verticalAlignment = Container.VERTICAL_ALIGNMENT_CENTER;

        const btn = Button.CreateSimpleButton("StartBtn", Strings.START);
        btn.height = "75px";
        btn.color = "black";
        btn.background = "white";
        btn.thickness = 4;
        btn.width = "300px";
        btn.horizontalAlignment = Button.HORIZONTAL_ALIGNMENT_CENTER;
        btn.verticalAlignment = Button.VERTICAL_ALIGNMENT_CENTER;

        if (btn.textBlock)
        {
            btn.textBlock.color = "black";
            btn.textBlock.fontSize = "33px";
            btn.textBlock.fontFamily = "BasisGrotesquePro";
        }

        btn.onPointerClickObservable.add(() => {
            this._onStart();
        });
        btn.onPointerEnterObservable.add(() => {
            this._onEnter();
        });
        btn.onPointerOutObservable.add(() => {
            this._onExit();
        });

        this._btn = btn;
        this._container.addControl(btn);
        btn.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_CENTER;
        btn.verticalAlignment = Container.VERTICAL_ALIGNMENT_CENTER;
    }

    private _onStart() {
        this.resetCursor();

        const mainScene = this._scene as MainMenuScene;
        if (mainScene === null) return;

        mainScene.onStart();
    }

    private _onEnter() {
        this._btn.background = "black";
        this._btn.thickness = 0;
        this._btn.color = "white";

        if (this._btn.textBlock)
            this._btn.textBlock.color = "white";

        this.setCursor("pointer");
    }

    private _onExit() {
        this._btn.background = "white";
        this._btn.thickness = 5;
        this._btn.color = "black";

        if (this._btn.textBlock)
            this._btn.textBlock.color = "black";

        this.resetCursor();
    }
}