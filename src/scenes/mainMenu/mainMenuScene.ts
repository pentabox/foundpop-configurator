import {Engine} from "@babylonjs/core";
import {SceneBase, States} from "../sceneBase";
import {SceneGUI} from "../../gui/sceneGui";
import {MainMenuGui} from "./mainMenuGui";
import {FoundPopConfigurator} from "../../index";

export class MainMenuScene extends SceneBase {
    constructor(_engine : Engine, _app: FoundPopConfigurator) {
        super(_engine, _app);
    }

    protected _loadScene() {
        super._loadScene();

        this._camera.detachControl();
    }

    getSceneGui(): SceneGUI | null {
        return new MainMenuGui(this, this._app);
    }

    public onStart() {
        this._app.goToState(States.ROOM_MENU);
    }

    public hasSideMenu() : boolean {
        return false;
    }
}