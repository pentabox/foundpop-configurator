import { Container, TextBlock } from "@babylonjs/gui";
import { FoundPopConfigurator } from "../..";
import { SceneGUI } from "../../gui/sceneGui";
import { Strings } from "../../utils/strings";
import { SceneBase } from "../sceneBase";

export class NotSupportedSceneGui extends SceneGUI {
    constructor(scene: SceneBase, _app: FoundPopConfigurator) {
        super(scene, _app);

        this._container.left = "0%";
        this._container.width = "100%";
        this._container.height = "100%";
        this._container.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_CENTER;
        this._container.verticalAlignment = Container.VERTICAL_ALIGNMENT_CENTER;
        this._container.zIndex = 9999;
        this._container.background = "white";

        const notSupportedText = new TextBlock("NotSupportedText", Strings.NOT_SUPPORTED);
        notSupportedText.color = "black";
        notSupportedText.fontFamily = "BasisGrotesquePro";
        notSupportedText.fontSizeInPixels = 65;
        notSupportedText.width = "100%";
        notSupportedText.height = "100%";
        notSupportedText.textWrapping = true;

        this._container.addControl(notSupportedText);
    }
}