import {Engine} from "@babylonjs/core";
import {SceneBase, States} from "../sceneBase";
import {SceneGUI} from "../../gui/sceneGui";
import {FoundPopConfigurator} from "../../index";
import { NotSupportedSceneGui } from "./notSupportedSceneGui";

export class NotSupportedScene extends SceneBase {
    constructor(_engine : Engine, _app: FoundPopConfigurator) {
        super(_engine, _app);
    }

    protected _loadScene() {
        super._loadScene();

        this._camera.detachControl();
    }

    getSceneGui(): SceneGUI | null {
        return new NotSupportedSceneGui(this, this._app);
    }

    public hasSideMenu() : boolean {
        return false;
    }

    public hasTopMenu(): boolean {
        return false;
    }
}