import {SceneGUI} from "../../gui/sceneGui";
import {Button, Container, Control, Grid, Image, InputText, Rectangle, StackPanel, TextBlock} from "@babylonjs/gui";
import {SceneBase} from "../sceneBase";
import {RoomShape} from "../../models/roomShape";
import {RoomMenuScene} from "./roomMenuScene";
import {Strings} from "../../utils/strings";
import {RoomShapes} from "../../data/roomShapes";
import {FoundPopConfigurator} from "../../index";
import { ModalWindow } from "../../gui/modalWindow";
import { PlanManager } from "../../utils/planManager";

export class RoomMenuGui extends SceneGUI {
    private _grid1: Grid | undefined;
    private _grid2: Grid | undefined;
    private _gridStack: Container | undefined;
    private _shapeButtons: Button[] = [];

    private _planModal: ModalWindow | undefined;

    constructor(scene: SceneBase, _app: FoundPopConfigurator) {
        super(scene, _app);

        this._container.left = "0";
        this._container.width = "1200px";
        this._container.height = "100%";
        this._container.horizontalAlignment = StackPanel.HORIZONTAL_ALIGNMENT_CENTER;
        this._container.verticalAlignment = StackPanel.VERTICAL_ALIGNMENT_CENTER;
        //this._container.isVertical = false;

        const roomStack = new Container("RoomStack");
        roomStack.width = "100%";
        roomStack.height = "80%";

        this._container.addControl(roomStack);
        roomStack.horizontalAlignment = StackPanel.HORIZONTAL_ALIGNMENT_CENTER;
        roomStack.verticalAlignment = StackPanel.VERTICAL_ALIGNMENT_CENTER;

        this._createPickRoomText("0", roomStack);
        this._createGrid("15%", roomStack);
        this._createPlanNumber("90%", roomStack);

        this._loadRoomShapeButtons();
        this._createPlanModal();
    }

    private _createPlanModal() {
        const info = new TextBlock("InfoBlock", Strings.ENTER_VALID_PLAN_NUMBER);
        info.fontFamily = "BasisGrotesquePro";
        info.fontSize = "30px";
        info.color = "white";

        const cnt = new Container("InfoCnt");
        cnt.addControl(info);

        this._planModal = new ModalWindow(cnt, this._app, this._app.Gui!.GuiTexture);
        this._planModal.hide();
        this._planModal.addCenterButton(Strings.OK, () => {
            this._planModal?.hide();
        });
    }

    private _createPickRoomText(top: string, parent: Container) {
        const pickRoomText = new TextBlock("PickRoomText", Strings.PICK_ROOM_SHAPE);
        pickRoomText.fontFamily = "BasisGrotesquePro";
        pickRoomText.fontSize = "35px";
        pickRoomText.height = "15%"
        pickRoomText.top = top;

        parent.addControl(pickRoomText);
        pickRoomText.verticalAlignment = Container.VERTICAL_ALIGNMENT_TOP;
    }

    private _createGridBackground() {
        const bgLight = new Rectangle("BackgroundLight");
        this._gridStack?.addControl(bgLight);
        bgLight.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_CENTER;
        bgLight.verticalAlignment = Container.VERTICAL_ALIGNMENT_CENTER;

        bgLight.width = "100%";
        bgLight.height = "100%";
        bgLight.background = "#C7D0D8";

        const bgWhite = new Rectangle("BackgroundWhite");
        this._gridStack?.addControl(bgWhite);
        bgWhite.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_CENTER;
        bgWhite.verticalAlignment = Container.VERTICAL_ALIGNMENT_CENTER;

        bgWhite.width = "100%";
        bgWhite.height = "100%";
        bgWhite.background = "white";
        
        const padding = 8;
        bgWhite.paddingBottomInPixels = padding;
        bgWhite.paddingLeftInPixels = padding;
        bgWhite.paddingRightInPixels = padding;
        bgWhite.paddingTopInPixels = padding;
    }

    private _createGrid(top: string, parent: Container) {
        this._gridStack = new Container("GridStack");
        this._gridStack.width = "80%";
        this._gridStack.height = "70%";

        parent.addControl(this._gridStack);
        this._gridStack.top = top;
        this._gridStack.verticalAlignment = Container.VERTICAL_ALIGNMENT_TOP;

        this._createGridBackground();

        this._grid1 = new Grid("RoomShapeGrid1");
        this._grid1.width = "100%";
        this._grid1.height = "50%";

        this._grid1.addColumnDefinition(1 / 3);
        this._grid1.addColumnDefinition(1 / 3);
        this._grid1.addColumnDefinition(1 / 3);

        this._grid1.addRowDefinition(1);

        this._gridStack.addControl(this._grid1);
        this._grid1.verticalAlignment = Container.VERTICAL_ALIGNMENT_TOP;
        this._grid1.paddingLeftInPixels = 8;
        this._grid1.paddingRightInPixels = 7;
        this._grid1.paddingTopInPixels = 8;

        this._grid2 = new Grid("RoomShapeGrid2");
        this._grid2.width = "100%";
        this._grid2.height = "50%";

        this._grid2.addColumnDefinition(0.25);
        this._grid2.addColumnDefinition(0.25);
        this._grid2.addColumnDefinition(0.25);
        this._grid2.addColumnDefinition(0.25);

        this._grid2.addRowDefinition(1);

        this._gridStack.addControl(this._grid2);
        this._grid2.top = "50%";
        this._grid2.verticalAlignment = Container.VERTICAL_ALIGNMENT_TOP;
        this._grid2.paddingBottomInPixels = 8;
        this._grid2.paddingLeftInPixels = 8;
        this._grid2.paddingRightInPixels = 7;
        this._grid2.paddingTopInPixels = -5;
    }

    private _createPlanNumber(top: string, parent: Container) {
        const planNumberContainer = new Container("PlanNumberWrapper");
        planNumberContainer.width = "40%";
        planNumberContainer.height = "7%";

        const lightPNBG = new Rectangle("PlanNumberBG");
        lightPNBG.background = "#C7D0D8";
        planNumberContainer.addControl(lightPNBG);
        lightPNBG.height = "100%";
        lightPNBG.width = "80%";
        lightPNBG.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;

        const inputPlanNumber = new InputText("PlanNumberText");
        inputPlanNumber.placeholderText = Strings.PLAN_NUMBER_INPUT_PLACEHOLDER;
        inputPlanNumber.fontFamily = "BasisGrotesquePro";

        lightPNBG.addControl(inputPlanNumber);
        inputPlanNumber.height = "100%";
        inputPlanNumber.width = "99.9%";
        inputPlanNumber.background = "white";
        inputPlanNumber.placeholderColor = "black";
        inputPlanNumber.fontSize = "20px";
        inputPlanNumber.paddingBottomInPixels = 3;
        inputPlanNumber.paddingLeftInPixels = 5;
        inputPlanNumber.paddingRightInPixels = 2;
        inputPlanNumber.paddingTopInPixels = 5;
        inputPlanNumber.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;
        inputPlanNumber.focusedBackground = "white";
        inputPlanNumber.textHighlightColor = "black";
        inputPlanNumber.color = "black";
        inputPlanNumber.thickness = 0;

        const lightBBG = new Rectangle("GoButtonBG");
        lightBBG.background = "#C7D0D8";
        planNumberContainer.addControl(lightBBG);
        lightBBG.height = "100%";
        lightBBG.width = "20%";
        lightBBG.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_RIGHT;
        lightBBG.thickness = 0;

        const goButton = Button.CreateSimpleButton("GoButton", Strings.GO);
        goButton.fontFamily = "BasisGrotesquePro";

        lightBBG.addControl(goButton);
        goButton.height = "100%";
        goButton.width = "100%";
        goButton.background = "white";
        goButton.paddingBottomInPixels = 3;
        goButton.paddingLeftInPixels = 5;
        goButton.paddingRightInPixels = 3;
        goButton.paddingTopInPixels = 5;
        goButton.thickness = 0;

        goButton.onPointerEnterObservable.add(() => {
            goButton.background = "black";
            goButton.color = "white";
            lightBBG.background = "black";

            this._app.setCursor("pointer");
        });
        goButton.onPointerOutObservable.add(() => {
            goButton.background = "white";
            lightBBG.background = "#C7D0D8";
            goButton.color = "black";

            this._app.resetCursor();
        });
        goButton.onPointerClickObservable.add(() => {
            if (PlanManager.isPlanNumberValid(inputPlanNumber.text)) {
                this._app.showLoadingScreen();

                PlanManager.getRoom(inputPlanNumber.text).then((room) => {
                    this._app.hideLoadingScreen();

                    if (!room) {
                        // TODO error
                        return;
                    } 

                    this._app.loadRoomFromData(room);
                });
            } else {
                inputPlanNumber.text = "";
                this._planModal?.show();
            }
        });

        parent.addControl(planNumberContainer);
        planNumberContainer.top = top;
        planNumberContainer.verticalAlignment = Container.VERTICAL_ALIGNMENT_TOP;
    }

    private _onRoomShapeChosen(btn: Button, shape: RoomShape) {
        const roomScene = this._scene as RoomMenuScene;
        roomScene.onRoomChosen(shape);
    }

    private _loadRoomShapeButtons() {
        let i = 0;
        for (const roomShape of RoomShapes) {
            const btn = Button.CreateSimpleButton("RoomShapeBtn_" + roomShape.Name, roomShape.Name);
            btn.height = "100%";
            btn.width = "100%";

            btn.background = "transparent";
            btn.paddingBottomInPixels = -15;
            btn.paddingLeftInPixels = -15;
            btn.paddingRightInPixels = -15;
            btn.paddingTopInPixels = -15;

            const greyBg = new Rectangle("ButtonGrayBG");
            greyBg.background = "#E6EDF2";

            btn.addControl(greyBg);
            greyBg.width = "100%";
            greyBg.height = "100%";

            const whiteBg = new Rectangle("ButtonWhiteBG");
            whiteBg.background = "white";

            btn.addControl(whiteBg);
            whiteBg.width = "100%";
            whiteBg.height = "100%";

            const img = new Image("RoomButtonImage", roomShape.Image);
            img.stretch = Image.STRETCH_UNIFORM;
            btn.addControl(img);
            img.width = "90%";
            img.height = "90%";
            img.verticalAlignment = Button.VERTICAL_ALIGNMENT_CENTER;
            img.horizontalAlignment = Button.VERTICAL_ALIGNMENT_CENTER;

            btn.onPointerClickObservable.add(() => {
                this._app.resetCursor();
                this._onRoomShapeChosen(btn, roomShape);
            });
            btn.onPointerEnterObservable.add(() => {
                const toks = roomShape.Image.split(".png");
                img.source = toks[0] + "_black.png";

                this._app.setCursor("pointer");
            });
            btn.onPointerOutObservable.add(() => {
                img.source = roomShape.Image;

                this._app.resetCursor();
            });

            let pb = 0, pl = 0, pt = 0, pr = 0;
            let grid = this._grid1;
            let xPos = i;
            if (i > 2)
            {
                xPos = i - 3;
                grid = this._grid2;

                switch (xPos)
                {
                    case 0:
                        pt = 18;
                        pr = 20;
                        break;
                    case 1:
                        pt = 18;
                        pr = 20;
                        break;
                    case 2:
                        pt = 18;
                        pr = 20;
                        break;
                    case 3:
                        pt = 18;
                        break;
                }
            }
            else 
            {
                switch (xPos)
                {
                    case 0:
                        pb = 20;
                        pr = 20;
                        break;
                    case 1:
                        pb = 20;
                        pr = 20;
                        break;
                    case 2:
                        pb = 20;
                        break;
                }
            }

            whiteBg.paddingBottomInPixels = pb;
            whiteBg.paddingLeftInPixels = pl;
            whiteBg.paddingTopInPixels = pt;
            whiteBg.paddingRightInPixels = pr;

            if  (grid)
                grid.addControl(btn, 0, xPos);

            this._shapeButtons.push(btn);
            i++;
        }
    }
}