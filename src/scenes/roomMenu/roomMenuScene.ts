import {Engine} from "@babylonjs/core";
import {SceneBase, States} from "../sceneBase";
import {SceneGUI} from "../../gui/sceneGui";
import {FoundPopConfigurator} from "../../index";
import {RoomMenuGui} from "./roomMenuGui";
import {RoomShape} from "../../models/roomShape";

export class RoomMenuScene extends SceneBase {
    private _gui: RoomMenuGui | null = null;

    constructor(_engine : Engine, _app: FoundPopConfigurator) {
        super(_engine, _app);
    }

    protected _loadScene() {
        super._loadScene();

        this._camera.detachControl();
    }

    getSceneGui(): SceneGUI | null {
        if (!this._gui)
            this._gui = new RoomMenuGui(this, this._app);

        return this._gui;
    }

    public onRoomChosen(roomShape: RoomShape) {
        this._app.createRoom(roomShape);
    }

    public hasSideMenu() : boolean {
        return false;
    }
}