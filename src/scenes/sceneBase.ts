import {ArcRotateCamera, Camera, Color4, Engine, Scene, Vector3} from "@babylonjs/core";
import { AdvancedDynamicTexture } from "@babylonjs/gui";
import {FoundPopGUI} from "../gui/gui";
import {SceneGUI} from "../gui/sceneGui";
import {FoundPopConfigurator} from "../index";

export enum States {
    MAIN_MENU,
    ROOM_MENU,
    EDITING_2D,
    ELEMENTS_MENU,
    VIEW_3D,
    DEVICE_NOT_SUPPORTED
}

export class SceneBase {
    public Scene: Scene | null;

    // @ts-ignore
    protected _camera: Camera;
    // @ts-ignore
    protected _guiCamera: Camera;

    protected _minZoom = 2;
    protected _maxZoom = 12;
    protected _startingZoom = 6;

    constructor(protected _engine : Engine, protected _app: FoundPopConfigurator,
                public ShouldRunUpdate: boolean = false) {
        this.Scene = new Scene(this._engine);
        this.Scene.clearColor = new Color4(0.96, 0.96, 0.96, 1);

        this._loadScene();

        if (!this.ShouldRunUpdate) return;

        this.Scene.onBeforeRenderObservable.add(() => {
            this._onUpdate();
        });
    }

    protected _onUpdate() {
        const sceneGui = this.getSceneGui();
        if (sceneGui) {
            sceneGui.onUpdate();
        }
    }

    protected _loadScene() {
        const camera = new ArcRotateCamera("Camera", Math.PI / 2, Math.PI / 2, this._startingZoom, Vector3.Zero(), this.Scene as Scene);
        camera.wheelPrecision = 20;
        camera.upperRadiusLimit = this._maxZoom;
        camera.lowerRadiusLimit = this._minZoom;
        camera.attachControl(this._engine.getRenderingCanvas(), true);

        this._camera = camera;

        const guiCamera = new ArcRotateCamera("GuiCamera", Math.PI / 2 + Math.PI / 7, Math.PI / 2, 100, new Vector3(0, 20, 0), this.Scene as Scene);
        guiCamera.layerMask  = 0x10000000;

        this._guiCamera = guiCamera;

        // @ts-ignore
        this.Scene?.activeCameras = [camera, guiCamera];
    }

    public getCamera(): Camera {
        return this._camera;
    }

    public dispose() {
        if (!this.Scene) return;

        this.Scene.dispose();
        this.Scene = null;

        //if (this._app.CreatedRoom)
            //this._app.CreatedRoom.dispose();
    }

    public getSceneGui() : SceneGUI | null {
        return null;
    }

    public hasSideMenu() : boolean {
        return true;
    }

    public hasTopMenu() : boolean {
        return false;
    }
}