import {SceneBase} from "./sceneBase";
import {ArcRotateCamera, Engine, HemisphericLight, Mesh, MeshBuilder, Scene, Vector3} from "@babylonjs/core";
import {SceneGUI} from "../gui/sceneGui";
import {FoundPopConfigurator} from "../index";

export class TestScene extends SceneBase {
    constructor(_engine : Engine, _app: FoundPopConfigurator) {
        super(_engine, _app);
    }

    protected _loadScene() {
        const light: HemisphericLight = new HemisphericLight("Light", new Vector3(1, 1, 0), this.Scene as Scene);
        const sphere: Mesh = MeshBuilder.CreateSphere("Sphere", { diameter: 1 }, this.Scene);
    }

    getSceneGui(): SceneGUI | null {
        return new SceneGUI(this, this._app);
    }
}