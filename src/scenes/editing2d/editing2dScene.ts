import {
    ArcRotateCamera,
    Color3,
    DefaultRenderingPipeline,
    DirectionalLight,
    Engine,
    HemisphericLight,
    HighlightLayer,
    Observable,
    PickingInfo,
    PointerEventTypes,
    PointerInfo,
    RayHelper,
    Scene,
    ShadowGenerator,
    Vector2,
    Vector3
} from "@babylonjs/core";
import {SceneBase, States} from "../sceneBase";
import {SceneGUI} from "../../gui/sceneGui";
import {FoundPopConfigurator} from "../../index";
import {Editing2dGui, EditingState} from "./editing2dGui";
import {AdvancedDynamicTexture} from "@babylonjs/gui";
import {Room} from "../../models/room";
import {Placeable, PlaceableCategory} from "../../models/placeable";
import {PlaceableWall} from "../../elements/placeableWall";
import {RoomElement} from "../../elements/roomElement";
import {DetailsWindow} from "./details/detailsWindow";
import { UnitCalculator } from "../../utils/unitCalculator";
import { ActionType, HistoryEntry, HistoryManager } from "../../utils/historyManager";

export class Editing2dScene extends SceneBase {
    private _sceneGui: Editing2dGui | null = null;
    //private _helpers: RayHelper[] = [];
    private _light: HemisphericLight  | null = null;
    private _light3d: DirectionalLight | null = null;
    
    private _hoveredElement: Placeable | null = null;
    private _selectedElement: Placeable | null = null;

    private _highlightLayer: HighlightLayer | null = null;
    private _pipeline: DefaultRenderingPipeline | null = null;

    private _isDragging: boolean = false;
    private _dragStartPos: Vector2 = Vector2.Zero();

    public ShadowGenerator: ShadowGenerator | null = null;

    public onCurrentElementPositioned: Observable<RoomElement> = new Observable<RoomElement>();

    constructor(_engine : Engine, _app: FoundPopConfigurator) {
        super(_engine, _app, true);
        
        HistoryManager.onUndoRequested.add((entry) => {
            this._onUndoRequested(entry);
        });
    }

    private _removePlaceable(placeable: Placeable) {
        if (placeable.Category === PlaceableCategory.WALL) {
            const wall = placeable as PlaceableWall;
            this._app.CreatedRoom?.removeWall(wall);
        } else {
            const element = placeable as RoomElement;        
            this._app.CreatedRoom?.removeElement(element);
        }
    }

    private _addPlaceable(placeable: Placeable) {
        if (placeable.Category === PlaceableCategory.WALL) {
            this._app.CreatedRoom?.addWall(placeable as PlaceableWall);
        } else {
            this._app.CreatedRoom?.addElement(placeable as RoomElement);
        }
    }

    private _onUndoRequested(entry: HistoryEntry) {
        switch (entry.Type) {
            case ActionType.CREATE_ELEMENT:
                this._removePlaceable(entry.TargetElement!);
                break;
            case ActionType.ROTATE_ELEMENT:
                entry.TargetElement?.rotateZ(Math.PI / 2);
                break;
            case ActionType.DELETE_ELEMENT:
                entry.TargetElement?.render2D(this);
                this._addPlaceable(entry.TargetElement!);

                this._selectedElement = entry.TargetElement;
                entry.TargetElement!.select();
                entry.TargetElement!.setPickable();
                break;
            case ActionType.MOVE_ELEMENT:
                this._removePlaceable(entry.OriginalElement!);
                entry.TargetElement?.render2D(this);
                this._addPlaceable(entry.TargetElement!);

                this._selectedElement = entry.TargetElement;
                entry.TargetElement!.select();
                entry.TargetElement!.setPickable();
                break;
        }
    }

    private _rotateElementBy(euler: number) {
        if (!this._sceneGui || !this._sceneGui.CurrentElement) return;

        this._sceneGui.CurrentElement.rotateZ(euler);
    }

    private _positionElement() {
        if (!this._sceneGui || !this._sceneGui.CurrentElement) return;

        this._addPlaceable(this._sceneGui.CurrentElement);
        const element = this._sceneGui.CurrentElement;

        HistoryManager.pushAction(ActionType.CREATE_ELEMENT, this._sceneGui.CurrentElement);

        this._sceneGui.clearCurrentElement();
        this._sceneGui.enableFloorLabels();

        this.onCurrentElementPositioned.notifyObservers(element as RoomElement);
    }

    private _getHoveredPlaceable(pickInfo: PickingInfo): Placeable | null {
        if (!pickInfo.pickedMesh) return null;
        if (!pickInfo.pickedMesh.metadata) return null;
        if (!pickInfo.pickedMesh.metadata.placeable) return null;

        return pickInfo.pickedMesh.metadata.placeable as Placeable;
    }

    protected _loadScene() {
        super._loadScene();

        if (this.Scene)
            this.Scene.ambientColor = Color3.White();

        this._pipeline = new DefaultRenderingPipeline(
            "DefaultPipeline",
            true,
            this.Scene as Scene,
            [this._camera]
        );
        this._pipeline.samples = 8;

        this._setupLight2d();

        this._highlightLayer = new HighlightLayer("HighlightLayer", this.Scene as Scene);

        this._setupCamera2d();

        if (this._app.CreatedRoom) {
            this._app.CreatedRoom.render2D(this);
        }

        this.Scene?.onPointerObservable.add((pointerInfo) => {
            this._onPointerEvent(pointerInfo);
        });
    }

    private _setupLight2d() {
        if (!this._light3d) {
            this._light3d = (this.Scene as Scene).getLightByName("Light3D") as DirectionalLight;
        }
        if (this._light3d)
            this._light3d.setEnabled(false);

        if (!this._light) {
            this._light = (this.Scene as Scene).getLightByName("Light") as HemisphericLight;
        }
        if (!this._light) {
            const light: HemisphericLight = new HemisphericLight("Light", new Vector3(0, 0, 20), this.Scene as Scene);
            light.intensity = 1;
            light.specular = Color3.Black();
            this._light = light;
        }

        this._light.setEnabled(true);
    }

    private _setupLight3d() {
        if (!this._light) {
            this._light = (this.Scene as Scene).getLightByName("Light") as HemisphericLight;
        }
        this._light.setEnabled(false);

        if (!this._light3d) {
            this._light3d = (this.Scene as Scene).getLightByName("Light3D") as DirectionalLight;
        }
        if (!this._light3d) {
            const light = new DirectionalLight("Light3D", new Vector3(0.5, -1, -0.5), this.Scene as Scene);
            light.intensity = 1;
            light.position = new Vector3(0, 10, 0);
            this._light3d = light;

            this.ShadowGenerator = new ShadowGenerator(2048, light);
            this.ShadowGenerator.useBlurExponentialShadowMap = true;
        }

        this._light3d.setEnabled(true);
    }

    private _startDrag() {
        this._isDragging = true;
        this._hoveredElement?.setNotPickable();

        this._sceneGui?.disableFloorLabels();

        this._dragStartPos = new Vector2(this.Scene!.pointerX, this.Scene!.pointerY);

        let cloned: Placeable | null = null;
        if (this._hoveredElement?.Category === PlaceableCategory.WALL) {
            const wall = this._hoveredElement as PlaceableWall;
            cloned = wall.clone();
        } else {
            const element = this._hoveredElement as RoomElement;
            cloned = element.clone();
        }

        cloned.setPickable();
        HistoryManager.pushAction(ActionType.MOVE_ELEMENT, cloned, this._hoveredElement);
    }

    private _onDrag(pickInfo: PickingInfo) {
        if (!this._isDragging) return;
        if (!this.Scene) return;
        if (!pickInfo.pickedPoint) return;
        if (!this._hoveredElement) return;

        const curMousePos = new Vector2(this.Scene!.pointerX, this.Scene!.pointerY);
        const diff = Vector2.Distance(curMousePos, this._dragStartPos);
        if (diff <= 1) return;

        const rays = this._hoveredElement.getRays(pickInfo.pickedPoint);
        let allHit = true;

        /*for (const helper of this._helpers) {
            helper.dispose();
        }*/

        for (const ray of rays) {
            /*let helper = new RayHelper(ray);
            helper.show(this.Scene, Color3.Green());
            this._helpers.push(helper);*/

            const pick = this.Scene.pickWithRay(ray);
            if (!pick || !pick.pickedMesh || pick.pickedMesh.name !== "Floor_Polygon") {
                allHit = false;
                break;
            }

            allHit = allHit && pick.hit;
        }

        if (allHit) {
            pickInfo.pickedPoint.z = 0;
            this._hoveredElement.setPositionWithSnap(pickInfo.pickedPoint);
        }
    }

    private _stopDrag() {
        this._isDragging = false;
        this._hoveredElement?.setPickable();

        this._sceneGui?.enableFloorLabels();
    }

    private _onLeftMousePointerDown(pointerInfo: PointerInfo) {
        if (this._sceneGui?.EditingState == EditingState.POSITIONING) {
            if (this._hoveredElement && !this._sceneGui?.isContextMenuOpen()) {
                this._startDrag();

                const wall = this._hoveredElement as PlaceableWall;
                if (this._hoveredElement.Category === PlaceableCategory.WALL) {
                    this._app.Gui?.SideMenu?.goToWallPanel(wall);
                }
            } else if (this._sceneGui?.CurrentElement) {
                this._positionElement();
            }
        }
    }

    private _onRightMousePointerDown(pointerInfo: PointerInfo) {
        if (this._sceneGui?.EditingState === EditingState.POSITIONING) {
            if (this._hoveredElement && this.Scene && this._app.State !== States.VIEW_3D) {
                if (this._isDragging) {
                    this._hoveredElement.rotateZ(Math.PI / 2);
                } else {
                    this._sceneGui?.toggleContextMenu();
                }
            } else if (this._sceneGui?.CurrentElement) {
                this._rotateElementBy(Math.PI / 2);
            } else {
                this._sceneGui?.hideContextMenu();
            }
        } else {
            if (this._sceneGui)
                this._sceneGui.resetEditingState();
        }
    }

    private _onLeftMousePointerUp(pointerInfo: PointerInfo) {
        if (!this._sceneGui) return;

        switch (this._sceneGui.EditingState) {
            case EditingState.POSITIONING:
                if (this._hoveredElement && this._isDragging) {
                    this._stopDrag();

                    if (this._selectedElement) {
                        this._selectedElement.removeSelection();
                    }

                    this._selectedElement = this._hoveredElement;
                    this._selectedElement.select();
                }

                break;
            /*case EditingState.DELETE:
                this.deleteHovered();

                break;
            case EditingState.DUPLICATE:
                const wall = this._hoveredElement as PlaceableWall;
                let duplicate: Placeable | null = null;
                if (wall) {
                    duplicate = wall.clone();
                } else {
                    const element = this._hoveredElement as RoomElement;
                    duplicate = element.clone();
                }

                this._sceneGui.onElementChosen(duplicate);
                this._sceneGui.resetEditingState();

                break;
            case EditingState.ROTATE:
                if (this._hoveredElement)
                    this._hoveredElement.rotateZ(Math.PI / 2);

                HistoryManager.pushAction(ActionType.ROTATE_ELEMENT, this._hoveredElement);

                break;*/
        }
    }

    public rotateSelectedElement() {
        if (!this._selectedElement) return;

        this._selectedElement.rotateZ(Math.PI / 2);

        HistoryManager.pushAction(ActionType.ROTATE_ELEMENT, this._selectedElement);
    }

    public duplicateSelectedElement() {
        if (!this._selectedElement) return;
        
        const wall = this._selectedElement as PlaceableWall;
        let duplicate: Placeable | null = null;
        if (wall) {
            duplicate = wall.clone();
        } else {
            const element = this._selectedElement as RoomElement;
            duplicate = element.clone();
        }

        //duplicate.PositionInUnits.x += 1;
        this._sceneGui?.onElementChosen(duplicate);
    }

    protected _onPointerEvent(pointerInfo: PointerInfo) {
        switch (pointerInfo.type) {
            case PointerEventTypes.POINTERDOWN:
                if (pointerInfo.event.button === 0) // left mouse
                { 
                    this._onLeftMousePointerDown(pointerInfo);
                } 
                else if (pointerInfo.event.button === 2) // right mouse
                { 
                    this._onRightMousePointerDown(pointerInfo);
                }
                break;
            case PointerEventTypes.POINTERUP:
                if (pointerInfo.event.button === 0) // left mouse
                { 
                    this._onLeftMousePointerUp(pointerInfo);
                }
                break;
        }
    }

    protected _onUpdate() {
        super._onUpdate();

        if (!this.Scene) return;

        const pickInfo = this.Scene?.pick(this.Scene?.pointerX, this.Scene?.pointerY, undefined, undefined, this._camera);
        if (!pickInfo || !pickInfo.hit || !pickInfo.pickedPoint) {
            if (this._isDragging) {
                this._stopDrag();
            }

            if (!this._highlightLayer) {
                this._highlightLayer = this.Scene.getHighlightLayerByName("HighlightLayer");
            }
            if (!this._sceneGui?.isContextMenuOpen()) {
                this._hoveredElement?.removeHighlight(this._highlightLayer);
                this._hoveredElement = null;

                this._app.resetCursor();
            }
            return;
        }

        if (this._isDragging) {
            this._onDrag(pickInfo);
            return;
        }
        
        const hoveredElement = this._getHoveredPlaceable(pickInfo);
        if (hoveredElement && !this._sceneGui?.CurrentElement) {
            if (!this._highlightLayer) {
                this._highlightLayer = this.Scene.getHighlightLayerByName("HighlightLayer");
            }
            if (this._hoveredElement && !this._sceneGui?.isContextMenuOpen()) {
                this._hoveredElement.removeHighlight(this._highlightLayer);
            }
            this._hoveredElement = hoveredElement;
            this._hoveredElement.highlight(this._highlightLayer);

            this._app.setCursor("pointer");
        } else {
            if (!this._highlightLayer) {
                this._highlightLayer = this.Scene.getHighlightLayerByName("HighlightLayer");
            }
            if (!this._sceneGui?.isContextMenuOpen()) {
                this._hoveredElement?.removeHighlight(this._highlightLayer);
                this._hoveredElement = null;

                this._app.resetCursor();
            }

            if (this._app.CreatedRoom && this._sceneGui && this._sceneGui.CurrentElement && this.Scene && this._app.State === States.EDITING_2D) {
                const rays = this._sceneGui.CurrentElement.getRays(pickInfo.pickedPoint);
                let hits = 0;

                this._sceneGui.disableFloorLabels();

                //for (const helper of this._helpers) {
                //    helper.dispose();
                //}

                for (const ray of rays) {
                    //let helper = new RayHelper(ray);
                    //helper.show(this.Scene, Color3.Green());
                    //this._helpers.push(helper);

                    const pick = this.Scene.pickWithRay(ray);
                    if (!pick || !pick.pickedMesh || pick.pickedMesh.name !== "Floor_Polygon") {
                        continue;
                    }

                    if (pick.hit)
                        hits++;
                }

                if (hits == rays.length) {
                    pickInfo.pickedPoint.z = 0;
                    this._sceneGui.CurrentElement.setPositionWithSnap(pickInfo.pickedPoint);
                }
            }
        }
    }

    getSceneGui(): SceneGUI | null {
        if (!this._sceneGui) {
            this._sceneGui = new Editing2dGui(this, this._app);
        }
        return this._sceneGui;
    }

    _setupCamera2d() {
        const cam = this._camera as ArcRotateCamera;
        cam.lowerAlphaLimit = Math.PI / 2;
        cam.upperAlphaLimit = Math.PI / 2;
        cam.lowerBetaLimit = Math.PI / 2;
        cam.upperBetaLimit = Math.PI / 2;
        cam.lowerRadiusLimit = 4;
        cam.upperRadiusLimit = 33;

        cam.radius = 7.480;
    }

    _setupCamera3d() {
        const cam = this._camera as ArcRotateCamera;
        cam.lowerAlphaLimit = -Math.PI * 2;
        cam.upperAlphaLimit = Math.PI * 2;
        cam.lowerBetaLimit = -Math.PI * 2;
        cam.upperBetaLimit = Math.PI * 2;
        cam.lowerRadiusLimit = 4;
        cam.upperRadiusLimit = 33;

        cam.radius = 8;
        cam.beta = 1.214;
        cam.alpha = 0.868;
    }

    public zoomIn() {
        const cam = this._camera as ArcRotateCamera;
        cam.radius++;
    }

    public zoomOut() {
        const cam = this._camera as ArcRotateCamera;
        cam.radius--;
    }

    public to2d() {
        this._setupCamera2d();
        this._setupLight2d();

        if (this._app.CreatedRoom) {
            this._app.CreatedRoom.render2D(this);
        }

        if (this._sceneGui) {
            this._sceneGui.on2dView();
        }
    }

    public to3d() {
        this._setupCamera3d();
        this._setupLight3d();

        if (this._app.CreatedRoom) {
            this._app.CreatedRoom.render3D(this);
        }

        if (this._sceneGui) {
            this._sceneGui.on3dView();
        }
    }

    public getRoomArea(): number {
        if (!this._app.CreatedRoom) return 0.0;

        return this._app.CreatedRoom.Shape.getArea();
    }

    public getRoomHeight(): number {
        if (!this._app.CreatedRoom) return 0.0;

        return 0 / 100;
    }

    public getGuiTexture(): AdvancedDynamicTexture | null {
        return this._app.getAdvancedGUITexture();
    }

    public getRoom(): Room | null {
        return this._app.CreatedRoom;
    }

    public refreshRoom() {
        if (this._app.CreatedRoom) {
            this._app.CreatedRoom.render2D(this);
        }
    }

    public rotateHoveredZ() {
        if (!this._hoveredElement) return;

        this._hoveredElement.rotateZ(Math.PI / 2);
    }

    public rotateHoveredY() {
        if (!this._hoveredElement) return;

        this._hoveredElement.rotateY(Math.PI / 2);
    }

    public getHoveredDetails(): DetailsWindow | null {
        if (!this._hoveredElement) return null;

        // @ts-ignore
        return this._hoveredElement.getDetailsWindow(this.getGuiTexture());
    }

    public deleteHovered() {
        if (!this._hoveredElement) return;

        if (this._hoveredElement.Category === PlaceableCategory.WALL) {
            const wall = this._hoveredElement as PlaceableWall;
            HistoryManager.pushAction(ActionType.DELETE_ELEMENT, wall.clone());

            this._app.CreatedRoom?.removeWall(wall);
        } else {
            const element = this._hoveredElement as RoomElement;
            HistoryManager.pushAction(ActionType.DELETE_ELEMENT, element.clone());

            this._app.CreatedRoom?.removeElement(element);
        }
    }

    public deleteSelectedElement() {
        if (!this._selectedElement) return;

        if (this._selectedElement.Category === PlaceableCategory.WALL) {
            const wall = this._selectedElement as PlaceableWall;
            HistoryManager.pushAction(ActionType.DELETE_ELEMENT, wall.clone());

            this._app.CreatedRoom?.removeWall(wall);
        } else {
            const element = this._selectedElement as RoomElement;
            HistoryManager.pushAction(ActionType.DELETE_ELEMENT, element.clone());

            this._app.CreatedRoom?.removeElement(element);
        }
    }

    public hasTopMenu() : boolean {
        return true;
    }
}