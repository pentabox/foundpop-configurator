import {SceneGUI} from "../../gui/sceneGui";
import {SceneBase, States} from "../sceneBase";
import {Editing2dScene} from "./editing2dScene";
import {
    AdvancedDynamicTexture,
    Button,
    Container,
    Control,
    Ellipse,
    Grid,
    Image,
    InputText,
    Rectangle,
    StackPanel,
    TextBlock
} from "@babylonjs/gui";
import {Strings} from "../../utils/strings";
import {Color3, Mesh} from "@babylonjs/core";
import {RoomWall, WallOrientation} from "../../models/roomShape";
import {Constants} from "../../utils/constants";
import {FoundPopConfigurator} from "../../index";
import {Placeable} from "../../models/placeable";
import {RoomElements, RoomWalls} from "../../data/roomElements";
import {PlaceableWall} from "../../elements/placeableWall";
import {RoomElement} from "../../elements/roomElement";
import { CircleButton } from "../../gui/circleButton";
import { ModalWindow } from "../../gui/modalWindow";
import { ElementsList, RoomElementChoice } from "../../gui/elementsList";
import { HistoryManager } from "../../utils/historyManager";

export enum EditingState {
    POSITIONING,
    DUPLICATE,
    ROTATE,
    DELETE
}

export class Editing2dGui extends SceneGUI {
    private _infoPanel: Container | null = null;
    private _editingWall: RoomWall | null = null;
    private _areaText: TextBlock | null = null;
    private _wallButtons: Button[] = [];

    private _wallLengthModal: ModalWindow;
    private _wallLengthInputText: InputText | null = null;

    private _elementsList: ElementsList;
    private _elementsControls: Container | null = null;
    private _elementControlButtons: CircleButton[] = [];

    private _currentElementChoice: RoomElementChoice | null = null;

    private _contextMenu: Container | null = null;
    private _wallGuiLayer: AdvancedDynamicTexture;

    public EditingState: EditingState = EditingState.POSITIONING;

    public CurrentElement: Placeable | null = null;

    constructor(scene: SceneBase, _app: FoundPopConfigurator) {
        super(scene, _app);

        this._container.left = "150px";
        this._container.width = "1400px";
        this._container.height = "90%";
        this._container.horizontalAlignment = StackPanel.HORIZONTAL_ALIGNMENT_CENTER;
        this._container.verticalAlignment = StackPanel.VERTICAL_ALIGNMENT_CENTER;
        this._container.isPointerBlocker = false;
        this._container.zIndex = -1;
        this._container.isVisible = false;

        this._wallGuiLayer = AdvancedDynamicTexture.CreateFullscreenUI("WallLayer", false);

        this.setupFloorLabels();
        this._setupInfoPanel();

        this._setupZoomControls();
        this._setupElementsControls();

        const wallLengthContent = new Container("WallLength");
        const text = new TextBlock("WallLengthLabel", Strings.INSERT_WALL_LENGTH);
        text.fontFamily = "BasisGrotesquePro";
        text.fontSizeInPixels = 25;
        text.width = "100%";
        text.height = "50%";
        text.color = "white";

        const input = new InputText("WallLengthInput", "0");
        input.fontFamily = "BasisGrotesquePro";
        input.fontSizeInPixels = 25;

        wallLengthContent.addControl(text);
        text.top = "-10%";

        wallLengthContent.addControl(input);
        input.top = "20%";
        input.width = "90%";
        input.height = "30%";
        input.color = "white";
        input.background = "transparent";
        input.thickness = 3;
        input.focusedBackground = "transparent";
        input.textHighlightColor = "black";

        this._wallLengthInputText = input;

        this._wallLengthModal = new ModalWindow(wallLengthContent, _app, _app.Gui!.GuiTexture);
        this._wallLengthModal.addLeftButton(Strings.CONFIRM, () => {
            this._onWallLengthChanged();
        });
        this._wallLengthModal.addRightButton(Strings.CANCEL, () => {
            this._wallLengthModal.hide();
        });

        this._wallLengthModal.hide();

        this._elementsList = new ElementsList(_app);
        this._elementsList.onElementsChosen.add((choice: RoomElementChoice) => {
            this._onMultipleElementsChosen(choice);
        });

        //this._setupContextMenu();

        if (_app.Gui && _app.Gui.SideMenu) {
            _app.Gui.SideMenu.onHorizontalWallChosen.add(() => {
                const horz = RoomWalls.find(w => w.StartingOrientation == WallOrientation.HORIZONTAL);
                const newWall = this.onElementChosen(horz!);

                this._app.Gui?.SideMenu?.goToWallPanel(newWall as PlaceableWall);
            });
            _app.Gui.SideMenu.onVerticalWallChosen.add(() => {
                const vert = RoomWalls.find(w => w.StartingOrientation == WallOrientation.VERTICAL);
                const newWall = this.onElementChosen(vert!);

                this._app.Gui?.SideMenu?.goToWallPanel(newWall as PlaceableWall);
            });
        }
    }

    public resetEditingState() {
        this.EditingState = EditingState.POSITIONING;

        for (const btn of this._elementControlButtons) {
            btn.reset();
        }

        if (this._app.Gui && this._app.Gui.SideMenu)
                this._app.Gui.SideMenu.enable();
    }

    public disableWallButtons() {
        for (const b of this._wallButtons) {
            b.isEnabled = false;
        }
    }

    public enableWallButtons() {
        for (const b of this._wallButtons) {
            b.isEnabled = true;
        }
    }

    private _setupZoomControls() {
        const zoomContainer = new Container("ZoomControls");
        zoomContainer.width = "10%";
        zoomContainer.height = "20%";
        zoomContainer.left = "13%";
        zoomContainer.top = "0%";

        const plusBtn = new CircleButton("Plus", zoomContainer, this._app, 
        {
            image: "assets/images/icons/plus.svg"
        });

        plusBtn.getButton().onPointerClickObservable.add(() => {
            (this._scene as Editing2dScene).zoomOut();
        }); 

        plusBtn.setTop("-35px");

        const minusBtn = new CircleButton("Minus", zoomContainer, this._app, 
        {
            image: "assets/images/icons/minus.svg"
        });

        minusBtn.getButton().onPointerClickObservable.add(() => {
            (this._scene as Editing2dScene).zoomIn();
        }); 

        minusBtn.setTop("35px");

        const editingScene = (this._app.getScene() as Editing2dScene);
        const guiTexture = editingScene.getGuiTexture();
        if (guiTexture) {
            guiTexture.addControl(zoomContainer);
        }

        zoomContainer.verticalAlignment = Container.VERTICAL_ALIGNMENT_BOTTOM;
        zoomContainer.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;
    }

    private _setupElementsControls() {
        const elementsContainer = new Container("ElementControls");
        elementsContainer.width = "7%";
        elementsContainer.height = "44%";
        elementsContainer.left = "0%";
        elementsContainer.top = "0%";

        const fontSize = 18;
        const fontFamily = "BasisGrotesquePro";
        const rotateBtn = new CircleButton("Rotate", elementsContainer, this._app, {
            image: "assets/images/icons/rotate.svg",
            imageHeight: "35px",
            imageWidth: "35px",

            text: Strings.ROTATE,
            fontSize: fontSize,
            fontFamily: fontFamily
        });
        rotateBtn.setAlignment(Container.HORIZONTAL_ALIGNMENT_CENTER, Container.VERTICAL_ALIGNMENT_TOP);
        rotateBtn.setTop("30px");

        const copyBtn = new CircleButton("Copy", elementsContainer, this._app, {
            image: "assets/images/icons/duplicate.svg",
            imageHeight: "35px",
            imageWidth: "35px",

            text: Strings.COPY,
            fontSize: fontSize,
            fontFamily: fontFamily
        });
        copyBtn.setAlignment(Container.HORIZONTAL_ALIGNMENT_CENTER, Container.VERTICAL_ALIGNMENT_TOP);
        copyBtn.setTop("130px");

        let t = copyBtn.getText();
        if (t)
            t.top = "100px";

        const undoBtn = new CircleButton("Undo", elementsContainer, this._app, {
            image: "assets/images/icons/undo.svg",
            imageHeight: "30px",
            imageWidth: "30px",

            text: Strings.UNDO,
            fontSize: fontSize,
            fontFamily: fontFamily
        });
        undoBtn.setAlignment(Container.HORIZONTAL_ALIGNMENT_CENTER, Container.VERTICAL_ALIGNMENT_TOP);
        undoBtn.setTop("230px");

        t = undoBtn.getText();
        if (t)
            t.top = "200px";

        const deleteBtn = new CircleButton("Delete", elementsContainer, this._app, {
            image: "assets/images/icons/bin.svg",
            imageHeight: "35px",
            imageWidth: "35px",

            text: Strings.DELETE,
            fontSize: fontSize,
            fontFamily: fontFamily
        });
        deleteBtn.setAlignment(Container.HORIZONTAL_ALIGNMENT_CENTER, Container.VERTICAL_ALIGNMENT_TOP);
        deleteBtn.setTop("330px");

        copyBtn.getButton().onPointerClickObservable.add(() => {
            editingScene.duplicateSelectedElement();

            //this.EditingState = EditingState.DUPLICATE;

            //copyBtn.setLeft("-20px");
            //rotateBtn.reset();
            //deleteBtn.reset();

            //if (this._app.Gui && this._app.Gui.SideMenu)
              //  this._app.Gui.SideMenu.disable();
        });

        rotateBtn.getButton().onPointerClickObservable.add(() => {
            editingScene.rotateSelectedElement();

            /*this.EditingState = EditingState.ROTATE;

            rotateBtn.setLeft("-20px");
            deleteBtn.reset();
            copyBtn.reset();

            if (this._app.Gui && this._app.Gui.SideMenu)
                this._app.Gui.SideMenu.disable();*/
        });

        deleteBtn.getButton().onPointerClickObservable.add(() => {
            editingScene.deleteSelectedElement();

            /*this.EditingState = EditingState.DELETE;

            deleteBtn.setLeft("-20px");
            rotateBtn.reset();
            copyBtn.reset();

            if (this._app.Gui && this._app.Gui.SideMenu)
                this._app.Gui.SideMenu.disable();*/
        });
        
        undoBtn.getButton().onPointerClickObservable.add(() => {
            this._undoLastOperation();

            rotateBtn.reset();
            copyBtn.reset();
            deleteBtn.reset();
        });

        this._elementControlButtons.push(rotateBtn);
        this._elementControlButtons.push(deleteBtn);
        this._elementControlButtons.push(copyBtn);

        t = deleteBtn.getText();
        if (t)
            t.top = "300px";

        const editingScene = (this._app.getScene() as Editing2dScene);
        const guiTexture = editingScene.getGuiTexture();
        if (guiTexture) {
            guiTexture.addControl(elementsContainer);
        }

        elementsContainer.verticalAlignment = Container.VERTICAL_ALIGNMENT_BOTTOM;
        elementsContainer.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_RIGHT;

        this._elementsControls = elementsContainer;
        elementsContainer.isVisible = true;
    }

    private _undoLastOperation() {
        HistoryManager.undo();
    }

    public on3dView() {
        this.clearFloorLabels();

        if (this._elementsControls)
            this._elementsControls.isVisible = false;

        if (this._app.Gui && this._app.Gui.SideMenu)
            this._app.Gui.SideMenu.disable();
    }

    public on2dView() {
        this.setupFloorLabels();

        if (this._elementsControls)
            this._elementsControls.isVisible = true;

        if (this._app.Gui && this._app.Gui.SideMenu)
            this._app.Gui.SideMenu.enable();
    }

    public showElementControls() {
        if (!this._elementsControls) return;

        this._elementsControls.isVisible = true;
    }

    public hideElementControls() {
        if (!this._elementsControls) return;

        this._elementsControls.isVisible = true;
    }

    private _onMultipleElementsChosen(choice: RoomElementChoice) {
        if (this._currentElementChoice)
            this._currentElementChoice = null;

        this._currentElementChoice = choice;

        const editingScene = this._scene as Editing2dScene;
        
        editingScene.onCurrentElementPositioned.clear();
        editingScene.onCurrentElementPositioned.add((element: RoomElement) => {
            const qt = this._currentElementChoice!.getQuantity(element);
            if (qt <= 0) {
                element = this._currentElementChoice!.getFirstAvailableElement();
            }

            this.onElementChosen(element);
            this._currentElementChoice!.remove(element, 1);
        });

        const element = this._currentElementChoice!.getFirstAvailableElement();
        this.onElementChosen(element);
        this._currentElementChoice!.remove(element, 1);
    }

    public onElementChosen(element: Placeable): Placeable {
        if (this.CurrentElement) {
            this.CurrentElement.dispose();
            this.CurrentElement = null;
        }

        const placeableWall = element as PlaceableWall;
        if (placeableWall) {
            this.CurrentElement = placeableWall.clone();
        } else {
            this.CurrentElement = (element as RoomElement).clone();
        }

        if (this.CurrentElement)
            this.CurrentElement.render2D(this._scene);

        this.CurrentElement.setPickable();
        return this.CurrentElement;
    }

    public clearCurrentElement() {
        if (!this.CurrentElement) return;

        this.CurrentElement.setPickable();
        this.CurrentElement = null;
    }

    public clearFloorLabels() {
        for (const btn of this._wallButtons) {
            btn.dispose();
        }
    }

    public disableFloorLabels() {
        for (const btn of this._wallButtons) {
            btn.isVisible = false;
        }
    }

    public enableFloorLabels() {
        for (const btn of this._wallButtons) {
            //btn.isEnabled = true;
            btn.isVisible = true;
        }
    }

    public setupFloorLabels() {
        const editingScene = this._scene as Editing2dScene;

        const room = editingScene.getRoom();
        if (!room) return;

        const floor = room.FloorPolygon;
        if (!floor) return;

        for (const btn of this._wallButtons) {
            btn.dispose();
        }

        this._wallButtons = [];
        for (const wall of room.Shape.Walls) {
            const length = wall.getLength();
            const rectangle = Button.CreateSimpleButton("WallBtn", length.toFixed(2) + Strings.METERS);
            rectangle.width = "150px";
            rectangle.height = "40px";
            rectangle.color = "black";
            rectangle.cornerRadius = 0;
            rectangle.thickness = 1;
            rectangle.background = "white";
            rectangle.rotation = wall.Rotation;
            rectangle.isPointerBlocker = true;

            if (rectangle.textBlock)
            {
                rectangle.textBlock.textHorizontalAlignment = Button.HORIZONTAL_ALIGNMENT_LEFT;
                rectangle.textBlock.left = "15px";   
            }

            const editImage = new Image("WallBtn_EditImage", "assets/images/icons/pencil.svg");
            editImage.stretch = Image.STRETCH_UNIFORM;
            editImage.height = "85%";
            editImage.width = "30%";

            rectangle.addControl(editImage);
            editImage.verticalAlignment = Button.VERTICAL_ALIGNMENT_CENTER;
            editImage.horizontalAlignment = Button.HORIZONTAL_ALIGNMENT_RIGHT;

            rectangle.onPointerClickObservable.add(() => {
                this._onWallLabelClick(wall);
            });
            rectangle.onPointerEnterObservable.add(() => {
                this._app.setCursor("pointer");
            });
            rectangle.onPointerOutObservable.add(() => {
                this._app.resetCursor();
            });

            this._wallButtons.push(rectangle);

            this._wallGuiLayer.addControl(rectangle);
            //rectangle.zIndex = -1;

            const middle = wall.getMiddlePoint();
            const diffX = middle.x + floor.position.x;
            const diffZ = middle.z + floor.position.z;

            const referenceMesh = new Mesh("wallLabelReference");
            referenceMesh.parent = floor;
            referenceMesh.position.z = diffZ;
            referenceMesh.position.x = diffX;
            rectangle.metadata = {wall: wall, mesh: referenceMesh};
            rectangle.linkWithMesh(referenceMesh);
        }
    }

    private _setupInfoPanel() {
        const editingScene = this._scene as Editing2dScene;

        const infoPanel = new Container("InfoPanel");
        infoPanel.background = "transparent";
        infoPanel.width = "15%";
        infoPanel.height = "110px";
        infoPanel.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT;
        infoPanel.verticalAlignment = Control.VERTICAL_ALIGNMENT_BOTTOM;
        infoPanel.zIndex = 15;

        const grayLine1 = new Rectangle();
        grayLine1.width = "98%";
        grayLine1.height = "4px";
        grayLine1.background = "#C7D0D8";

        if (grayLine1)
            infoPanel.addControl(grayLine1);
        grayLine1.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_CENTER;
        grayLine1.verticalAlignment = Control.VERTICAL_ALIGNMENT_TOP;

        const areaLabel = new TextBlock("AreaLabel", Strings.AREA);
        if (areaLabel)
            infoPanel.addControl(areaLabel);
        
        areaLabel.fontFamily = "BasisGrotesquePro";
        areaLabel.fontSizeInPixels = 30;
        areaLabel.height = "40px";
        areaLabel.top = "-20px";

        const areaText = new TextBlock("AreaValue", editingScene.getRoomArea().toFixed(2) + Strings.METERS_SQUARED);
        if (areaText)
            infoPanel.addControl(areaText);

        areaText.fontFamily = "BasisGrotesquePro";
        areaText.fontSizeInPixels = 30;
        areaText.height = "40px";
        areaText.top = "20px"; 

        this._areaText = areaText;
        this._infoPanel = infoPanel;

        const guiTexture = editingScene.getGuiTexture();
        if (guiTexture && this._infoPanel) {
            guiTexture.addControl(this._infoPanel);
        }

        this._infoPanel.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT;
        this._infoPanel.verticalAlignment = Control.VERTICAL_ALIGNMENT_BOTTOM;
    }

    private _onWallLabelClick(wall: RoomWall) {
        if (!this._wallLengthModal) return;
        if (!this._wallLengthInputText) return;
        if (this._app.State === States.VIEW_3D) return;

        this._wallLengthInputText.text = (wall.getLength() * 1000).toFixed(2);
        this._wallLengthModal.show();
        this._editingWall = wall;
    }

    public showElements() {
        this.disableFloorLabels();
        this._elementsList.show();
    }

    private _onWallLengthChanged() {
        if (!this._wallLengthModal) return;
        console.log("modal");
        if (!this._wallLengthInputText) return;
        console.log("input text");
        if (!this._editingWall) return;
        console.log("editing wall");
        if (this._app.State === States.VIEW_3D) return;
        console.log("state");

        let length = Number(this._wallLengthInputText.text) / 1000;
        if (length < Constants.MIN_WALL_LENGTH)
            length = Constants.MIN_WALL_LENGTH;
        if (length > Constants.MAX_WALL_LENGTH)
            length = Constants.MAX_WALL_LENGTH;

        console.log(length);

        const editingScene = this._scene as Editing2dScene;
        const room = editingScene.getRoom();
        room?.scaleWall(this._editingWall, length);

        this._editingWall = null;

        editingScene.refreshRoom();

        if (this._areaText && room)
            this._areaText.text = room.Shape.getArea().toFixed(2) + Strings.METERS_SQUARED;

        if (room) {
            for (const wall of room?.Shape.Walls) {
                const btn = this._wallButtons.find(b => b.metadata.wall === wall);
                if (!btn || !btn.textBlock) continue;

                btn.textBlock.text = wall.getLength().toFixed(2) + Strings.METERS;
            }
        }

        this.setupFloorLabels();

        this._wallLengthModal.hide();
    }

    private _setupContextMenu() {
        const contextMenu = new Rectangle("ContextMenu");
        contextMenu.background = "white";
        contextMenu.height = "150px";
        contextMenu.width = "250px";
        contextMenu.thickness = 1;
        contextMenu.color = "black";
        contextMenu.cornerRadius = 5;

        const stackPanel = new StackPanel("ContextMenu_StackPanel");
        stackPanel.isVertical = true;
        stackPanel.height = "150px";
        stackPanel.width = "250px";
        contextMenu.addControl(stackPanel);

        const btnDetails = Button.CreateSimpleButton("ContextMenu_DetailsBtn", "DETAILS");
        btnDetails.width = "250px";
        btnDetails.height = "50px";

        btnDetails.onPointerClickObservable.add((evt) => {
            const editingScene = this._scene as Editing2dScene;
            if (editingScene) {
                const details = editingScene.getHoveredDetails();
                if (details) {
                    details.show(this._scene);
                }
            }

            contextMenu.isVisible = false;
        });

        stackPanel.addControl(btnDetails);

        const btnRotate = Button.CreateSimpleButton("ContextMenu_RotateBtn", "ROTATE");
        btnRotate.width = "250px";
        btnRotate.height = "50px";

        btnRotate.onPointerClickObservable.add((evt) => {
            const editingScene = this._scene as Editing2dScene;
            if (editingScene) {
                editingScene.rotateHoveredZ();
            }
        });

        stackPanel.addControl(btnRotate);

        const btnDelete = Button.CreateSimpleButton("ContextMenu_DeleteBtn", "DELETE");
        btnDelete.width = "250px";
        btnDelete.height = "50px";

        btnDelete.onPointerClickObservable.add((evt) => {
            const editingScene = this._scene as Editing2dScene;
            if (editingScene) {
                editingScene.deleteHovered();
            }

            this.hideContextMenu();
        });

        stackPanel.addControl(btnDelete);

        const guiTexture = this._app.getAdvancedGUITexture();
        guiTexture?.addControl(contextMenu);

        contextMenu.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT;
        contextMenu.verticalAlignment = Control.VERTICAL_ALIGNMENT_TOP;

        contextMenu.isVisible = false;
        this._contextMenu = contextMenu;
    }

    public toggleContextMenu() {
        if (!this._contextMenu) return;

        if (this._contextMenu.isVisible) {
            this.hideContextMenu();
        } else {
            this.showContextMenu();
        }
    }

    public showContextMenu() {
        if (!this._contextMenu) return;
        if (!this._scene || !this._scene.Scene) return;

        this._contextMenu.isVisible = true;
        this._contextMenu.left = this._scene.Scene.pointerX;
        this._contextMenu.top = this._scene.Scene.pointerY;
    }

    public hideContextMenu() {
        if (!this._contextMenu) return;

        this._contextMenu.isVisible = false;
    }

    public isContextMenuOpen(): boolean {
        if (!this._contextMenu) return false;

        return this._contextMenu.isVisible;
    }
}