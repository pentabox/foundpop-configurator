import {DetailsWindow} from "./detailsWindow";
import {Control, InputText, TextBlock} from "@babylonjs/gui";
import {Strings} from "../../../utils/strings";
import {PlaceableWall} from "../../../elements/placeableWall";

export class WallDetailsWindow extends DetailsWindow {
    protected _getFields(): Control[] {
        const controls: Control[] = [];

        const wall = this.Element as PlaceableWall;

        controls.push(...this._addTextInput("Height", (wall.HeightInUnits * 100).toFixed(0)));
        controls.push(...this._addTextInput("Width", (wall.WidthInUnits * 100).toFixed(0)));
        controls.push(...this._addTextInput("Length", (wall.LengthInUnits * 100).toFixed(0)));
        controls.push(...this._addColorInput("Color", wall.Color));

        return controls;
    }

    protected _onSave() {
        const height = this._getTextInputValue("Height");
        const width = this._getTextInputValue("Width");
        const length = this._getTextInputValue("Length");
        const color = this._getColorPickerValue("Color");

        const wall = this.Element as PlaceableWall;
        wall.HeightInUnits = parseInt(height) / 100;
        wall.WidthInUnits = parseInt(width) / 100;
        wall.LengthInUnits = parseInt(length) / 100;
        wall.Color = color;

        if (this._scene) {
            wall.render2D(this._scene);
        }
    }
}