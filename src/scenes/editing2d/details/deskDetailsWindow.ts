import {DetailsWindow} from "./detailsWindow";
import {Control} from "@babylonjs/gui";
import {RoomElement} from "../../../elements/roomElement";

export class DeskDetailsWindow extends DetailsWindow {
    protected _onSave() {
        const color = this._getColorPickerValue("Color");

        const element = this.Element as RoomElement;
        element.Color = color;

        if (this._scene) {
            element.render2D(this._scene);
        }
    }

    protected _getWindowHeight(): string {
        return "400px";
    }

    protected _getFields(): Control[] {
        const controls: Control[] = [];

        const element = this.Element as RoomElement;

        controls.push(...this._addColorInput("Color", element.Color));

        return controls;
    }
}