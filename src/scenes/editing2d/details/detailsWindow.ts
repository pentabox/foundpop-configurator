import {
    AdvancedDynamicTexture,
    Button, ColorPicker,
    Container,
    Control,
    InputText,
    Rectangle,
    StackPanel,
    TextBlock
} from "@babylonjs/gui";
import {Strings} from "../../../utils/strings";
import {Placeable} from "../../../models/placeable";
import {SceneBase} from "../../sceneBase";
import {Color3} from "@babylonjs/core";

export class DetailsWindow {
    private _container: Container;
    protected _scene: SceneBase | null = null;

    protected _inputTexts: any = {};
    protected _colorPickers: any = {};

    constructor(public Name: string,
                public Element: Placeable,
                gui: AdvancedDynamicTexture) {
        this._container = new Rectangle(Name + "_DetailsWindow");
        this._container.width = "800px";
        this._container.height = this._getWindowHeight();

        const fields = this._getFields();

        const detailsStack = new StackPanel("DetailsStack");
        detailsStack.isVertical = true;
        detailsStack.background = "white";
        detailsStack.width = "800px";
        detailsStack.height = this._getWindowHeight();

        const btnStack = new StackPanel("BtnStack");
        btnStack.height = "70px";
        btnStack.isVertical = false;
        btnStack.top = "50px";

        const previousBtn = Button.CreateSimpleButton("PreviousBtn", Strings.PREVIOUS)
        previousBtn.width = "200px";
        previousBtn.height = "50px";

        previousBtn.onPointerDownObservable.add(() => {
            this._onPreviousBtn();
        });

        const nextBtn = Button.CreateSimpleButton("NextBtn", Strings.NEXT);
        nextBtn.width = "200px";
        nextBtn.height = "50px";

        nextBtn.onPointerDownObservable.add(() => {
            this._onNextBtn();
        });

        btnStack.addControl(previousBtn);
        btnStack.addControl(nextBtn);

        for (const control of fields) {
            detailsStack.addControl(control);
        }

        detailsStack.addControl(btnStack);

        this._container.isVisible = false;

        this._container.addControl(detailsStack);
        this._container.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_CENTER;

        gui.addControl(this._container);
    }

    protected _getWindowHeight(): string {
        return "550px";
    }

    public show(scene: SceneBase) {
        this._container.isVisible = true;
        this._scene = scene;
    }

    private _onPreviousBtn() {
        this._container.isVisible = false;
        this._container.dispose();
    }

    private _onNextBtn() {
        this._container.isVisible = false;

        this._onSave();

        this._container.dispose();
    }

    protected _onSave() {

    }

    protected _getFields(): Control[] {
        const controls: Control[] = [];

        const heightLabel = new TextBlock("HeightLabel", Strings.ROOM_HEIGHT);
        heightLabel.height = "100px";
        controls.push(heightLabel);

        const heightText = new InputText("HeightText", "1000");
        heightText.height = "100px";
        heightText.color = "black";
        heightText.background = "#E0E0E0";
        heightText.width = "80%";
        heightText.focusedBackground = "#D0D0D0";
        heightText.margin = "10px";
        controls.push(heightText);

        return controls;
    }

    protected _addTextInput(name: string, defaultVal: string): Control[] {
        const controls: Control[] = [];

        const inputLabel = new TextBlock(name + "Label", name + " (cm)");
        inputLabel.height = "50px";

        controls.push(inputLabel);

        const inputText = new InputText(name + "Text", defaultVal);
        inputText.height = "50px";
        inputText.color = "black";
        inputText.background = "#E0E0E0";
        inputText.width = "80%";
        inputText.focusedBackground = "#D0D0D0";
        inputText.margin = "10px";

        this._inputTexts[name] = inputText;

        controls.push(inputText);

        return controls;
    }

    protected _addColorInput(name: string, defaultVal: Color3): Control[] {
        const controls: Control[] = [];

        const inputLabel = new TextBlock(name + "Label", name + " (cm)");
        inputLabel.height = "50px";

        controls.push(inputLabel);

        const colorPicker = new ColorPicker(name + "Color");
        colorPicker.height = "150px";
        colorPicker.width = "150px";
        colorPicker.value = defaultVal;

        this._colorPickers[name] = colorPicker;

        controls.push(colorPicker);

        return controls;
    }

    protected _getTextInputValue(name: string): string {
        const inputText = this._inputTexts[name];
        if (!inputText) return "";

        return (inputText as InputText).text;
    }

    protected _getColorPickerValue(name: string): Color3 {
        const colorPicker = this._colorPickers[name];
        if (!colorPicker) return Color3.White();

        return (colorPicker as ColorPicker).value;
    }
}