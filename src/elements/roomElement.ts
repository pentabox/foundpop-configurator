import {
    AbstractMesh,
    Color3,
    Color4,
    Mesh,
    MeshBuilder, PBRMaterial, PBRMetallicRoughnessMaterial,
    Quaternion,
    Scene,
    SceneLoader,
    StandardMaterial,
    Texture,
    TransformNode,
    Vector3
} from "@babylonjs/core";
import {ExportVector, Placeable, PlaceableCategory, PlaceableRendering} from "../models/placeable";
import {SceneBase} from "../scenes/sceneBase";
import {UnitCalculator} from "../utils/unitCalculator";
import {DetailsWindow} from "../scenes/editing2d/details/detailsWindow";
import {AdvancedDynamicTexture} from "@babylonjs/gui";
import {DeskDetailsWindow} from "../scenes/editing2d/details/deskDetailsWindow";
import { Strings } from "../utils/strings";
import { RoomElements } from "../data/roomElements";

export interface ExportRoomElement {
    Name: string,
    Category: PlaceableCategory,
    Rotation: ExportVector,
    Position: ExportVector
}

export class RoomElement extends Placeable {
    private _texture: Texture | null = null;
    private _loadedMesh: AbstractMesh | null = null;
    private _transformNode3d: TransformNode | null = null;

    constructor(Name: string,
                Category: PlaceableCategory,
                MenuImagePath: string,
                ModelPath: string,
                TexturePath: string,
                public SizeInUnits: Vector3,
                public ModelRotation: Vector3 = Vector3.Zero(),
                public ModelScale: number = 0.4, 
                public Color: Color3 = Color3.White(),
                Pivot2d: Vector3 = Vector3.Zero(),
                Pivot3d: Vector3 = Vector3.Zero(),
                public MaxQuantityInRoom: number = 10,
                PositionInUnits: Vector3 = Vector3.Zero(),
                Rotation: Vector3 = Vector3.Zero()) {
        super(Name, Category, MenuImagePath, ModelPath, TexturePath);
        this.Pivot2d = Pivot2d;
        this.Pivot3d = Pivot3d;

        this.PositionInUnits = PositionInUnits;
    }

    public clone(): RoomElement {
        return new RoomElement(
            this.Name,
            this.Category,
            this.MenuImagePath,
            this.ModelPath,
            this.TexturePath,
            this.SizeInUnits,
            this.ModelRotation,
            this.ModelScale,
            this.Color,
            this.Pivot2d,
            this.Pivot3d,
            this.MaxQuantityInRoom,
            this.PositionInUnits,
            this.Rotation
        );
    }

    public toJson(): ExportRoomElement {
        const json: ExportRoomElement = {
            Name: this.Name,
            Category: this.Category,
            Position: UnitCalculator.vectorToJson(this.PositionInUnits),
            Rotation: UnitCalculator.vectorToJson(this.Rotation)
        };
        return json;
    }

    public static fromJson(element: ExportRoomElement) {
        const roomElement = RoomElements.find(e => e.Category === element.Category && e.Name === element.Name)!;
        const el = new RoomElement(
            element.Name,
            element.Category,
            roomElement.MenuImagePath,
            roomElement.ModelPath,
            roomElement.TexturePath,
            roomElement.SizeInUnits,
            roomElement.ModelRotation,
            roomElement.ModelScale,
            Color3.White(),
            roomElement.Pivot2d,
            roomElement.Pivot3d,
            roomElement.MaxQuantityInRoom
        );
        el.Rotation = new Vector3(element.Rotation.X, element.Rotation.Y, element.Rotation.Z);
        el.PositionInUnits = new Vector3(element.Position.X, element.Position.Y, element.Position.Z);

        return el;
    }

    public render2D(scene: SceneBase) {
        super.render2D(scene);

        this._createElementSprite();
        this._setupPivot2d();

        this.setPositionInUnits2d(this.PositionInUnits);

        if (this._transformNode)
            this._transformNode.rotation = this.Rotation;
    }

    public render3D(scene: SceneBase) {
        super.render3D(scene);

        if (!this._scene || !this._scene.Scene) return;

        if (!this._transformNode3d) {
            this._transformNode3d = this._scene.Scene.getMeshByName(this.Name + "_TransformNode3d");
        }
        if (!this._transformNode3d) {
            this._transformNode3d = new TransformNode(this.Name + "_TransformNode3d", scene.Scene);
        }

        this._createElementModel();

        if (this._transformNode3d)
            this._transformNode3d.rotation = this.Rotation3d;
    }

    private _loadMesh(): Promise<AbstractMesh> {
        return SceneLoader.ImportMeshAsync(null, "./assets/models/", this.ModelPath, this._scene?.Scene)
            .then((result) => {
                const mesh = result.meshes[1];
                mesh.isPickable = false;
                mesh.metadata = {placeable: this};

                //mesh.enableEdgesRendering();
                //mesh.edgesWidth = 0.5;
                //mesh.edgesColor = Color4.FromColor3(Color3.Black());
                mesh.isVisible = false;

                const pbr = (mesh.material as PBRMaterial);
                if (pbr) {
                    pbr.ambientColor = new Color3(229 / 255, 229 / 255, 229 / 255);
                }

                for (let i = 2; i < result.meshes.length; ++i) {
                    const m = result.meshes[i] as Mesh;
                    if (!m) continue;

                    const pbr = (m.material as PBRMaterial);
                    if (pbr) {
                        pbr.ambientColor = new Color3(229 / 255, 229 / 255, 229 / 255);
                    }

                    m.setParent(mesh);
                    m.isVisible = false;
                }

                this._loadedMesh = mesh;
                this._loadedMesh.setParent(null);
                this._loadedMesh.rotation = this.ModelRotation;

                this._resizeLoadedMesh();

                result.meshes[0].dispose();

                return mesh;
            });
    }

    public getTransformNode3d(): TransformNode | null {
        return this._transformNode3d;
    }

    private _resizeLoadedMesh() {
        if (!this._loadedMesh) return;

        const expectedSize = UnitCalculator.unitToBabylon(this.SizeInUnits);
        const currentSize = this._loadedMesh.getBoundingInfo().boundingBox.extendSizeWorld.clone();

        const scaleFactor = this.ModelScale;
        const scale = Vector3.Zero(); // * 0.5
        scale.x = scaleFactor; 
        scale.y = scaleFactor;
        scale.z = scaleFactor;

        this._loadedMesh.scaling = scale;
    }

    private _createElementSprite() {
        if (this._loadedMesh) {
            this._loadedMesh.isVisible = false;

            for (const mesh of this._loadedMesh.getChildMeshes()) {
                mesh.isVisible = false;
            }
        } else {
            this._loadMesh();
        }
        if (this._spriteMaterial) {
            this._spriteMaterial.dispose();
        }
        if (this._spriteMesh) {
            this._spriteMesh.dispose();
        }

        this._spriteMaterial = new StandardMaterial(this.Name + "_PlaneMat", this._scene?.Scene as Scene);

        if (!this._texture)
            this._texture = new Texture(this.TexturePath, this._scene?.Scene as Scene);

        this._spriteMaterial.diffuseTexture = this._texture;
        this._spriteMaterial.diffuseColor = this.Color;

        const size = UnitCalculator.unitToBabylon(this.SizeInUnits);
        const plane = MeshBuilder.CreateBox(this.Name + "_Plane", {
            width: size.x,
            height: size.z,
            depth: 0.01,
            sideOrientation: Mesh.DOUBLESIDE
        }, this._scene?.Scene);

        plane.material = this._spriteMaterial;
        plane.isPickable = false;
        //plane.enableEdgesRendering();
        //plane.edgesWidth = 0.5;
        //plane.edgesColor = Color4.FromColor3(Color3.Black());

        this._spriteMesh = plane;
        plane.checkCollisions = false;

        //plane.rotation.x = Math.PI / 2;
        plane.metadata = { placeable: this };
    }

    protected _getCurrentMesh(): AbstractMesh | null {
        let mesh: AbstractMesh | null = null;
        switch (this.Rendering) {
            case PlaceableRendering.TwoD:
                mesh = this._spriteMesh;
                break;
            case PlaceableRendering.ThreeD:
                mesh = this._loadedMesh;
                break;
        }
        return mesh;
    }

    private _createElementModel() {
        if (this._spriteMesh) {
            this._spriteMesh.dispose();
        }
        if (this._meshMaterial) {
            this._meshMaterial.dispose();
        }
        if (!this._loadedMesh) {
            this._loadMesh().then((mesh) => {
                mesh.isVisible = true;
                mesh.isPickable = true;

                this._setupPivot3d(mesh);
                this.setPositionInUnits3d(this.PositionInUnits3d);
                this._setupShadows(mesh);
            });
        } else {
            this._loadedMesh.isVisible = true;

            for (const mesh of this._loadedMesh.getChildMeshes()) {
                mesh.isVisible = true;
            }

            this._loadedMesh.isPickable = true;

            this._setupPivot3d(this._loadedMesh, this._transformNode3d);
            this.setPositionInUnits3d(this.PositionInUnits3d, this._transformNode3d);
            this._setupShadows(this._loadedMesh);
        }
    }

    public getDetailsWindow(gui: AdvancedDynamicTexture): DetailsWindow | null {
        let window: DetailsWindow | null = null;
        switch (this.Category) {
            case PlaceableCategory.DESK:
                window = new DeskDetailsWindow(this.Name + "_DeskDetails", this, gui);
                break;
        }
        return window;
    }
}