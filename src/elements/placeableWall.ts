import {ExportVector, Placeable, PlaceableCategory} from "../models/placeable";
import {
    Color3,
    Color4,
    Mesh,
    MeshBuilder,
    PBRMetallicRoughnessMaterial, Ray,
    Scene,
    Vector3
} from "@babylonjs/core";
import {RoomWall, WallOrientation} from "../models/roomShape";
import {SceneBase} from "../scenes/sceneBase";
import {AdvancedDynamicTexture} from "@babylonjs/gui";
import {DetailsWindow} from "../scenes/editing2d/details/detailsWindow";
import {WallDetailsWindow} from "../scenes/editing2d/details/wallDetailsWindow";
import {UnitCalculator} from "../utils/unitCalculator";

export interface ExportWall {
    Orientation: WallOrientation,
    Length: number,
    Width: number,
    Height: number,
    Position: ExportVector,
    Rotation: ExportVector
}

export class PlaceableWall extends Placeable {
    private _wallData: RoomWall;

    constructor(Name: string,
                public StartingOrientation: WallOrientation,
                MenuImagePath: string,
                ModelPath: string,
                public LengthInUnits: number,
                public WidthInUnits: number = 1,
                public HeightInUnits: number = 0.5,
                public Color: Color3 = Color3.Black(),
                PositionInUnits: Vector3 = Vector3.Zero(),
                Rotation: Vector3 = Vector3.Zero()) {
        super(Name, PlaceableCategory.WALL, MenuImagePath, ModelPath, "");

        let start = Vector3.Zero();
        let end = Vector3.Zero();

        switch (StartingOrientation) {
            case WallOrientation.HORIZONTAL:
                end.x = LengthInUnits * 0.5;
                break;
            case WallOrientation.VERTICAL:
                end.z = LengthInUnits * 0.5;
                break;
        }

        this._wallData = new RoomWall(start, end, 0, "Additional", false);
        this.Pivot2d = new Vector3(-1, 1, 1);
        this.Pivot3d = new Vector3(-1, 2, -1);

        this.PositionInUnits = PositionInUnits;
        this.Rotation = Rotation;
    }

    public clone(): PlaceableWall {
        return new PlaceableWall(
            this.Name,
            this.StartingOrientation,
            this.MenuImagePath,
            this.ModelPath,
            this.LengthInUnits,
            this.WidthInUnits,
            this.HeightInUnits,
            this.Color,
            this.PositionInUnits,
            this.Rotation
        );
    }

    public static fromJson(wall: ExportWall) {
        const w = new PlaceableWall(
            "ImportedWall",
            wall.Orientation,
            "",
            "",
            wall.Length,
            wall.Width,
            wall.Height,
            Color3.Black(),
            new Vector3(wall.Position.X, wall.Position.Y, wall.Position.Z)
        );
        w.Rotation = new Vector3(wall.Rotation.X, wall.Rotation.Y, wall.Rotation.Z);
        return w;
    }

    public toJson(): ExportWall {
        let json: ExportWall = {
            Orientation: this.StartingOrientation,
            Width: this.WidthInUnits,
            Height: this.HeightInUnits,
            Length: this.LengthInUnits,
            Position: UnitCalculator.vectorToJson(this.PositionInUnits),
            Rotation: UnitCalculator.vectorToJson(this.Rotation)
        };
        return json;
    }

    public getRays(position: Vector3): Ray[] {
        const rays: Ray[] = [];
        if (!this._spriteMesh) return rays;

        const pivot = this.Pivot2d.clone();
        let tmp = 0;
        let yMult = 1;
        let xMult = 1;
        const zRot = this.Rotation.z % (Math.PI * 2);
        switch (zRot) {
            case Math.PI / 2:
                tmp = pivot.y;
                pivot.y = -pivot.x;
                pivot.x = tmp;

                break;
            case (Math.PI * 3) / 2:
                tmp = pivot.y;
                pivot.y = -pivot.x;
                pivot.x = tmp;

                xMult = -1;

                break;
            case Math.PI:
                yMult = -1;

                break;
            case 0:
                break;
        }

        const distanceX = 2 - Math.abs(pivot.x);
        const distanceY = 2 - Math.abs(pivot.y);

        const extend = this._spriteMesh.getBoundingInfo().boundingBox.extendSizeWorld;

        const minX = position.x + (extend.x * pivot.x);
        const maxX = position.x + (extend.x * distanceX * xMult);

        const minY = position.y + (extend.y * pivot.y);
        const maxY = position.y + (extend.y * -distanceY * yMult);

        const direction = new Vector3(0, 0, -1);
        const length = 2;

        rays.push(new Ray(new Vector3(minX, minY, 0), direction, length));
        rays.push(new Ray(new Vector3(minX, maxY, 0), direction, length));
        rays.push(new Ray(new Vector3(maxX, minY, 0), direction, length));
        rays.push(new Ray(new Vector3(maxX, maxY, 0), direction, length));

        return rays;
    }

    public render2D(scene: SceneBase) {
        super.render2D(scene);

        this._createWall();
        this._setupPivot2d();

        this.setPositionInUnits2d(this.PositionInUnits);

        if (this._transformNode)
            this._transformNode.rotation = this.Rotation;
    }

    public render3D(scene: SceneBase) {
        super.render3D(scene);

        this._createWall(UnitCalculator.unitToBabylonSingle(this.HeightInUnits), 0, false);
        this._setupPivot3d();

        this.setPositionInUnits3d(this.PositionInUnits3d);

        this._setupShadows();

        if (this._transformNode)
            this._transformNode.rotation = this.Rotation3d;
    }

    private _createWall(depth: number = 0.01, xRotation: number = Math.PI / 2, edges: boolean = true) {
        if (this._spriteMesh) {
            this._spriteMesh.dispose();
        }
        if (this._meshMaterial) {
            this._meshMaterial.dispose();
        }

        this._meshMaterial = new PBRMetallicRoughnessMaterial(this.Name + "_PolygonMat", this._scene?.Scene as Scene);
        this._meshMaterial.baseColor = this.Color;
        this._meshMaterial.metallic = 0;
        this._meshMaterial.roughness = 1;
        //this._meshMaterial.emissiveColor = new Color3(213 / 255, 213 / 255, 213 / 255);

        switch (this.StartingOrientation) {
            case WallOrientation.HORIZONTAL:
                this._wallData.EndInUnits.x = this.LengthInUnits * 0.5;
                break;
            case WallOrientation.VERTICAL:
                this._wallData.EndInUnits.z = this.LengthInUnits * 0.5;
                break;
        }

        const shape = this._getShape();
        const polygon = MeshBuilder.ExtrudePolygon(this.Name + "_Polygon", {
            shape: shape,
            depth: depth,
            sideOrientation: Mesh.DOUBLESIDE
        }, this._scene?.Scene);

        polygon.material = this._meshMaterial;
        polygon.isPickable = true;

        if (edges) {
            polygon.enableEdgesRendering();
            polygon.edgesWidth = 1.5;
            polygon.edgesColor = Color4.FromColor3(Color3.Black());
        }

        //polygon.setPivotMatrix(Matrix.Translation(0, 1, 0), false);

        this._spriteMesh = polygon;
        polygon.rotation.x = xRotation;
        polygon.checkCollisions = false;

        polygon.metadata = { placeable: this };
    }

    private _getShape(): Vector3[] {
        const shape: Vector3[] = [];
        const start = this._wallData.StartInUnits;
        const end = this._wallData.EndInUnits;

        shape.push(start);
        shape.push(end);

        const orientation = this._wallData.getOrientation();
        let bottomRight = Vector3.Zero();
        switch (orientation) {
            case WallOrientation.VERTICAL:
                bottomRight = end.clone();
                bottomRight.x += this.WidthInUnits * 0.5;

                shape.push(bottomRight);

                let topRight = start.clone();
                topRight.x = bottomRight.x;

                shape.push(topRight);

                break;
            case WallOrientation.HORIZONTAL:
                bottomRight = end.clone();
                bottomRight.z += this.WidthInUnits * 0.5;

                shape.push(bottomRight);

                const bottomLeft = start.clone();
                bottomLeft.z = bottomRight.z;

                shape.push(bottomLeft);

                break;
        }

        shape.push(start);
        return shape;
    }

    public getDetailsWindow(gui: AdvancedDynamicTexture): DetailsWindow | null {
        return new WallDetailsWindow(this.Name + "_WallDetails", this, gui);
    }
}