import {RoomShape, RoomWall, WallOrientation} from "./roomShape";
import {ExportRoomElement, RoomElement} from "../elements/roomElement";
import {SceneBase} from "../scenes/sceneBase";
import {
    Color3,
    Color4,
    Mesh,
    MeshBuilder,
    PBRMetallicRoughnessMaterial,
    Scene,
    Tools,
    Vector3
} from "@babylonjs/core";
import {ExportWall, PlaceableWall} from "../elements/placeableWall";
import { ExportVector } from "./placeable";
import { UnitCalculator } from "../utils/unitCalculator";

export interface RoomData {
    AdditionalWalls: ExportWall[];
    Elements: ExportRoomElement[];
    Completed: boolean;
    Shape: string;
    ShapePath: ExportVector[];
    PlanNumber: string;

    Contact?: Contact;
    Quote?: Quote;
}

export interface Contact {
    Name: string;
    Company: string;
    Email: string;
}

export interface Quote {
    Name: string;
    Email: string;
    Company: string;
    Phone: string;
    RentalStart: string;
    RentalEnd: string;
    Notes: string;
    Location: string;
}

export class Room {
    public AdditionalWalls: PlaceableWall[] = [];
    public Elements: RoomElement[] = [];
    public Completed: boolean = false;

    public Contact?: Contact;
    public Quote?: Quote;

    public FloorPolygon: Mesh | null = null;

    private _wallBlockers: Mesh[] = [];
    private _floorMaterial: PBRMetallicRoughnessMaterial | null = null;

    constructor(public Shape: RoomShape,
                public PlanNumber: string) {

    }

    public getBlockers(): Mesh[] {
        return this._wallBlockers;
    }

    public getWalls(): RoomWall[] {
        const base = this.Shape.Walls;
        return base;
    }

    public addWall(wall: PlaceableWall) {
        this.AdditionalWalls.push(wall);
    }

    public removeWall(wall: PlaceableWall) {
        this.AdditionalWalls.splice(this.AdditionalWalls.indexOf(wall), 1);
        wall.dispose();
    }

    public addElement(element: RoomElement) {
        this.Elements.push(element);
    }

    public removeElement(element: RoomElement) {
        this.Elements.splice(this.Elements.indexOf(element), 1);
        element.dispose();
    }

    public render2D(scene: SceneBase): Mesh {
        this.dispose();

        const floor = this._renderFloor(scene);
        //this._renderWallBlockers(scene);

        for(const wall of this.AdditionalWalls) {
            wall.render2D(scene);
        }
        for (const element of this.Elements) {
            element.render2D(scene);
            element.setPickable();
        }

        return floor;
    }

    public render3D(scene: SceneBase): Mesh {
        this.dispose();

        const floor = this._renderFloor(scene, 0);
        for (const wall of this.AdditionalWalls) {
            wall.render3D(scene);
        }
        for (const element of this.Elements) {
            element.render3D(scene);
        }

        return floor;
    }

    public dispose() {
        if (this.FloorPolygon) {
            this.FloorPolygon.dispose();
        }
        this.FloorPolygon = null;

        if (this._floorMaterial) {
            this._floorMaterial.dispose();
        }
        this._floorMaterial = null;

        for (const wall of this.AdditionalWalls) {
            wall.dispose();
        }
        for (const element of this.Elements) {
            element.dispose();
        }
    }

    public clearSelection() {
        for (const el of this.Elements) {
            el.removeSelection();
        }
        for (const el of this.AdditionalWalls) {
            el.removeSelection();
        }
    }

    private _renderWallBlockers(scene: SceneBase) {
        for (const wall of this.Shape.Walls) {
            const blocker = MeshBuilder.CreateBox("RoomBlocker", {
                width: wall.getLengthBabylon(),
                height: 0.4,
                depth: 0.5
            }, scene.Scene);

            const middle = wall.getMiddlePoint();

            blocker.setParent(this.FloorPolygon);

            blocker.rotation.y = wall.Rotation;
            blocker.position = middle;
            blocker.isPickable = false;
            blocker.isVisible = false;
            blocker.checkCollisions = true;
            blocker.metadata = {wall: wall};

            wall.Blocker = blocker;

            this._wallBlockers.push(blocker);
        }
    }

    private _renderFloor(scene: SceneBase, xRotation: number = Math.PI / 2): Mesh {
        const mat = new PBRMetallicRoughnessMaterial("Floor_PolygonMat", scene.Scene as Scene);
        mat.baseColor = Color3.White();
        mat.metallic = 0;
        mat.roughness = 1;
        mat.emissiveColor = new Color3(213 / 255, 213 / 255, 213 / 255);

        this._floorMaterial = mat;

        const shape = this.Shape.getPath();
        const polygon = MeshBuilder.ExtrudePolygon("Floor_Polygon", {
            shape: shape,
            depth: 0.001,
            sideOrientation: Mesh.DOUBLESIDE
        }, scene.Scene);

        polygon.material = mat;
        polygon.rotation.x = xRotation;
        polygon.isPickable = true;
        polygon.receiveShadows = true;

        polygon.enableEdgesRendering();
        polygon.edgesWidth = 3;
        polygon.edgesColor = Color4.FromColor3(Color3.FromInts(199, 208, 216));

        this.FloorPolygon = polygon;
        return polygon;
    }

    public scaleWall(wall: RoomWall, length: number, ignoreOpposed: boolean = false) {
        const previousLength = wall.getLength();

        const diff = length - previousLength;
        const factor = length / previousLength;

        const startBackup = wall.StartInUnits.clone();
        const endBackup = wall.EndInUnits.clone();

        switch (wall.getOrientation()) {
            case WallOrientation.VERTICAL:
                wall.StartInUnits.z *= factor;
                wall.EndInUnits.z *= factor;
                break;
            case WallOrientation.HORIZONTAL:
                wall.StartInUnits.x *= factor;
                wall.EndInUnits.x *= factor;
                break;
        }

        if (!wall.ScaleAroundCenter) {
            switch (wall.getOrientation()) {
                case WallOrientation.VERTICAL:
                    if (wall.StartInUnits.z < wall.EndInUnits.z) {
                        const zDiff = endBackup.z - wall.EndInUnits.z;
                        wall.EndInUnits.z = endBackup.z;
                        wall.StartInUnits.z += zDiff;
                    } else {
                        const zDiff = startBackup.z - wall.StartInUnits.z;
                        wall.StartInUnits.z = startBackup.z;
                        wall.EndInUnits.z += zDiff;
                    }
                    break;
                case WallOrientation.HORIZONTAL:
                    if (wall.StartInUnits.x < wall.EndInUnits.x) {
                        const xDiff = endBackup.x - wall.EndInUnits.x;
                        wall.EndInUnits.x = endBackup.x;
                        wall.StartInUnits.x += xDiff;
                    } else {
                        const xDiff = startBackup.x - wall.StartInUnits.x;
                        wall.StartInUnits.x = startBackup.x;
                        wall.EndInUnits.x += xDiff;
                    }
                    break;
            }
        }

        if (!ignoreOpposed) {
            this._fixAdjacentWalls([], wall, wall);
        }
    }

    private _fixAdjacentWalls(starting: RoomWall[], changedWall: RoomWall, currentWall: RoomWall, propagateToChildren = true) {
        if (starting.length === this.Shape.Walls.length) return;

        const wallIdx = this.Shape.Walls.indexOf(currentWall);

        let previousIdx = wallIdx - 1;
        if (previousIdx < 0)
            previousIdx = this.Shape.Walls.length - 1;
        let nextIdx = wallIdx + 1;
        if (nextIdx > this.Shape.Walls.length - 1)
            nextIdx = 0;

        const previousWall = this.Shape.Walls[previousIdx];
        previousWall.setEndingPoint(currentWall.StartInUnits);

        const nextWall = this.Shape.Walls[nextIdx];
        nextWall.setStartingPoint(currentWall.EndInUnits);

        starting.push(currentWall);

        if (!starting.includes(previousWall)) {
            previousWall.setEndingPoint(currentWall.StartInUnits);
            if (propagateToChildren)
                this._fixAdjacentWalls(starting, changedWall, previousWall, false);
        }

        if (!starting.includes(nextWall)) {
            nextWall.setStartingPoint(currentWall.EndInUnits);
            if (propagateToChildren)
                this._fixAdjacentWalls(starting, changedWall, nextWall, false);
        }
    }

    public toJson(): RoomData {
        const additionalWalls: ExportWall[] = [];
        for (const wall of this.AdditionalWalls) {
            additionalWalls.push(wall.toJson());
        }

        const elements: ExportRoomElement[] = [];
        for (const element of this.Elements) {
            elements.push(element.toJson());
        }

        const shapePath: ExportVector[] = [];
        for (const path of this.Shape.getPath()) {
            shapePath.push(UnitCalculator.vectorToJson(path));
        }

        let json: RoomData = {
            PlanNumber: this.PlanNumber,
            Shape: this.Shape.Name,
            ShapePath: shapePath,
            AdditionalWalls: additionalWalls,
            Elements: elements,
            Completed: this.Completed
        };
        return json;
    }

    public toWebM() {
        // TODO
        // https://doc.babylonjs.com/divingDeeper/scene/renderToVideo
    }
}