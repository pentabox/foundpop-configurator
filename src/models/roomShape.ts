import {Mesh, Quaternion, Vector3} from "@babylonjs/core";
import {Placeable} from "./placeable";
import {UnitCalculator} from "../utils/unitCalculator";

export enum WallOrientation {
    HORIZONTAL,
    VERTICAL,
    UNKNOWN
}

export class RoomWall {
    public Blocker: Mesh | null = null;

    constructor(public StartInUnits: Vector3,
                public EndInUnits: Vector3,
                public Rotation: number = 0,
                public WallGroup: string = "Default",
                public ScaleAroundCenter: boolean = true) {
    }

    public getLength(): number {
        return Vector3.Distance(this.EndInUnits, this.StartInUnits);
    }

    public getLengthBabylon(): number {
        return Vector3.Distance(UnitCalculator.unitToBabylon(this.EndInUnits), UnitCalculator.unitToBabylon(this.StartInUnits));
    }

    public getOrientation(): WallOrientation {
        if (Math.abs(this.EndInUnits.x - this.StartInUnits.x) > 0)
            return WallOrientation.HORIZONTAL;
        if (Math.abs(this.EndInUnits.z - this.StartInUnits.z) > 0)
            return WallOrientation.VERTICAL;

        return WallOrientation.UNKNOWN;
    }

    public getMiddlePoint(): Vector3 {
        const endBabylon = UnitCalculator.unitToBabylon(this.EndInUnits);
        const startBabylon = UnitCalculator.unitToBabylon(this.StartInUnits);

        let dir = endBabylon.subtract(startBabylon);
        const length = dir.length();
        dir = dir.normalize();
        dir = dir.scale(length * 0.5);
        const middle = startBabylon.add(dir);

        let normal = Vector3.Zero();
        dir.rotateByQuaternionToRef(Quaternion.FromEulerAngles(0, Math.PI / 2, 0), normal);

        normal = normal.normalize().scale(0.2);
        return middle.add(normal);
    }

    public setStartingPoint(newStart: Vector3) {
        const orientation = this.getOrientation();

        this.StartInUnits.x = newStart.x;
        this.StartInUnits.z = newStart.z;

        switch (orientation) {
            case WallOrientation.VERTICAL:
                this.EndInUnits.x = newStart.x;
                break;
            case WallOrientation.HORIZONTAL:
                this.EndInUnits.z = newStart.z;
                break;
        }
    }

    public setEndingPoint(newEnd: Vector3) {
        const orientation = this.getOrientation();

        this.EndInUnits.x = newEnd.x;
        this.EndInUnits.z = newEnd.z;

        switch (orientation) {
            case WallOrientation.VERTICAL:
                this.StartInUnits.x = newEnd.x;
                break;
            case WallOrientation.HORIZONTAL:
                this.StartInUnits.z = newEnd.z;
                break;
        }
    }
}

export class RoomShape {
    constructor(public Name: string,
                public Image: string,
                public Walls: RoomWall[],
                public IgnoredGroups: string[] = []) {
    }

    public getPath(): Vector3[] {
        const path: Vector3[] = [];

        for (const wall of this.Walls) {
            path.push(UnitCalculator.unitToBabylon(wall.StartInUnits));
            path.push(UnitCalculator.unitToBabylon(wall.EndInUnits));
        }
        return path;
    }

    public getWallPoints(): Vector3[] {
        const points: Vector3[] = [];

        for (const wall of this.Walls) {
            points.push(wall.StartInUnits);
        }

        return points;
    }

    public getArea(): number {
        let area = 0;
        const points = this.getWallPoints();
        let j = points.length - 1;

        for (let i = 0; i < points.length; i++) {
            area += (points[j].x + points[i].x) * (points[j].z - points[i].z);
            j = i;
        }

        return Math.abs(area / 2);
    }

    public getWallsByGroup(includeIgnored: boolean = false): any {
        const groups: any = {};

        for (const wall of this.Walls) {
            if (!includeIgnored && this.IgnoredGroups.includes(wall.WallGroup))
                continue;

            if (!groups.hasOwnProperty(wall.WallGroup)) {
                groups[wall.WallGroup] = [];
            }
            groups[wall.WallGroup].push(wall);
        }

        return groups;
    }
}