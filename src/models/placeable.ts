import {
    AbstractMesh,
    Animation,
    BoundingInfo,
    Color3,
    HighlightLayer,
    IAnimationKey,
    Mesh, PBRMetallicRoughnessMaterial, PointerDragBehavior, Quaternion,
    Ray,
    StandardMaterial,
    TransformNode,
    Vector3
} from "@babylonjs/core";
import {SceneBase} from "../scenes/sceneBase";
import {UnitCalculator} from "../utils/unitCalculator";
import {RoomWall} from "./roomShape";
import {Editing2dScene} from "../scenes/editing2d/editing2dScene";
import {DetailsWindow} from "../scenes/editing2d/details/detailsWindow";
import {AdvancedDynamicTexture} from "@babylonjs/gui";
import { Strings } from "../utils/strings";
import { Constants } from "../utils/constants";

export interface ExportVector {
    X: number,
    Y: number,
    Z: number
}

export enum PlaceableCategory {
    WALL,
    SHELVING_UNIT,
    RAIL,
    DESK,
    FITTING_ROOM,
    MIRROR,
    TABLE,
    PLINTH,
    DISPLAY_CASE
}

export enum PlaceableRendering {
    TwoD,
    ThreeD
}

export class Placeable {
    public PositionInUnits: Vector3 = Vector3.Zero();
    public PositionInUnits3d: Vector3 = Vector3.Zero();
    public Rotation: Vector3 = Vector3.Zero();
    public Rotation3d: Vector3 = Vector3.Zero();
    public Pivot2d: Vector3 = new Vector3(0.5, 0.5, 0.5);
    public Pivot3d: Vector3 = new Vector3(0.5, 0.5, 0.5);
    public Rendering: PlaceableRendering = PlaceableRendering.TwoD;

    protected _meshMaterial: PBRMetallicRoughnessMaterial | null = null;
    protected _spriteMaterial: StandardMaterial | null = null;
    protected _spriteMesh: Mesh | null = null;
    protected _transformNode: TransformNode | null = null;
    protected _scene: SceneBase | null = null;
    protected _dragBehaviour: PointerDragBehavior | null = null;
    protected _positionBackup: Vector3 = Vector3.Zero();

    constructor(public Name: string,
                public Category: PlaceableCategory,
                public MenuImagePath: string,
                public ModelPath: string,
                public TexturePath: string) {
    }

    public render2D(scene: SceneBase) {
        this._scene = scene;
        this._transformNode = new TransformNode(this.Name + "_TransformNode", scene.Scene);
        this.Rendering = PlaceableRendering.TwoD;
    }

    public render3D(scene: SceneBase) {
        this._scene = scene;
        this._transformNode = new TransformNode(this.Name + "_TransformNode", scene.Scene);

        this.PositionInUnits3d = this.PositionInUnits.clone();
        this.PositionInUnits3d.z = -this.PositionInUnits3d.y;
        this.PositionInUnits3d.y = 0;

        this.Rotation3d = this.Rotation.clone();
        this.Rotation3d.y = this.Rotation3d.z;
        this.Rotation3d.z = 0;

        this.Rendering = PlaceableRendering.ThreeD;
    }

    public setPositionInUnits2d(position: Vector3) {
        if (!this._transformNode) return;

        this._transformNode.position = UnitCalculator.unitToBabylon(position);
        this.PositionInUnits = position;
    }

    public setPositionInUnits3d(position: Vector3, parent: TransformNode | null = null) {
        if (!parent) {
            parent = this._transformNode;
        }
        if (!parent) return;

        parent.position = UnitCalculator.unitToBabylon(position);
        this.PositionInUnits3d = position;
    }

    public getCenterOffset(): Vector3 {
        if (!this._transformNode) return Vector3.Zero();

        return this._transformNode.position;
    }

    protected _setupPivot2d() {
        if (!this._spriteMesh) return;

        const bounds = this._spriteMesh.getBoundingInfo();

        this._spriteMesh.position.x = bounds.boundingBox.extendSizeWorld.x * this.Pivot2d.x;
        this._spriteMesh.position.y = bounds.boundingBox.extendSizeWorld.y * this.Pivot2d.y;
        this._spriteMesh.position.z = bounds.boundingBox.extendSizeWorld.z * this.Pivot2d.z;
        this._spriteMesh.setParent(this._transformNode);

        this._spriteMesh.position.x = bounds.boundingBox.extendSizeWorld.x * this.Pivot2d.x;
        this._spriteMesh.position.y = bounds.boundingBox.extendSizeWorld.y * this.Pivot2d.y;
        this._spriteMesh.position.z = bounds.boundingBox.extendSizeWorld.z * this.Pivot2d.z;
    }

    protected _setupShadows(mesh: AbstractMesh | null = null) {
        if (!this._scene) return;

        if (!mesh) {
            mesh = this._spriteMesh;
        }
        if (!mesh) return;

        const editingScene = this._scene as Editing2dScene;
        if (!editingScene.ShadowGenerator) return;

        // @ts-ignore
        editingScene.ShadowGenerator.addShadowCaster(mesh, true);
    }

    protected _setupPivot3d(mesh: AbstractMesh | null = null, parent: TransformNode | null = null) {
        if (!mesh) {
            mesh = this._spriteMesh;
        }
        if (!mesh) return;

        if (!parent) {
            parent = this._transformNode;
        }

        const bounds = mesh.getBoundingInfo();

        mesh.position.x = bounds.boundingBox.extendSizeWorld.x * this.Pivot3d.x;
        mesh.position.y = bounds.boundingBox.extendSizeWorld.y * this.Pivot3d.y;
        mesh.position.z = bounds.boundingBox.extendSizeWorld.z * this.Pivot3d.z;
        mesh.setParent(parent);

        mesh.position.x = bounds.boundingBox.extendSizeWorld.x * this.Pivot3d.x;
        mesh.position.y = bounds.boundingBox.extendSizeWorld.y * this.Pivot3d.y;
        mesh.position.z = bounds.boundingBox.extendSizeWorld.z * this.Pivot3d.z;
    }

    public getCategoryName(): string {
        return Placeable.getCategoryName(this.Category);
    }

    public static getCategoryName(cat: PlaceableCategory): string {
        switch (cat) {
            case PlaceableCategory.SHELVING_UNIT:
                return Strings.SHELVING_UNIT_CATEGORY;
            case PlaceableCategory.RAIL:
                return Strings.RAIL_CATEGORY;
            case PlaceableCategory.DESK:
                return Strings.DESK_CATEGORY;
            case PlaceableCategory.FITTING_ROOM:
                return Strings.FITTING_ROOM_CATEGORY;
            case PlaceableCategory.MIRROR:
                return Strings.MIRROR_CATEGORY;
            case PlaceableCategory.TABLE:
                return Strings.TABLE_CATEGORY;
            case PlaceableCategory.PLINTH:
                return Strings.PLINTH_CATEGORY;
            case PlaceableCategory.DISPLAY_CASE:
                return Strings.DISPLAY_CASE_CATEGORY;
        }

        return "##INVALID CATEGORY##";
    }

    public static getCategoryImage(cat: PlaceableCategory): string {
        let base = "assets/images/categories/";

        switch (cat) {
            case PlaceableCategory.SHELVING_UNIT:
                base += "shelving_unit.jpg";
                break;
            case PlaceableCategory.RAIL:
                base += "rail.jpg";
                break;
            case PlaceableCategory.DESK:
                base += "desk.jpg";
                break;
            case PlaceableCategory.FITTING_ROOM:
                base += "fitting_room.jpg";
                break;
            case PlaceableCategory.MIRROR:
                base += "mirror.jpg";
                break;
            case PlaceableCategory.TABLE:
                base += "table.jpg";
                break;
            case PlaceableCategory.PLINTH:
                base += "plinth.jpg";
                break;
            case PlaceableCategory.DISPLAY_CASE:
                base += "display_case.jpg";
                break;
        }

        return base;
    }

    public getHalfSize(): Vector3 {
        if (!this._spriteMesh) return Vector3.Zero();

        return this._spriteMesh.getBoundingInfo().boundingBox.extendSizeWorld.scale(0.5);
    }

    public getSize(): Vector3 {
        if (!this._spriteMesh) return Vector3.Zero();

        return this._spriteMesh.getBoundingInfo().boundingBox.extendSizeWorld;
    }

    private _isMeshInsideBlocker(wall: RoomWall): boolean {
        if (!wall.Blocker) return false;
        if (!this._spriteMesh) return false;

        const meshMin = this._spriteMesh.getBoundingInfo().boundingBox.minimumWorld;
        const meshMax = this._spriteMesh.getBoundingInfo().boundingBox.maximumWorld;

        return this._isPointInsideBBox(meshMin.x, meshMin.y, wall.Blocker.getBoundingInfo()) ||
               this._isPointInsideBBox(meshMin.x, meshMax.y, wall.Blocker.getBoundingInfo()) ||
               this._isPointInsideBBox(meshMax.x, meshMin.y, wall.Blocker.getBoundingInfo()) ||
               this._isPointInsideBBox(meshMax.x, meshMax.y, wall.Blocker.getBoundingInfo());
    }

    private _isPointInsideBBox(x: number, y: number, bBox: BoundingInfo): boolean {
        const bBoxMin = bBox.boundingBox.minimumWorld;
        const bBoxMax = bBox.boundingBox.maximumWorld;

        return (x >= bBoxMin.x && x <= bBoxMax.x) && (y >= bBoxMin.y && y <= bBoxMax.y);
    }

    public setPickable() {
        if (!this._spriteMesh) return;

        this._spriteMesh.isPickable = true;
    }

    public setNotPickable() {
        if (!this._spriteMesh) return;

        this._spriteMesh.isPickable = false;
    }

    private _getPositionInsideWallGroup(position: Vector3, walls: RoomWall[]) {
        const halfSize = this.getSize();
        let minX = 99999, maxX = -999999, minY = 99999, maxY = -99999;

        for (const wall of walls) {
            const start = UnitCalculator.unitToBabylon(wall.StartInUnits);
            const end = UnitCalculator.unitToBabylon(wall.EndInUnits);

            let tmpMinX = Math.min(start.x, end.x);
            let tmpMaxX = Math.max(start.x, end.x);

            let tmpMinY = Math.min(start.z, end.z);
            if (minY > tmpMinY)
                minY = tmpMinY;

            let tmpMaxY = Math.max(start.z, end.z);
            if (maxY < tmpMaxY)
                maxY = tmpMaxY;

            if (minX > tmpMinX)
                minX = tmpMinX;
            if (maxX < tmpMaxX)
                maxX = tmpMaxX;
        }

        if ((position.x < minX || position.x > maxX) || (position.y < minY || position.y > maxY))
            return position;

        minX += halfSize.x;
        maxX -= halfSize.x;
        minY += halfSize.y;
        maxY -= halfSize.y;

        if (position.x < minX) {
            position.x = minX;
        }
        if (position.x > maxX) {
            position.x = maxX;
        }
        if (position.y < minY) {
            position.y = minY;
        }
        if (position.y > maxY) {
            position.y = maxY;
        }

        return position;
    }

    public setPosition(position: Vector3) {
        if (!this._transformNode) return;
        if (!this._spriteMesh) return;

        this._transformNode.position = position;
        this.PositionInUnits = UnitCalculator.babylonToUnit(position);
    }

    public setPositionWithSnap(position: Vector3) {
        if (!this._spriteMesh) return;

        const babylonSnap = UnitCalculator.unitToBabylonSingle(Constants.PLACEABLE_SNAP_STEP);
        
        const xOffset = position.x % babylonSnap;
        const yOffset = position.y % babylonSnap;

        if (xOffset > (babylonSnap / 2))
            position.x += babylonSnap - xOffset;
        else
            position.x -= xOffset;

        if (yOffset > (babylonSnap / 2))
            position.y += babylonSnap - yOffset;
        else 
            position.y -= yOffset;

        this.setPosition(position);
    }

    public getPosition(): Vector3 {
        if (!this._transformNode) return Vector3.Zero();

        return this._transformNode.position;
    }

    public getRays(position: Vector3): Ray[] {
        const rays: Ray[] = [];
        if (!this._spriteMesh) return rays;

        const pivot = this.Pivot2d.clone();
        let tmp = 0;
        let yMult = 1;
        let xMult = 1;
        const zRot = this.Rotation.z % (Math.PI * 2);
        switch (zRot) {
            case Math.PI / 2:
                tmp = pivot.y;
                pivot.y = -pivot.x;
                pivot.x = tmp;

                break;
            case (Math.PI * 3) / 2:
                tmp = pivot.y;
                pivot.y = -pivot.x;
                pivot.x = tmp;

                xMult = -1;

                break;
            case Math.PI:
                yMult = -1;

                break;
            case 0:
                break;
        }

        const distanceX = 2 - Math.abs(pivot.x);
        const distanceY = 2 - Math.abs(pivot.y);

        const extend = this._spriteMesh.getBoundingInfo().boundingBox.extendSizeWorld;

        const minX = position.x -extend.x;
        const maxX = position.x + extend.x;

        const minY = position.y - extend.y;
        const maxY = position.y + extend.y;

        const direction = new Vector3(0, 0, -1);
        const length = 2;

        rays.push(new Ray(new Vector3(minX, minY, 0), direction, length));
        rays.push(new Ray(new Vector3(minX, maxY, 0), direction, length));
        rays.push(new Ray(new Vector3(maxX, minY, 0), direction, length));
        rays.push(new Ray(new Vector3(maxX, maxY, 0), direction, length));

        return rays;
    }

    public rotateZ(euler: number) {
        if (!this._spriteMesh) return;
        if (!this._transformNode) return;

        this._transformNode.rotation.z += euler;
        this.Rotation = this._transformNode.rotation;
    }

    public rotateY(euler: number) {
        if (!this._spriteMesh) return;
        if (!this._transformNode) return;

        this._transformNode.rotation.y += euler;
        this.Rotation3d = this._transformNode.rotation;
    }

    public dispose() {
        const editingScene = this._scene as Editing2dScene;
        if (editingScene && this._spriteMesh && editingScene.ShadowGenerator) {
            editingScene.ShadowGenerator.removeShadowCaster(this._spriteMesh, true);
        }

        if (this._spriteMesh)
            this._spriteMesh.dispose();

        if (this._transformNode)
            this._transformNode.dispose();

        if (this._meshMaterial)
            this._meshMaterial.dispose();

        if (this._spriteMaterial)
            this._spriteMaterial.dispose();
    }

    protected _getCurrentMesh(): AbstractMesh | null {
        return this._spriteMesh;
    }

    private _internalHighlight(layer: HighlightLayer | null, color: Color3) {
        if (!layer) return;

        const mesh = this._getCurrentMesh() as Mesh;
        if (!mesh) {
            for (const abstractMesh of this._getCurrentMesh()!.getChildMeshes()) {
                const m = abstractMesh as Mesh;
                if (!m) continue;

                layer.addMesh(m, color);
            }
        } else {
            layer.addMesh(mesh, color);
        }
    }

    public select() {
        const mesh = this._getCurrentMesh() as Mesh;
        if (mesh) {
            mesh.overlayColor = Constants.SELECT_COLOR;
            mesh.renderOverlay = true;
        }

        for (const abstractMesh of mesh!.getChildMeshes()) {
            abstractMesh.overlayColor = Constants.SELECT_COLOR;
            abstractMesh.renderOverlay = true;
        }
    }

    public removeSelection() {
        const mesh = this._getCurrentMesh() as Mesh;
        if (mesh) {
            mesh.renderOverlay = false;
        }

        for (const abstractMesh of mesh!.getChildMeshes()) {
            abstractMesh.renderOverlay = false;
        }
    }

    public highlight(layer: HighlightLayer | null) {
        this._internalHighlight(layer, Constants.HIGHLIGHT_COLOR);
    }

    public getTransformNode3d(): TransformNode | null {
        return null;
    }

    public getTransformNode(): TransformNode | null {
        let tNode = this.getTransformNode3d();
        if (!tNode)
            tNode = this._transformNode;
        
        return tNode;
    }

    public async startRecordingAnimation(): Promise<void> {
        const tNode = this.getTransformNode();
        if (!tNode) return;

        const anim = new Animation(this.Name + "_VideoAnimation", "position.y", 60, Animation.ANIMATIONTYPE_FLOAT);
        const keys: IAnimationKey[] = [
            {
                frame: 0,
                value: tNode.position.y
            },
            {
                frame: 60,
                value: tNode.position.y - 5
            }
        ];
        anim.setKeys(keys);

        tNode.animations.push(anim);

        return new Promise((resolve, reject) => {
            this._scene?.Scene?.beginAnimation(tNode, 0, 60, false, 1, () => {
                resolve();
            });
        });
    }

    public removeHighlight(layer: HighlightLayer | null) {
        if (!layer) return;

        const mesh = this._getCurrentMesh() as Mesh;
        if (!mesh) {
            for (const abstractMesh of this._getCurrentMesh()!.getChildMeshes()) {
                const m = abstractMesh as Mesh;
                if (!m) continue;

                layer.removeMesh(m);
            }
        } else {       
            layer.removeMesh(mesh);
        }
    }

    public getDetailsWindow(gui: AdvancedDynamicTexture): DetailsWindow | null {
        return new DetailsWindow(this.Name, this, gui);
    }
}