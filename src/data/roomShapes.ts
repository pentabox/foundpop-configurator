import {Vector3} from "@babylonjs/core";
import {RoomShape, RoomWall} from "../models/roomShape";

export const RoomShapes: RoomShape[] = [
    new RoomShape("Square", "assets/images/shapes/square.png", [
        new RoomWall(new Vector3(5, 0, 5), new Vector3(-5, 0, 5), 0),
        new RoomWall(new Vector3(-5, 0, 5), new Vector3(-5, 0, -5), Math.PI / 2),
        new RoomWall(new Vector3(-5, 0, -5), new Vector3(5, 0, -5), 0),
        new RoomWall(new Vector3(5, 0, -5), new Vector3(5, 0, 5), -Math.PI / 2)
    ]),
    new RoomShape("Rectangle1", "assets/images/shapes/rectangle.png", [
        new RoomWall(new Vector3(5, 0, 2.5), new Vector3(-5, 0, 2.5), 0),
        new RoomWall(new Vector3(-5, 0, 2.5), new Vector3(-5, 0, -2.5), Math.PI / 2),
        new RoomWall(new Vector3(-5, 0, -2.5), new Vector3(5, 0, -2.5), 0),
        new RoomWall(new Vector3(5, 0, -2.5), new Vector3(5, 0, 2.5), -Math.PI / 2)
    ]),
    new RoomShape("Rectangle2", "assets/images/shapes/rectangle2.png", [
        new RoomWall(new Vector3(2.5, 0, 5), new Vector3(-2.5, 0, 5), 0),
        new RoomWall(new Vector3(-2.5, 0, 5), new Vector3(-2.5, 0, -5), Math.PI / 2),
        new RoomWall(new Vector3(-2.5, 0, -5), new Vector3(2.5, 0, -5), 0),
        new RoomWall(new Vector3(2.5, 0, -5), new Vector3(2.5, 0, 5), -Math.PI / 2)
    ]),
    new RoomShape("L1", "assets/images/shapes/Lshape1.png", [
        new RoomWall(new Vector3(2.5, 0, 5), new Vector3(-7.5, 0, 5), 0, "Lower", false),
        new RoomWall(new Vector3(-7.5, 0, 5), new Vector3(-7.5, 0, 0), Math.PI / 2, "Lower", false),
        new RoomWall(new Vector3(-7.5, 0, 0), new Vector3(-2.5, 0, 0), 0, "Default", false),
        new RoomWall(new Vector3(-2.5, 0, 0), new Vector3(-2.5, 0, -5), Math.PI / 2, "Upper", false),
        new RoomWall(new Vector3(-2.5, 0, -5), new Vector3(2.5, 0, -5), 0, "Upper", false),
        new RoomWall(new Vector3(2.5, 0, -5), new Vector3(2.5, 0, 5), -Math.PI / 2, "Default", false)
    ], ["Default"]),
    new RoomShape("L2", "assets/images/shapes/Lshape2.png", [
        new RoomWall(new Vector3(2.5, 0, 5), new Vector3(-2.5, 0, 5), 0, "Lower", false),
        new RoomWall(new Vector3(-2.5, 0, 5), new Vector3(-2.5, 0, 0), Math.PI / 2, "Lower", false),
        new RoomWall(new Vector3(-2.5, 0, 0), new Vector3(-7.5, 0, 0), 0, "Default", false),
        new RoomWall(new Vector3(-7.5, 0, 0), new Vector3(-7.5, 0, -5), Math.PI / 2, "Upper", false),
        new RoomWall(new Vector3(-7.5, 0, -5), new Vector3(2.5, 0, -5), 0, "Upper", false),
        new RoomWall(new Vector3(2.5, 0, -5), new Vector3(2.5, 0, 5), -Math.PI / 2, "Default", false)
    ], ["Default"]),
    new RoomShape("L3", "assets/images/shapes/Lshape3.png", [
        new RoomWall(new Vector3(-2.5, 0, 5), new Vector3(-7.5, 0, 5), 0, "Lower", false),
        new RoomWall(new Vector3(-7.5, 0, 5), new Vector3(-7.5, 0, -5), Math.PI / 2, "Lower", false),
        new RoomWall(new Vector3(-7.5, 0, -5), new Vector3(2.5, 0, -5), 0, "Default", false),
        new RoomWall(new Vector3(2.5, 0, -5), new Vector3(2.5, 0, 0), Math.PI / 2, "Upper", false),
        new RoomWall(new Vector3(2.5, 0, 0), new Vector3(-2.5, 0, 0), 0, "Upper", false),
        new RoomWall(new Vector3(-2.5, 0, 0), new Vector3(-2.5, 0, 5), -Math.PI / 2, "Default", false)
    ], ["Default"]),
    new RoomShape("L4", "assets/images/shapes/Lshape4.png", [
        new RoomWall(new Vector3(2.5, 0, 5), new Vector3(-7.5, 0, 5), 0, "Lower", false),
        new RoomWall(new Vector3(-7.5, 0, 5), new Vector3(-7.5, 0, -5), Math.PI / 2, "Lower", false),
        new RoomWall(new Vector3(-7.5, 0, -5), new Vector3(-2.5, 0, -5), 0, "Default", false),
        new RoomWall(new Vector3(-2.5, 0, -5), new Vector3(-2.5, 0, 0), Math.PI / 2, "Upper", false),
        new RoomWall(new Vector3(-2.5, 0, 0), new Vector3(2.5, 0, 0), 0, "Upper", false),
        new RoomWall(new Vector3(2.5, 0, 0), new Vector3(2.5, 0, 5), -Math.PI / 2, "Default", false)
    ], ["Default"])
];