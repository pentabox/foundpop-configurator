import { ArcRotateCamera, Color3, Color4, Engine, Tools, Vector3, VideoRecorder } from "@babylonjs/core";
import "@babylonjs/core/Debug/debugLayer";
import "@babylonjs/inspector";
import FontFaceObserver from "fontfaceobserver";

import { SceneBase, States } from "./scenes/sceneBase";
import { FoundPopSceneLoader } from "./utils/sceneLoader";

import "./sass/style.sass";

import {FoundPopGUI} from "./gui/gui";
import {Room, RoomData} from "./models/room";
import {RoomShape} from "./models/roomShape";

import * as earcut from "earcut";
import {AdvancedDynamicTexture} from "@babylonjs/gui";
import {SceneGUI} from "./gui/sceneGui";
import { PlanManager } from "./utils/planManager";
import { FoundPopLoadingScreen } from "./gui/loading";
import { RoomShapes } from "./data/roomShapes";
import { PlaceableWall } from "./elements/placeableWall";
import { RoomElement } from "./elements/roomElement";
import { UnitCalculator } from "./utils/unitCalculator";
import { Editing2dScene } from "./scenes/editing2d/editing2dScene";
import { RoomExporter } from "./utils/roomExporter";
import { Constants, Endpoints } from "./utils/constants";
import { Editing2dGui } from "./scenes/editing2d/editing2dGui";
(window as any).earcut = earcut;

export class FoundPopConfigurator {
    public SceneLoader: FoundPopSceneLoader;
    public Gui: FoundPopGUI | null = null;

    public CreatedRoom: Room | null = null;

    private readonly _canvas: HTMLCanvasElement;
    private readonly _engine: Engine;
    private _currentScene: SceneBase | null = null;

    public State: States = States.MAIN_MENU;

    constructor() {
        this._canvas = this._setupCanvas();
        this._engine = this._setupEngine();
        this._engine.loadingScreen = new FoundPopLoadingScreen();
        this.SceneLoader = new FoundPopSceneLoader(this._engine, this);

        let state = this.State;
        if (this._isNotSupportedDevice())
            state = States.DEVICE_NOT_SUPPORTED;

        this._loadFonts().then(() => {
            this.SceneLoader.loadSceneForState(state).then(sceneBase => {
                this._currentScene = sceneBase;
    
                this.Gui = new FoundPopGUI(this, sceneBase?.hasSideMenu());
    
                if (sceneBase !== null) {
                    const sceneGui = sceneBase.getSceneGui();
                    if (sceneGui !== null) {
                        this.Gui.addControl(sceneGui.getContainer());
                    }
                }
            });
    
            this._eventListeners();
            this._mainLoop();
        });
    }

    private _isNotSupportedDevice(): boolean {
        let hasTouchScreen = false;

        if ("maxTouchPoints" in navigator) {
            hasTouchScreen = navigator.maxTouchPoints > 0;
        } else if ("msMaxTouchPoints" in navigator) {
            /* @ts-ignore */
            hasTouchScreen = navigator.msMaxTouchPoints > 0;
        } else {
            /* @ts-ignore */
            var mq = window.matchMedia && matchMedia("(pointer:coarse)");
            if (mq && mq.media === "(pointer:coarse)") {
                hasTouchScreen = !!mq.matches;
            } else if ('orientation' in window) {
                hasTouchScreen = true;
            } else {
                const agent = navigator.userAgent;
                hasTouchScreen = (
                    /\b(BlackBerry|webOS|iPhone|IEMobile)\b/i.test(agent) ||
                    /\b(Android|Windows Phone|iPad|iPod)\b/i.test(agent)
                );
            }
        }

        return hasTouchScreen;
    }

    private _isSafari(): boolean {
        return navigator.userAgent.indexOf("Safari") > -1 && navigator.userAgent.indexOf("Chrome") <= -1;
    }

    public showLoadingScreen() {
        this._engine.displayLoadingUI();
    }

    public hideLoadingScreen() {
        this._engine.hideLoadingUI();
    }

    public async createRoomPDF() {
        if (!this.CreatedRoom) return;
        if (!this.Gui || !this.Gui.GuiTexture) return;

        const editingScene = this._currentScene as Editing2dScene;
        const scene = this._currentScene!.Scene!;
        const prevClear = scene.clearColor;

        editingScene.to2d();
        (editingScene.getSceneGui() as Editing2dGui).disableFloorLabels();

        this.CreatedRoom.clearSelection();

        //scene.clearColor = new Color4(1, 1, 1, 1);
        scene.layers.splice(scene.layers.indexOf(this.Gui.GuiTexture.layer!), 1);
        scene.render();

        const area = this.CreatedRoom.Shape.getArea();

        const minZoom = 8;
        const maxZoom = 33;
        const diffZoom = maxZoom - minZoom;

        const minArea = Constants.MIN_WALL_LENGTH * Constants.MIN_WALL_LENGTH;
        const maxArea = Constants.MAX_WALL_LENGTH * Constants.MAX_WALL_LENGTH;
        const diffArea = maxArea - minArea;

        const coeff = diffZoom / diffArea;
        const zoom = (coeff * area) + minZoom;

        (editingScene.getCamera() as ArcRotateCamera).radius = zoom;

        const exporter = new RoomExporter(this.CreatedRoom);
        await exporter.load();

        scene.onReadyObservable.addOnce(() => {
            Tools.CreateScreenshot(this._engine, this._currentScene!.getCamera(), {
                height: this._engine.getRenderHeight(),
                width: this._engine.getRenderWidth()
            }, async (data2d) => {
                editingScene.to3d();
                (editingScene.getCamera() as ArcRotateCamera).radius = zoom;

                setTimeout(() => {
                    Tools.CreateScreenshot(this._engine, this._currentScene!.getCamera(), {
                        height: this._engine.getRenderHeight(),
                        width: this._engine.getRenderWidth()
                    }, async (data3d) => {
                        const blob = await exporter.savePDF(data2d, data3d, (this._engine.getRenderWidth() / 4) - 100, (this._engine.getRenderHeight() / 3) - 150);
                        if (blob)
                            this._sendPDFToServer(blob);

                        if (VideoRecorder.IsSupported(this._engine) && !this._isSafari())
                            await this._recordVideo();

                        (editingScene.getSceneGui() as Editing2dGui).enableFloorLabels();

                        scene.clearColor = prevClear;
                        scene.layers.push(this.Gui!.GuiTexture.layer!);
                    });
                }, 500);
            });
        });
    }

    private async _recordVideo(): Promise<void> {
        if (!VideoRecorder.IsSupported(this._engine)) return new Promise((resolve, reject) => {
            resolve();
        });

        for (const wall of this.CreatedRoom!.AdditionalWalls) {
            wall.getTransformNode()!.position.y += 5;
        }

        for (const element of this.CreatedRoom!.Elements) {
            element.getTransformNode()!.position.y += 5;
        }

        var recorder = new VideoRecorder(this._engine, {
            fps: 60,
            mimeType: "video/webm;codecs=h264",
            recordChunckSize: 3000
        });
        recorder.startRecording(null).then((blob: Blob) => {
            this._sendWebMToServer(blob);
        });

        for (const wall of this.CreatedRoom!.AdditionalWalls) {
            await wall.startRecordingAnimation();
        }

        for (const element of this.CreatedRoom!.Elements) {
            await element.startRecordingAnimation();
        }

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                recorder.stopRecording();
                resolve();
            }, 1000);
        });
    }

    private _sendBlobToServer(blob: Blob, url: string, contentType: string) {
        const auth = Buffer.from("admin:TDFDhu4POIIGDl573cYC").toString("base64");

        fetch(url, {
            method: "POST",
            headers: {
                "Accept": contentType,
                "Content-Type": contentType,
                "Authorization": "Basic " + auth
            },
            body: blob
        })
        .then(result => result.json())
        .then(json => {
            if (json["success"]) {

            } else {
                console.log(json["error"]);
                // TODO error window
            }
        })
        .catch(ex => {
            console.log(ex);
            //TODO error window
        });
    }

    private _sendPDFToServer(pdfBlob: Blob) {
        let url = Endpoints.UPLOAD_PDF;
        url = url.replace("{plan}", this.CreatedRoom!.PlanNumber);
        this._sendBlobToServer(pdfBlob, url, "application/pdf");
    }

    private _sendWebMToServer(webmBlob: Blob) {
        let url = Endpoints.UPLOAD_VIDEO;
        url = url.replace("{plan}", this.CreatedRoom!.PlanNumber);
        this._sendBlobToServer(webmBlob, url, "video/webm");
    }

    public goToState(state: States) {
        this.SceneLoader.loadSceneForState(state).then(sceneBase => {
            this._currentScene = sceneBase;
            this.State = state;

            this.Gui = new FoundPopGUI(this, sceneBase?.hasSideMenu(), sceneBase?.hasTopMenu());

            if (sceneBase !== null) {
                const sceneGui = sceneBase.getSceneGui();
                if (sceneGui !== null) {
                    this.Gui.addControl(sceneGui.getContainer());
                }
            }
        });
    }

    public createRoom(shape: RoomShape) {
        this.CreatedRoom = new Room(shape, PlanManager.newPlanNumber());

        this.goToState(States.EDITING_2D);
    }

    public loadRoomFromData(data: RoomData) {
        const shape = RoomShapes.find(s => s.Name === data.Shape);
        for (let i = 0; i < data.ShapePath.length; i = i + 2) {
            const start = data.ShapePath[i];
            const end = data.ShapePath[i + 1];

            const wallIdx = i / 2;
            shape!.Walls[wallIdx].StartInUnits = UnitCalculator.babylonToUnit(new Vector3(start.X, start.Y, start.Z));
            shape!.Walls[wallIdx].EndInUnits = UnitCalculator.babylonToUnit(new Vector3(end.X, end.Y, end.Z));
        }

        this.CreatedRoom = new Room(shape!, data.PlanNumber);
        this.CreatedRoom.Completed = true;
        
        if (data.Contact) {
            this.CreatedRoom.Contact = data.Contact;
        }
        if (data.Quote) {
            this.CreatedRoom.Quote = data.Quote;
        }

        for (const wall of data.AdditionalWalls) {
            const w = PlaceableWall.fromJson(wall);
            this.CreatedRoom.addWall(w);
        }

        for (const element of data.Elements) {
            const e = RoomElement.fromJson(element);
            this.CreatedRoom.addElement(e);
        }

        this.goToState(States.EDITING_2D);
    }

    public getSceneGUI(): SceneGUI | null {
        if (!this._currentScene) return null;

        return this._currentScene.getSceneGui();
    }

    public getScene(): SceneBase | null {
        return this._currentScene;
    }

    public getAdvancedGUITexture(): AdvancedDynamicTexture | null {
        if (!this.Gui) return null;

        return this.Gui.GuiTexture;
    }

    async _loadFonts() {
        return Promise.all([
            new FontFaceObserver("BasisGrotesquePro").load(),
            new FontFaceObserver("MinionPro").load()
        ]);
    }

    _eventListeners() {
        window.addEventListener("keydown", (ev) => {
           if (ev.shiftKey && ev.ctrlKey && ev.altKey) {
               if (this._currentScene?.Scene?.debugLayer.isVisible()) {
                   this._currentScene?.Scene?.debugLayer.hide();
               } else {
                   this._currentScene?.Scene?.debugLayer.show();
               }
           }
        });
        window.addEventListener("resize", () => {
            this._engine.resize();
            this.Gui?.GuiTexture.scaleTo(this._engine.getRenderWidth(), this._engine.getRenderHeight());
        });
    }

    _mainLoop() {
        this._engine.runRenderLoop(() => {
           this._currentScene?.Scene?.render();
        });
    }

    _setupCanvas() : HTMLCanvasElement {
        const canvas = document.createElement("canvas");
        canvas.id = "foundpop-configurator";

        document.body.appendChild(canvas);
        return canvas;
    }

    _setupEngine() : Engine {
        return new Engine(this._canvas, true, { stencil: true, preserveDrawingBuffer: true });
    }

    public resetCursor() {
        document.body.style.cursor = "";
    }

    public setCursor(type: string) {
        document.body.style.cursor = type;
    }
}

new FoundPopConfigurator();