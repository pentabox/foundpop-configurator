import { Observable } from "@babylonjs/core";
import { Placeable } from "../models/placeable";

export enum ActionType {
    DELETE_ELEMENT,
    CREATE_ELEMENT,
    MOVE_ELEMENT,
    ROTATE_ELEMENT,
}

export class HistoryEntry {
    constructor(public Type: ActionType,
                public TargetElement: Placeable | null = null,
                public OriginalElement: Placeable | null = null) 
    {

    }
}

export class HistoryManager {
    static HistorySize: number = 1;
    static HistoryEntries: HistoryEntry[] = [];

    public static onUndoRequested: Observable<HistoryEntry> = new Observable<HistoryEntry>();

    public static pushAction(type: ActionType, element: Placeable | null = null, original: Placeable | null = null) {
        if (this.HistoryEntries.length >= this.HistorySize) {
            this.HistoryEntries.splice(0, 1); // remove oldest entry
        }

        this.HistoryEntries.push(new HistoryEntry(
            type,
            element,
            original
        ));
    }

    public static undo() {
        const last = this.HistoryEntries.pop();
        if (!last) return;

        this.onUndoRequested.notifyObservers(last);
    }
}