import { Color3 } from "@babylonjs/core";

export const Constants = {
    MIN_WALL_LENGTH: 2,
    MAX_WALL_LENGTH: 50,
    MIN_WALL_SIZE: 0.1,
    MAX_WALL_SIZE: 10,

    PLAN_NUMBER_LENGTH: 10,
    PLAN_SHOWCASE_NAME: "showcase.webm",
    PLAN_PDF_NAME: "summary.pdf",
    PDF_TEMPLATE: "assets/templates/BlankPlan.pdf",

    PLACEABLE_SNAP_STEP: 0.1, 

    HIGHLIGHT_COLOR: Color3.White(),
    SELECT_COLOR: Color3.Green(),
};

export const Endpoints = {
    SAVE_ROOM: "https://foundpop.com/wp-json/foundpop/v1/upload_json/{plan}",
    GET_ROOM: "https://foundpop.com/wp-content/uploads/foundpop/{plan}.json",
    UPLOAD_VIDEO: "https://foundpop.com/wp-json/foundpop/v1/upload_webm/{plan}",
    UPLOAD_PDF: "https://foundpop.com/wp-json/foundpop/v1/upload_pdf/{plan}",
    REQUEST_QUOTE: "https://foundpop.com/wp-json/foundpop/v1/upload_json/{plan}",
};