import { Room, RoomData } from "../models/room";
import { Constants, Endpoints } from "./constants";

export class PlanManager {

    public static isPlanNumberValid(planNumber: string): boolean {
        if (planNumber.length != Constants.PLAN_NUMBER_LENGTH) 
            return false;

        var isNumeric = true;
        for (let i = 0; i < planNumber.length; ++i) {
            isNumeric = isNumeric && !isNaN(+planNumber[i].trim());
        }
        if (!isNumeric)
            return false;

        return true;
    }

    public static async getRoom(planNumber: string): Promise<RoomData | null | undefined> {
        let url = Endpoints.GET_ROOM;
        url = url.replace("{plan}", planNumber);
        return fetch(url, {
            method: "GET",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            }
        })
        .then(response => response.json())
        .then(room => {
            console.log(room);
            if (room["error"])
                return null;

            return room["Room"] as RoomData;
        })
        .catch (error => {
            console.log(error);
            return null;
        });
    }

    public static newPlanNumber(): string {
        var chars = "0123456789";
        var result = "";

        for (let i = Constants.PLAN_NUMBER_LENGTH; i > 0; --i) {
            result += chars[Math.floor(Math.random() * chars.length)];
        }

        return result;
    }
}