import { PDFDocument, PDFFont } from "pdf-lib";
import { Room } from "../models/room";
import { Constants } from "./constants";
import fontkit from "@pdf-lib/fontkit";
import { RoomElement } from "../elements/roomElement";

export class RoomExporter {
    private _pdf: PDFDocument | null = null;
    private _font: PDFFont | null = null;

    constructor(private _room: Room) {

    }

    private async _loadAsset(asset: string): Promise<Uint8Array> {
        return fetch(asset)
        .then(response => response.arrayBuffer())
        .then(response => new Uint8Array(response));
    }

    private _downloadBlob(blob: Blob, filename: string) {
        const url = URL.createObjectURL(blob);
        const a = document.createElement("a");
        a.href = url;
        a.download = filename || "download";

        const handler = () => {
            setTimeout(() => {
                URL.revokeObjectURL(url);
                a.removeEventListener("click", handler);
            }, 150);
        };
        a.addEventListener("click", handler, false);
        a.click();
    }

    private _crop(imgBase64: string, width: number, height: number, x: number, y: number, callback: (data: string) => void) {
        const resize_canvas = document.createElement("canvas");
        const orig = new Image();
        orig.src = imgBase64;
        orig.onload = () => {
            resize_canvas.width = width;
            resize_canvas.height = height;
            const ctx = resize_canvas.getContext("2d");
            ctx?.drawImage(orig, x, y, width, height, 0, 0, width, height);
            const newUri = resize_canvas.toDataURL("image/png").toString();
            callback(newUri);
        };
    }

    public async load() {
        const fontBytes = await this._loadAsset("assets/fonts/BasisGrotesquePro-Light.ttf");

        const bytes = await this._loadAsset(Constants.PDF_TEMPLATE);
        this._pdf = await PDFDocument.load(bytes);

        this._pdf.registerFontkit(fontkit);
        this._font = await this._pdf.embedFont(fontBytes);
    }

    public async addRoomImage(imgBase64: string, offsetX: number = -100, offsetY: number = 50) {
        if (!this._pdf) return;

        const image = await this._pdf.embedPng(imgBase64);
        const imageDims = image.scale(0.55);

        const page = this._pdf.getPage(0);

        page.drawImage(image, {
            width: imageDims.width,
            height: imageDims.height,
            x: (page.getWidth() / 2 - imageDims.width / 2) + offsetX,
            y: (page.getHeight() / 2 - imageDims.height / 2) + offsetY
        });
    }

    public async savePDF(imgBase642d: string, imgBase643d: string, cropX: number, cropY: number): Promise<Blob | null> {
        if (!this._pdf) return null;
        if (!this._font) return null;

        const page = this._pdf.getPage(0);
        page.setFont(this._font);

        const baseXElements = page.getWidth() - 210;
        const baseXQty = page.getWidth() - 30;
        let baseYElements = (page.getHeight() / 2) + 370;

        if (this._room.AdditionalWalls.length > 0) {
            page.moveTo(baseXElements, baseYElements);
            page.drawText("Wall/Partition", { size: 11 });
            page.moveTo(baseXQty, baseYElements);
            page.drawText("x" + this._room.AdditionalWalls.length, { size: 11 });
    
            baseYElements -= 18;
        }

        const insertedElements: string[] = [];
        for (const element of this._room.Elements) {
            if (insertedElements.includes(element.Name))
                continue;

            insertedElements.push(element.Name);
            const qty = this._room.Elements.filter(e => e.Name === element.Name && e.Category === element.Category).length;
            
            page.moveTo(baseXElements, baseYElements);
            page.drawText(element.Name, { size: 11 });

            page.moveTo(baseXQty, baseYElements);
            page.drawText("x" + qty, { size: 11 });

            baseYElements -= 18;
        }

        const baseXInfo = page.getWidth() - 200;
        const baseYInfo = (page.getHeight() / 2) - 120;

        page.moveTo(baseXInfo + 100, baseYInfo);
        page.drawText(this._room.PlanNumber, { size: 9 });

        if (this._room.Contact && !this._room.Quote) {
            page.moveTo(baseXInfo + 20, baseYInfo - 12);
            page.drawText(this._room.Contact.Name, { size: 9 });

            page.moveTo(baseXInfo + 30, baseYInfo - 23);
            page.drawText(this._room.Contact.Company, { size: 9 });

            page.moveTo(baseXInfo + 50, baseYInfo - 34);
            page.drawText(this._room.Contact.Email, { size: 9 });
        }

        if (this._room.Quote) {
            page.moveTo(baseXInfo + 20, baseYInfo - 12);
            page.drawText(this._room.Quote.Name, { size: 9 });

            page.moveTo(baseXInfo + 30, baseYInfo - 23);
            page.drawText(this._room.Quote.Company, { size: 9 });

            page.moveTo(baseXInfo + 50, baseYInfo - 34);
            page.drawText(this._room.Quote.Email, { size: 9 });

            page.moveTo(baseXInfo + 30, baseYInfo - 45);
            page.drawText(this._room.Quote.Phone, { size: 9 });

            page.moveTo(baseXInfo + 60, baseYInfo - 56);
            page.drawText(this._room.Quote.RentalStart, { size: 9 });

            page.moveTo(baseXInfo + 60, baseYInfo - 67);
            page.drawText(this._room.Quote.RentalEnd, { size: 9 });

            page.moveTo(baseXInfo + 20, baseYInfo - 83);
            page.drawText(this._room.Quote.Notes, { size: 9 });
        }

        return new Promise((resolve, reject) => {
            this._crop(imgBase642d, 1200, 700, cropX - 30, cropY - 50, async (data2d) => {
                await this.addRoomImage(data2d, -100, 240);
    
                this._crop(imgBase643d, 1200, 700, cropX, cropY, async (data3d) => {
                    if (!this._pdf) 
                    {
                        reject();
                        return;
                    }
        
                    await this.addRoomImage(data3d, -100, -140);
        
                    const pdfBytes = await this._pdf.save();
                    const blob = new Blob([pdfBytes]);
                    this._downloadBlob(blob, Constants.PLAN_PDF_NAME);

                    resolve(blob);
                });
            });
        });
    }
}