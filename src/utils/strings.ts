export const Strings = {
    ADD_WALLS: "ADD WALL/PARTITION",
    ADD_ELEMENTS: "ADD FURNITURE",

    AREA: "FLOOR AREA:",
    METERS: " m",
    METERS_SQUARED: " SQMT",

    ROOM_HEIGHT_NOUNIT: "Room Height: ",

    ROOM_HEIGHT: "Room Height (cm)",
    PREVIOUS: "PREVIOUS",
    NEXT: "NEXT",
    OK: "OK",
    CANCEL: "CANCEL",
    WALL_LENGTH: "Wall Length (cm)",
    ELEMENTS: "Elements: ",

    START: "START",
    ENTER_VALID_PLAN_NUMBER: "PLEASE ENTER A VALID PLAN NUMBER.",
    PICK_ROOM_SHAPE: "PICK ROOM SHAPE",
    PLAN_NUMBER_INPUT_PLACEHOLDER: "PLAN NUMBER:",
    GO: "GO",

    CONFIGURE_PLAN: "CONFIGURE PLAN",
    VISUAL_3D: "3D VISUAL",
    SAVE: "SAVE",
    QUOTE: "QUOTE",

    ROTATE: "ROTATE",
    COPY: "DUPLICATE",
    UNDO: "UNDO",
    DELETE: "DELETE",
    CONFIRM: "CONFIRM",
    INSERT_WALL_LENGTH: "PLEASE INSERT THE DESIRED WALL LENGTH",

    BACK: "< BACK",
    WALL_PARTITION: "WALL/PARTITION",
    HEIGHT: "HEIGHT: ",
    WIDTH: "WIDTH: ",
    DEPTH: "DEPTH: ",

    SHELVING_UNIT_CATEGORY: "Shelving units",
    RAIL_CATEGORY: "Rails & Rail Bases",
    DESK_CATEGORY: "Desks",
    FITTING_ROOM_CATEGORY: "Fitting Rooms",
    MIRROR_CATEGORY: "Mirrors",
    TABLE_CATEGORY: "Tables",
    PLINTH_CATEGORY: "Plinths",
    DISPLAY_CASE_CATEGORY: "Display Cases",

    NOT_SUPPORTED: "TO ENSURE THE BEST EXPERIENCE PLEASE USE A DESKTOP BROWSER",
    ADD_TO_PLAN: "ADD TO PLAN",

    NAME: "NAME",
    COMPANY: "COMPANY",
    EMAIL: "EMAIL ADDRESS",
    PHONE: "PHONE NUMBER",
    RENTAL_START: "RENTAL START DATE",
    RENTAL_END: "RENTAL END DATE",
    NOTES: "PROJECT NOTES",
    REQUEST_QUOTE: "REQUEST A QUOTE",
    LOCATION: "LOCATION",

    PLAN_SAVED: "YOUR PLAN HAS BEEN SAVED!\nPLAN NUMBER: {plan}",
    DOWNLOAD_PLAN: "DOWNLOAD PLAN",
    SUBMIT_QUOTE: "SUBMIT QUOTE",

    CONGRATULATIONS: "CONGRATULATIONS, YOU HAVE NOW DESIGNED YOUR SPACE!\nA MEMBER OF OUR TEAM WILL BE IN TOUCH SHORTLY WITH YOUR QUOTE.\nTHANK YOU.",
};

export const ElementNames = {
    HORIZONTAL_WALL: "HORIZONTAL WALL",
    VERTICAL_WALL: "VERTICAL WALL",

    SHELVING_UNIT1_BIRCH: "Shelving Unit 1 Birch",
    SHELVING_UNIT1_BLACK: "Shelving Unit 1 Black",
    SHELVING_UNIT1_WHITE: "Shelving Unit 1 White",

    SHELVING_UNIT_STORAGE1_BIRCH: "Shelving Unit with Storage 1 Birch",
    SHELVING_UNIT_STORAGE1_BLACK: "Shelving Unit with Storage 1 Black",
    SHELVING_UNIT_STORAGE1_WHITE: "Shelving Unit with Storage 1 White",

    SHELVING_UNIT_STORAGE2_BIRCH: "Shelving Unit with Storage 2 Birch",
    SHELVING_UNIT_STORAGE2_BLACK: "Shelving Unit with Storage 2 Black",
    SHELVING_UNIT_STORAGE2_WHITE: "Shelving Unit with Storage 2 White",

    SHELVING_UNIT_STORAGE3_BIRCH: "Shelving Unit with Storage 3 Birch",
    SHELVING_UNIT_STORAGE3_BLACK: "Shelving Unit with Storage 3 Black",
    SHELVING_UNIT_STORAGE3_WHITE: "Shelving Unit with Storage 3 White",

    SHELVING_UNIT2_BIRCH: "Shelving Unit 2 Birch",
    SHELVING_UNIT2_BLACK: "Shelving Unit 2 Black",
    SHELVING_UNIT2_WHITE: "Shelving Unit 2 White",

    SHELVING_UNIT3_BIRCH: "Shelving Unit 3 Birch",
    SHELVING_UNIT3_BLACK: "Shelving Unit 3 Black",
    SHELVING_UNIT3_WHITE: "Shelving Unit 3 White",

    SMALL_CASH_DESK_BIRCH: "Small Cash Desk Birch",
    SMALL_CASH_DESK_BLACK: "Small Cash Desk Black",
    SMALL_CASH_DESK_WHITE: "Small Cash Desk White",

    CASH_DESK_BIRCH: "Cash Desk Birch",
    CASH_DESK_BLACK: "Cash Desk Black",
    CASH_DESK_WHITE: "Cash Desk White",

    CREATIVE_TABLE_BIRCH: "Creative Table Birch",
    CREATIVE_TABLE_BLACK: "Creative Table Black",
    CREATIVE_TABLE_WHITE: "Creative Table White",

    TABLE_BIRCH: "Table Birch",
    TABLE_BLACK: "Table Black",
    TABLE_WHITE: "Table White",

    DISPLAY_CASE_BIRCH: "Display Case Birch",
    DISPLAY_CASE_BLACK: "Display Case Black",
    DISPLAY_CASE_WHITE: "Display Case White",

    SMALL_PLINTH_BIRCH: "Small Plinth Birch",
    SMALL_PLINTH_BLACK: "Small Plinth Black",
    SMALL_PLINTH_WHITE: "Small Plinth White",

    MEDIUM_PLINTH_BIRCH: "Medium Plinth Birch",
    MEDIUM_PLINTH_BLACK: "Medium Plinth Black",
    MEDIUM_PLINTH_WHITE: "Medium Plinth White",

    LARGE_PLINTH_BIRCH: "Large Plinth Birch",
    LARGE_PLINTH_BLACK: "Large Plinth Black",
    LARGE_PLINTH_WHITE: "Large Plinth White",

    MIRROR_BLACK: "Mirror Black",
    MIRROR_STEEL: "Mirror Steel",
    MIRROR_WHITE: "Mirror White",

    FITTING_ROOM_BLACK: "Fitting Room Black",
    FITTING_ROOM_BLUE: "Fitting Room Blue",
    FITTING_ROOM_GREY: "Fitting Room Grey",

    RAIL_BASE_BIRCH: "Rail Base Birch",
    RAIL_BASE_BLACK: "Rail Base Black",
    RAIL_BASE_WHITE: "Rail Base White",

    RAIL_BASE_SMALL_BIRCH: "Rail Base Small Birch",
    RAIL_BASE_SMALL_BLACK: "Rail Base Small Black",
    RAIL_BASE_SMALL_WHITE: "Rail Base Small White",

    TALL_RAIL_BLACK: "H1850xW1600 Hanging Rail Black",
    TALL_RAIL_STEEL: "H1850xW1600 Hanging Rail Steel",
    TALL_RAIL_WHITE: "H1850xW1600 Hanging Rail White",

    TALL_RAIL_SMALL_BLACK: "H1850xW1000 Hanging Rail Black",
    TALL_RAIL_SMALL_STEEL: "H1850xW1000 Hanging Rail Steel",
    TALL_RAIL_SMALL_WHITE: "H1850xW1000 Hanging Rail White",

    RAIL_SMALL_BLACK: "H1700xW1000 Hanging Rail Black",
    RAIL_SMALL_STEEL: "H1700xW1000 Hanging Rail Steel",
    RAIL_SMALL_WHITE: "H1700xW1000 Hanging Rail White",

    RAIL_STANDARD_BLACK: "H1700xW1600 Hanging Rail Black",
    RAIL_STANDARD_STEEL: "H1700xW1600 Hanging Rail Steel",
    RAIL_STANDARD_WHITE: "H1700xW1600 Hanging Rail White",
    
    
};