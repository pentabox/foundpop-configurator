import {Vector3} from "@babylonjs/core";
import { ExportVector } from "../models/placeable";

export class UnitCalculator {
    public static BabylonToUnitConversionRate: number = 2.5;

    public static babylonToUnit(babylonPoint: Vector3) : Vector3 {
        const p = babylonPoint.clone();
        p.x *= this.BabylonToUnitConversionRate;
        p.y *= this.BabylonToUnitConversionRate;
        p.z *= this.BabylonToUnitConversionRate;
        return p;
    }

    public static unitToBabylon(unitPoint: Vector3) : Vector3 {
        const p = unitPoint.clone();
        p.x /= this.BabylonToUnitConversionRate;
        p.y /= this.BabylonToUnitConversionRate;
        p.z /= this.BabylonToUnitConversionRate;
        return p;
    }

    public static unitToBabylonSingle(unit: number): number {
        return unit / this.BabylonToUnitConversionRate;
    }

    public static clamp(num: number, min: number, max: number): number {
        return Math.min(Math.max(num, min), max);
    }

    public static lerp(start: number, end: number, alpha: number) {
        return (1 - alpha) * start + (alpha * end);
    }

    public static vectorToJson(v: Vector3): ExportVector {
        const json: ExportVector = {
            X: v.x,
            Y: v.y,
            Z: v.z
        }
        return json;
    }
}