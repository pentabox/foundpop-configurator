import { Engine } from "@babylonjs/core";

import { SceneBase, States } from "../scenes/sceneBase";
import {MainMenuScene} from "../scenes/mainMenu/mainMenuScene";
import {FoundPopConfigurator} from "../index";
import {RoomMenuScene} from "../scenes/roomMenu/roomMenuScene";
import {Editing2dScene} from "../scenes/editing2d/editing2dScene";
import { NotSupportedScene } from "../scenes/notSupported/notSupportedScene";

export class FoundPopSceneLoader {
    private _sceneDictionary: { [key in States]?: typeof SceneBase } = {
        [States.MAIN_MENU]: MainMenuScene,
        [States.ROOM_MENU]: RoomMenuScene,
        [States.EDITING_2D]: Editing2dScene,
        [States.DEVICE_NOT_SUPPORTED]: NotSupportedScene
    };

    private _sceneCache: any = {};
    private _currentScene: SceneBase | null = null;

    constructor(private _engine: Engine, private _app : FoundPopConfigurator) {

    }

    public async loadSceneForState(state: States) : Promise<SceneBase | null> {
        this._engine.displayLoadingUI();

        if (this._currentScene !== null) {
            this._currentScene.Scene?.detachControl();

            // @ts-ignore
            this._app.Gui?.removeControl(this._currentScene.getSceneGui().getContainer());
        }

        const sceneName = this._sceneDictionary[state] as typeof SceneBase;
        let scene: SceneBase | null = null;
        if (this._sceneCache[sceneName.name]) {
            scene = this._sceneCache[sceneName.name];
        } else {
            scene = (new sceneName(this._engine, this._app)) as SceneBase;
            if (scene === null) return null;

            await scene.Scene?.whenReadyAsync();
            this._engine.hideLoadingUI();
        }

        if (this._currentScene) {
            this._currentScene.dispose();
        }

        this._currentScene = scene;
        return scene;
    }
}