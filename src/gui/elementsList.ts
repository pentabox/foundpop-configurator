import { Observable } from "@babylonjs/core";
import { Button, Container, Grid, Image, Rectangle, ScrollViewer, TextBlock } from "@babylonjs/gui";
import { FoundPopConfigurator } from "..";
import { RoomElements } from "../data/roomElements";
import { RoomElement } from "../elements/roomElement";
import { Placeable, PlaceableCategory } from "../models/placeable";
import { Strings } from "../utils/strings";

export class RoomElementChoice {
    public Choices: Map<RoomElement, number> = new Map<RoomElement, number>();

    public getQuantity(element: RoomElement): number {
        let qt = this.Choices.get(element);
        if (!qt)
            return 0;

        return qt;
    }

    public getFirstAvailableElement(): RoomElement {
        const [first] = this.Choices.keys();
        return first;
    }

    public add(element: RoomElement, quantity: number = 1): number {
        let previousQt = this.getQuantity(element);
        previousQt += quantity;

        this.Choices.set(element, previousQt);

        return previousQt;
    }

    public remove(element: RoomElement, quantity: number = 1): number {
        let previousQt = this.getQuantity(element);
        if (previousQt <= 0) return 0;

        previousQt -= quantity;
        if (previousQt <= 0) {
            this.Choices.delete(element);
            return 0;
        }

        this.Choices.set(element, previousQt);
        return previousQt;
    }

    public clear() {
        this.Choices.clear();
    }
}

export class ElementsList {
    public onElementsChosen: Observable<RoomElementChoice>;

    private _overlay: Rectangle;
    private _window: Container;
    private _backBtn: Button | null = null;

    private _choice: RoomElementChoice;

    private _categoriesGridScroll: ScrollViewer | null = null;
    private _elementsGridScroll: ScrollViewer | null = null;
    private _categoriesGrid: Grid | null = null;
    private _elementsGrid: Grid | null = null;
    private _addElementsButton: Button | null = null;

    private _scrollUpButton: Button | null = null;
    private _scrollDownButton: Button | null = null;

    private _currentCategory: PlaceableCategory = PlaceableCategory.DESK;

    constructor(private _app: FoundPopConfigurator) {
        this.onElementsChosen = new Observable<RoomElementChoice>();

        this._choice = new RoomElementChoice();

        this._overlay = new Rectangle("ElementsListOverlay");
        this._overlay.width = "100%";
        this._overlay.height = "100%";
        this._overlay.zIndex = 991;
        this._overlay.background = "black";
        this._overlay.alpha = 0.5;

        const window = new Container("ElementsList");
        window.background = "white";

        window.height = "95%";
        window.width = "90%";
        window.fontFamily = "BasisGrotesquePro";
        window.zIndex = 999;

        this._window = window;

        this._setupTopMenu();
        this._setupGrids();

        if (_app.Gui && _app.Gui.GuiTexture)
        {
            _app.Gui.GuiTexture.addControl(this._overlay);
            _app.Gui.GuiTexture.addControl(window);
        }

        window.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_CENTER;
        window.verticalAlignment = Container.VERTICAL_ALIGNMENT_CENTER;

        this.hide();
    }

    private _setupGrids() {
        const catGrid = new Grid("CategoriesGrid");
        catGrid.width = "100%";
        catGrid.height = "100%";

        // 4 x 2
        catGrid.addRowDefinition(0.5);
        catGrid.addRowDefinition(0.5);

        catGrid.addColumnDefinition(0.25);
        catGrid.addColumnDefinition(0.25);
        catGrid.addColumnDefinition(0.25);
        catGrid.addColumnDefinition(0.25);

        this._categoriesGrid = catGrid;

        this._elementsGridScroll = new ScrollViewer("ElementsGridScroll");
        this._elementsGridScroll.width = "90%";
        this._elementsGridScroll.height = "80%";

        this._window.addControl(this._elementsGridScroll);
        this._elementsGridScroll.isVisible = false;

        this._elementsGridScroll.verticalBar.onValueChangedObservable.add(() => {
            const v = this._elementsGridScroll?.verticalBar.value!;
            if (v <= 0 && this._scrollUpButton)
                this._scrollUpButton.isVisible = false;
            else if (this._scrollUpButton)
                this._scrollUpButton.isVisible = true;

            if (v >= 1 && this._scrollDownButton)
                this._scrollDownButton.isVisible = false;
            else if (this._scrollDownButton)
                this._scrollDownButton.isVisible = true;

            const elements = RoomElements.filter(element => element.Category === this._currentCategory);

            if (elements.length < 7) {
                this._scrollDownButton!.isVisible = false;
                this._scrollUpButton!.isVisible = false;
            }
        });

        const addElementsBtn = Button.CreateSimpleButton("AddElementsBtn", Strings.ADD_TO_PLAN);
        addElementsBtn.width = "15%";
        addElementsBtn.height = "50px";
        addElementsBtn.background = "#C7D0D8";
        addElementsBtn.thickness = 0;
        
        if (addElementsBtn.textBlock) {
            addElementsBtn.textBlock.fontSizeInPixels = 30;
            addElementsBtn.textBlock.fontFamily = "BasisGrotesquePro";
            addElementsBtn.textBlock.color = "black";
        }

        addElementsBtn.onPointerEnterObservable.add(() => {
            this._app.setCursor("pointer");

            addElementsBtn.background = "black";
            if (addElementsBtn.textBlock)
                addElementsBtn.textBlock.color = "white";
        });
        addElementsBtn.onPointerOutObservable.add(() => {
            this._app.resetCursor();

            addElementsBtn.background = "#C7D0D8";
            if (addElementsBtn.textBlock)
                addElementsBtn.textBlock.color = "black";
        });
        addElementsBtn.onPointerClickObservable.add(() => {
            this.hide();

            this.onElementsChosen.notifyObservers(this._choice);
        });
        
        this._window.addControl(addElementsBtn);
        addElementsBtn.verticalAlignment = Container.VERTICAL_ALIGNMENT_BOTTOM;
        addElementsBtn.top = "-20px";

        this._addElementsButton = addElementsBtn;
        addElementsBtn.isVisible = false;

        this._categoriesGridScroll = new ScrollViewer("CategoriesGridScroll");
        this._categoriesGridScroll.width = "90%";
        this._categoriesGridScroll.height = "80%";

        this._categoriesGridScroll.addControl(catGrid);

        this._window.addControl(this._categoriesGridScroll);

        this._fillCategoriesGrid();
        this._createScrollButtons();
    }

    private _createScrollButtons() {
        const downBtn = Button.CreateImageOnlyButton("ScrollUpBtn", "assets/images/icons/scroll_down.svg");
        downBtn.thickness = 0;
        downBtn.width = "40px";
        downBtn.height = "40px";
        downBtn.zIndex = 99999;
        downBtn.top = "-10.5%";
        downBtn.left = "-2%";
        downBtn.onPointerClickObservable.add(() => {
            if (!this._elementsGridScroll)
                return;

            this._elementsGridScroll.verticalBar.value += 0.1;
        });

        this._window.addControl(downBtn);
        downBtn.horizontalAlignment = Button.HORIZONTAL_ALIGNMENT_RIGHT;
        downBtn.verticalAlignment = Button.VERTICAL_ALIGNMENT_BOTTOM;
        downBtn.isVisible = false;

        this._scrollDownButton = downBtn;

        const upBtn = Button.CreateImageOnlyButton("ScrollUpBtn", "assets/images/icons/scroll_down.svg");
        upBtn.thickness = 0;
        upBtn.width = "40px";
        upBtn.height = "40px";
        upBtn.zIndex = 99999;
        upBtn.rotation = Math.PI;
        upBtn.left = "-2%";
        upBtn.top = "10.5%";
        upBtn.onPointerClickObservable.add(() => {
            if (!this._elementsGridScroll)
                return;

            this._elementsGridScroll.verticalBar.value -= 0.1;
        });

        this._window.addControl(upBtn);
        upBtn.horizontalAlignment = Button.HORIZONTAL_ALIGNMENT_RIGHT;
        upBtn.verticalAlignment = Button.VERTICAL_ALIGNMENT_TOP;
        upBtn.isVisible = false;

        this._scrollUpButton = upBtn;
    }

    private _fillCategoriesGrid() {
        const values = Object.values(PlaceableCategory).filter((v) => !isNaN(Number(v)));

        let i = 0;
        for (const cat of values) {
            if ((cat as PlaceableCategory) === PlaceableCategory.WALL) continue;

            const entry = this._getCategoryEntry(cat as PlaceableCategory);
            this._categoriesGrid?.addControl(entry, i / 4, i % 4);

            i++;
        }
    }

    private _getCategoryEntry(cat: PlaceableCategory): Container {
        const name = Placeable.getCategoryName(cat);
        const imgPath = Placeable.getCategoryImage(cat);

        const entry = new Container("CategoryEntry");
        entry.width = "100%";
        entry.height = "100%";

        const image = new Image("CategoryImage", imgPath);
        image.width = "80%";
        image.height = "80%";
        image.stretch = Image.STRETCH_UNIFORM;

        entry.addControl(image);
        image.verticalAlignment = Container.VERTICAL_ALIGNMENT_TOP;

        const label = new TextBlock("CategoryLabel", name);
        label.width = "100%";
        label.height = "100%";
        label.fontSizeInPixels = 30;
        label.fontFamily = "BasisGrotesquePro";
        label.color = "black";

        const rectangle = new Rectangle("CategoryLabelRectangle");
        rectangle.width = "80%";
        rectangle.height = "20%";
        rectangle.thickness = 0;
        rectangle.background = "transparent";

        rectangle.addControl(label);

        entry.addControl(rectangle);
        rectangle.verticalAlignment = Container.VERTICAL_ALIGNMENT_BOTTOM;

        const btn = Button.CreateSimpleButton("CategoryBtn", "");
        btn.thickness = 0;
        btn.background = "transparent";
        btn.color = "transparent";
        btn.width = "100%";
        btn.height = "100%";
        btn.zIndex = 9991;

        entry.addControl(btn);

        btn.onPointerEnterObservable.add(() => {
            this._app.setCursor("pointer");

            rectangle.background = "black";
            label.color = "white";
        });
        btn.onPointerOutObservable.add(() => {
            this._app.resetCursor();

            rectangle.background = "transparent";
            label.color = "black";
        });
        btn.onPointerDownObservable.add(() => {
            label.scaleX = 0.9;
            label.scaleY = 0.9;
        });
        btn.onPointerUpObservable.add(() => {
            label.scaleX = 1;
            label.scaleY = 1;
        });
        btn.onPointerClickObservable.add(() => {
            if (this._categoriesGridScroll)
                this._categoriesGridScroll.isVisible = false;
            
            if (this._backBtn)
                this._backBtn.isVisible = true;

            const v = this._elementsGridScroll?.verticalBar.value!;
            if (v <= 0 && this._scrollUpButton)
                this._scrollUpButton.isVisible = false;
            else if (this._scrollUpButton)
                this._scrollUpButton.isVisible = true;

            if (v >= 1 && this._scrollDownButton)
                this._scrollDownButton.isVisible = false;
            else if (this._scrollDownButton)
                this._scrollDownButton.isVisible = true;

            this._fillElementsGrid(cat);
        });

        return entry;
    }

    private _fillElementsGrid(category: PlaceableCategory) {
        if (!this._elementsGridScroll) return;

        this._currentCategory = category;

        if (this._elementsGrid)
            this._elementsGrid.dispose();

        this._elementsGrid = null;

        const elGrid = new Grid("ElementsGrid");
        elGrid.width = "100%";
        elGrid.height = "100%";

        // 3 x 2
        elGrid.addColumnDefinition(1 / 3);
        elGrid.addColumnDefinition(1 / 3);
        elGrid.addColumnDefinition(1 / 3);

        this._elementsGrid = elGrid;
        this._elementsGridScroll.addControl(elGrid);

        const elements = RoomElements.filter(element => element.Category === category);

        const numRows = elements.length / 3;
        for (let j = 0; j < numRows; ++j) {
            this._elementsGrid.addRowDefinition(1 / numRows);
        }

        this._elementsGrid.height = ((numRows / 2) * 100) + "%";

        let i = 0;
        for (const element of elements) {
            const entry = this._getElementEntry(element);
            this._elementsGrid?.addControl(entry, i / 3, i % 3);

            i++;
        }

        if (elements.length < 7) {
            this._scrollDownButton!.isVisible = false;
            this._scrollUpButton!.isVisible = false;
        }

        this._elementsGridScroll.isVisible = true;

        if (this._addElementsButton)
            this._addElementsButton.isVisible = true;
    }

    private _getElementEntry(element: RoomElement): Container {
        const entry = new Container("ElementEntry");
        entry.width = "100%";
        entry.height = "100%";

        const image = new Image("ElementImage", element.MenuImagePath);
        image.width = "80%";
        image.height = "80%";
        image.stretch = Image.STRETCH_UNIFORM;

        entry.addControl(image);
        image.verticalAlignment = Container.VERTICAL_ALIGNMENT_TOP;

        const entryQtContainer = new Container("ElementQuantityContainer");
        entryQtContainer.width = "30%";
        entryQtContainer.height = "20%";
        entryQtContainer.background = "transparent";

        const qtLabel = new TextBlock("ElementQuantityLabel", "0");
        qtLabel.width = "25%";
        qtLabel.height = "100%";
        qtLabel.fontSizeInPixels = 30;
        qtLabel.fontFamily = "BasisGrotesquePro";
        qtLabel.color = "black";

        entryQtContainer.addControl(qtLabel);

        const plusBtn = Button.CreateSimpleButton("PlusBtn", "+");
        plusBtn.width = "20%";
        plusBtn.height = "100%";
        plusBtn.thickness = 0;
        plusBtn.background = "transparent";

        if (plusBtn.textBlock) {
            plusBtn.textBlock.fontSizeInPixels = 40;
            plusBtn.textBlock.fontFamily = "BasisGrotesquePro";
            plusBtn.textBlock.color = "black";
        }

        plusBtn.onPointerEnterObservable.add(() => {
            this._app.setCursor("pointer");

            plusBtn.background = "black";
            if (plusBtn.textBlock)
                plusBtn.textBlock.color = "white";
        });
        plusBtn.onPointerOutObservable.add(() => {
            this._app.resetCursor();

            plusBtn.background = "transparent";
            if (plusBtn.textBlock)
                plusBtn.textBlock.color = "black";
        });
        plusBtn.onPointerClickObservable.add(() => {
            let prev = this._choice.Choices.get(element);
            if (!prev)
                prev = 0;

            if (prev + 1 > element.MaxQuantityInRoom)
                return;

            const qty = this._choice.add(element);
            qtLabel.text = qty.toString();
        });

        entryQtContainer.addControl(plusBtn);
        plusBtn.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_RIGHT;

        const minusBtn = Button.CreateSimpleButton("MinusBtn", "-");
        minusBtn.width = "20%";
        minusBtn.height = "100%";
        minusBtn.thickness = 0;
        minusBtn.background = "transparent";

        if (minusBtn.textBlock) {
            minusBtn.textBlock.fontSizeInPixels = 40;
            minusBtn.textBlock.fontFamily = "BasisGrotesquePro";
            minusBtn.textBlock.color = "black";
        }

        minusBtn.onPointerEnterObservable.add(() => {
            this._app.setCursor("pointer");

            minusBtn.background = "black";
            if (minusBtn.textBlock)
                minusBtn.textBlock.color = "white";
        });
        minusBtn.onPointerOutObservable.add(() => {
            this._app.resetCursor();

            minusBtn.background = "transparent";
            if (minusBtn.textBlock)
                minusBtn.textBlock.color = "black";
        });
        minusBtn.onPointerClickObservable.add(() => {
            const qty = this._choice.remove(element);
            qtLabel.text = qty.toString();
        });

        entryQtContainer.addControl(minusBtn);
        minusBtn.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;

        entry.addControl(entryQtContainer);
        entryQtContainer.verticalAlignment = Container.VERTICAL_ALIGNMENT_BOTTOM;

        return entry;
    }

    private _setupTopMenu() {
        const topMenu = new Container("ElementsTopMenu");
        topMenu.width = "100%";
        topMenu.height = "60px";
        topMenu.background = "#C7D0D8";
        
        this._window.addControl(topMenu);
        topMenu.verticalAlignment = Container.VERTICAL_ALIGNMENT_TOP;

        const closeBtn = Button.CreateImageOnlyButton("CloseButton", "assets/images/icons/exit.svg");
        closeBtn.height = "35px";
        closeBtn.width = "35px";
        closeBtn.thickness = 0;
        closeBtn.left = "-20px";
        if (closeBtn.image)
        {
            closeBtn.image.stretch = Image.STRETCH_UNIFORM;
            closeBtn.image.horizontalAlignment = Button.HORIZONTAL_ALIGNMENT_RIGHT;
        }

        closeBtn.onPointerClickObservable.add(() => {
            this.hide();
        });
        closeBtn.onPointerEnterObservable.add(() => {
            this._app.setCursor("pointer");
        });
        closeBtn.onPointerOutObservable.add(() => {
            this._app.resetCursor();
        });

        topMenu.addControl(closeBtn);
        closeBtn.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_RIGHT;

        const backBtn = Button.CreateSimpleButton("BackButton", Strings.BACK);
        backBtn.thickness = 0;
        backBtn.color = "transparent";
        backBtn.background = "transparent";
        backBtn.width = "10%";
        if (backBtn.textBlock) {
            backBtn.textBlock.fontSizeInPixels = 30;
            backBtn.textBlock.fontFamily = "BasisGrotesquePro";
            backBtn.textBlock.color = "black";
            backBtn.textBlock.textHorizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;
            backBtn.textBlock.left = "15px";
        }

        backBtn.onPointerClickObservable.add(() => {
            if (this._elementsGridScroll)
                this._elementsGridScroll.isVisible = false;
            if (this._addElementsButton)
                this._addElementsButton.isVisible = false;

            if (this._categoriesGridScroll)
                this._categoriesGridScroll.isVisible = true;

            if (this._scrollUpButton)
                this._scrollUpButton.isVisible = false;
            if (this._scrollDownButton)
                this._scrollDownButton.isVisible = false;

            backBtn.isVisible = false;
        });
        backBtn.onPointerEnterObservable.add(() => {
            this._app.setCursor("pointer");
        });
        backBtn.onPointerOutObservable.add(() => {
            this._app.resetCursor();
        });

        topMenu.addControl(backBtn);
        backBtn.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;

        this._backBtn = backBtn;
        backBtn.isVisible = false;
    }

    public hide() {
        this._window.isVisible = false;
        this._overlay.isVisible = false;

        if (this._elementsGridScroll)
            this._elementsGridScroll.isVisible = false;
        if (this._addElementsButton)
            this._addElementsButton.isVisible = false;

        if (this._categoriesGridScroll)
            this._categoriesGridScroll.isVisible = true; 
    }

    public show(clearPreviousChoice: boolean = true) {
        if (clearPreviousChoice)
            this._choice.clear();

        if (this._backBtn)
            this._backBtn.isVisible = false;
        if (this._scrollDownButton)
            this._scrollDownButton.isVisible = false;
        if (this._scrollUpButton)
            this._scrollUpButton.isVisible = false;

        this._window.isVisible = true;
        this._overlay.isVisible = true;

        if (this._elementsGridScroll)
            this._elementsGridScroll.isVisible = false;
        if (this._addElementsButton)
            this._addElementsButton.isVisible = false;

        if (this._categoriesGridScroll)
            this._categoriesGridScroll.isVisible = true; 
    }
}