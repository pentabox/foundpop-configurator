import { AdvancedDynamicTexture, Button, Container, InputText, Rectangle, StackPanel, TextBlock } from "@babylonjs/gui";
import { FoundPopConfigurator } from "..";
import { ModalWindow } from "./modalWindow";
import * as EmailValidator from 'email-validator';

export enum ButtonType {
    LEFT,
    RIGHT,
    CENTER
}
export class FormButtonConfig {
    constructor(public Type: ButtonType = ButtonType.CENTER,
                public Label: string = "",
                public Callback: (value: Map<string, string>) => void = (values: Map<string, string>) => {},
                public ShouldBeEnabledOnValidation: boolean = false)
    {

    }
}

export enum FieldValidationType {
    NONE,
    EMAIL,
    NOTEMPTY
}
export class FormFieldConfig {
    constructor(public Label: string = "",
                public Name: string = "",
                public StartingValue: string = "",
                public LabelWidth: string = "",
                public InputWidth: string = "",
                public FontColor: string = "white",
                public FontFamily: string = "BasisGrotesquePro",
                public FontSize: number = 30,
                public Validation: FieldValidationType = FieldValidationType.NONE)
    {

    }
}

export interface FormWindowOptions {
    height: string,
    width: string,
    spacing: string,

    fields: FormFieldConfig[];
    buttons: FormButtonConfig[];
}

export class FormWindow extends ModalWindow {
    private _fields: Map<string, InputText> = new Map<string, InputText>();
    private _buttons: Map<string, Button> = new Map<string, Button>();
    private _regexp: RegExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

    constructor(_app: FoundPopConfigurator, _guiTexture: AdvancedDynamicTexture, private _options: FormWindowOptions) {
        const content = FormWindow._getFormContent(_options);
        super(content, _app, _guiTexture, _options.width, _options.height);

        content.verticalAlignment = Container.VERTICAL_ALIGNMENT_CENTER;
        //content.top = "10px";

        this._createFields(content);

        const btnCallback = (b: FormButtonConfig) => {
            const values = this.getValues();
            b.Callback(values);
        };

        for (const b of _options.buttons) {
            switch (b.Type) {
                case ButtonType.CENTER:
                    const btn = this.addCenterButton(b.Label, () => {
                        btnCallback(b);
                    });
                    btn.width = _options.width;

                    const height = parseInt(_options.height);
                    this.setButtonContainerTop(((height / 2) + 3.7) + "%");

                    this._buttons.set(b.Label, btn);
                    if (b.ShouldBeEnabledOnValidation)
                        btn.isEnabled = false;

                    break;
                case ButtonType.LEFT:
                    const btn1 = this.addLeftButton(b.Label, () => {
                        btnCallback(b);
                    });
                    this._buttons.set(b.Label, btn1);
                    if (b.ShouldBeEnabledOnValidation)
                        btn1.isEnabled = false;

                    break;
                case ButtonType.RIGHT:
                    const btn2 = this.addRightButton(b.Label, () => {
                        btnCallback(b);
                    });
                    this._buttons.set(b.Label, btn2);
                    if (b.ShouldBeEnabledOnValidation)
                        btn2.isEnabled = false;

                    break;
            }
        }   
    }

    public getValues(): Map<string, string> {
        const m = new Map<string, string>();

        for (const field of this._fields) {
            m.set(field[0], field[1].text);
        }

        return m;
    }

    public setValue(key: string, value: string) {
        if (!this._fields.has(key)) return;

        this._fields.get(key)!.text = value;
    }

    public getValue(key: string) {
        if (!this._fields.has(key)) return undefined;
        
        return this._fields.get(key)!.text;
    }

    public show() {
        this._resetFields();
        super.show();
    }

    public setContentTop(top: string) {
        this._content.top = top;
    }

    private _createFields(parent: Container) {
        for (const field of this._options.fields) {
            const ff = this._getFormField(field);
            parent.addControl(ff);

            const spacing = new Container();
            spacing.width = "100%";
            spacing.height = this._options.spacing;
            spacing.background = "transparent";

            parent.addControl(spacing);
        }
    }

    private _resetFields() {
        /*for (const config of this._options.fields) {
            const field = this._fields.get(config.Name);
            if (!field) continue;

            field.text = config.StartingValue;
        }*/
    }

    private static _getFormContent(options: FormWindowOptions): Container {
        const parent = new StackPanel("FormStackPanel");
        parent.isVertical = true;
        parent.height = "90%";
        parent.width = "85%";

        return parent;
    }

    private _getFormField(config: FormFieldConfig): Container {
        const wrap = new Container(config.Name + "_Container");
        wrap.width = "100%";
        wrap.height = "70px";
        wrap.background = "transparent";
        
        const rectangle = new Rectangle(config.Name + "_Rectangle");
        rectangle.width = "100%";
        rectangle.height = "100%";
        rectangle.isPointerBlocker = false;
        rectangle.background = "transparent";
        rectangle.color = "white";
        rectangle.thickness = 5;

        wrap.addControl(rectangle);

        const label = new TextBlock(config.Name + "_Label", config.Label);
        label.color = config.FontColor;
        label.fontFamily = config.FontFamily;
        label.fontSizeInPixels = config.FontSize;
        label.width = config.LabelWidth;

        wrap.addControl(label);
        label.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;

        const input = new InputText(config.Name + "_Input", config.StartingValue);
        input.width = config.InputWidth;
        input.fontSizeInPixels = config.FontSize;
        input.fontFamily = config.FontFamily;
        input.color = config.FontColor;
        input.background = "transparent";
        input.focusedBackground = "transparent";
        input.textHighlightColor = "white";
        input.thickness = 0;

        wrap.addControl(input);
        input.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_RIGHT;

        input.onTextChangedObservable.add(() => {
            let validated = true;
            for (const f of this._fields) {
                const conf = this._options.fields.find(opt => opt.Name === f[0]);
                if (!conf) continue;

                validated = validated && this._checkValidation(conf, f[1]);
            }

            for (const b of this._buttons) {
                const conf = this._options.buttons.find(opt => opt.Label === b[0]);
                if (!conf) continue;
                if (!conf.ShouldBeEnabledOnValidation) continue;

                b[1].isEnabled = validated;
            }
        });

        this._fields.set(config.Name, input);
        return wrap;
    }

    private _checkValidation(config: FormFieldConfig, field: InputText): boolean {
        if (config.Validation === FieldValidationType.EMAIL) {
            return EmailValidator.validate(field.text);
        }
        if (config.Validation === FieldValidationType.NOTEMPTY) {
            if (field.text)
                return true;
            else
                return false;
        }

        return true;
    }
}