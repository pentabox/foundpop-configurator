import { EventState } from "@babylonjs/core";
import { AdvancedDynamicTexture, Button, Container, Image, Rectangle } from "@babylonjs/gui";
import { FoundPopConfigurator } from "..";
import { Editing2dGui } from "../scenes/editing2d/editing2dGui";

export class ModalWindow {
    protected _window: Container;
    private _overlay: Rectangle;
    private _buttonContainer: Container;

    constructor(protected _content: Container, private _app: FoundPopConfigurator, private _guiTexture: AdvancedDynamicTexture, width: string = "30%", height: string = "20%") {
        this._overlay = new Rectangle();
        this._overlay.width = "100%";
        this._overlay.height = "100%";
        this._overlay.zIndex = 991;
        this._overlay.background = "black";
        this._overlay.alpha = 0.5;

        _guiTexture.addControl(this._overlay);

        const window = new Container();
        window.background = "#C7D0D8";

        window.height = height;
        window.width = width;
        window.fontFamily = "BasisGrotesquePro";
        window.fontSize = "30px";
        window.zIndex = 999;

        _guiTexture.addControl(window); 

        window.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_CENTER;
        window.verticalAlignment = Container.VERTICAL_ALIGNMENT_CENTER;

        window.addControl(_content);
        _content.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_CENTER;
        _content.verticalAlignment = Container.VERTICAL_ALIGNMENT_TOP;

        const closeBtn = Button.CreateImageOnlyButton("CloseButton", "assets/images/icons/exit.svg");
        closeBtn.height = "35px";
        closeBtn.width = "35px";
        closeBtn.thickness = 0;
        if (closeBtn.image)
        {
            closeBtn.image.stretch = Image.STRETCH_UNIFORM;
            closeBtn.image.horizontalAlignment = Button.HORIZONTAL_ALIGNMENT_RIGHT;
        }

        closeBtn.onPointerClickObservable.add(() => {
            this.hide();
        });
        closeBtn.onPointerEnterObservable.add(() => {
            _app.setCursor("pointer");
        });
        closeBtn.onPointerOutObservable.add(() => {
            _app.resetCursor();
        });

        window.addControl(closeBtn);

        closeBtn.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_RIGHT;
        closeBtn.verticalAlignment = Container.VERTICAL_ALIGNMENT_TOP;

        this._window = window;

        this._buttonContainer = new Container("ButtonContainer");
        this._buttonContainer.width = window.width;
        this._buttonContainer.height = "60px";

        if (_app.Gui && _app.Gui.GuiTexture)
            _app.Gui.GuiTexture.addControl(this._buttonContainer);

        this._buttonContainer.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_CENTER;
        this._buttonContainer.verticalAlignment = Container.VERTICAL_ALIGNMENT_CENTER;
        this._buttonContainer.top = "13.7%";
        this._buttonContainer.zIndex = 999;
    }

    public setButtonContainerTop(top: string) {
        this._buttonContainer.top = top;
    }

    public addCenterButton(text: string, callback: () => void): Button {
        const btn = Button.CreateSimpleButton("ModalButton", text);
        btn.width = "300px";
        btn.height = "100%";
        btn.thickness = 0;
        btn.background = "#C7D0D8";

        if (btn.textBlock)
        {
            btn.textBlock.fontFamily = "BasisGrotesquePro";
            btn.textBlock.fontSize = "30px";
            btn.textBlock.color = "white";
        }

        btn.onPointerClickObservable.add(() => {
            callback();
        });
        btn.onPointerEnterObservable.add(() => {
            btn.background = "black";
            this._app.setCursor("pointer");
        });
        btn.onPointerOutObservable.add(() => {
            btn.background = "#C7D0D8";
            this._app.resetCursor();
        });
        
        this._buttonContainer.addControl(btn);
        btn.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_CENTER;
        btn.verticalAlignment = Container.VERTICAL_ALIGNMENT_CENTER;

        return btn;
    }

    public addLeftButton(text: string, callback: () => void): Button {
        const btn = this.addCenterButton(text, callback);
        btn.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;
        btn.width = "280px";

        return btn;
    }

    public addRightButton(text: string, callback: () => void): Button {
        const btn = this.addCenterButton(text, callback);
        btn.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_RIGHT;
        btn.width = "280px";

        return btn;
    }

    public hide() {
        this._window.isVisible = false;
        this._overlay.isVisible = false;
        this._buttonContainer.isVisible = false;
    }

    public show() {
        this._window.isVisible = true;
        this._overlay.isVisible = true;
        this._buttonContainer.isVisible = true;
    }
}