import {AdvancedDynamicTexture, Control, Image} from "@babylonjs/gui";
import {SideMenu} from "./sideMenu";
import {SceneBase} from "../scenes/sceneBase";
import {FoundPopConfigurator} from "../index";
import { imageProcessingDeclaration } from "@babylonjs/core/Shaders/ShadersInclude/imageProcessingDeclaration";
import { TopMenu } from "./topMenu";

export class FoundPopGUI {
    public readonly GuiTexture: AdvancedDynamicTexture;

    public SideMenu: SideMenu | undefined;
    public TopMenu: TopMenu | undefined;

    constructor(private _app: FoundPopConfigurator, hasSideMenu: boolean = true, hasTopMenu: boolean = false) {
        this.GuiTexture = AdvancedDynamicTexture.CreateFullscreenUI("GUI", true);
        this.GuiTexture.idealHeight = 1080;
        this.GuiTexture.idealWidth = 1920;
        // @ts-ignore
        this.GuiTexture.layer.layerMask = 0x10000000;

        if (hasSideMenu)
            this.SideMenu = new SideMenu(this.GuiTexture, _app);

        if (hasTopMenu)
            this.TopMenu = new TopMenu(this.GuiTexture, _app);

        const img = new Image("Logo", "assets/images/logo.png");
        img.width = 0.12;
        img.height = "50px";
        img.stretch = Image.STRETCH_UNIFORM;

        this.GuiTexture.addControl(img);
        img.horizontalAlignment = Image.HORIZONTAL_ALIGNMENT_LEFT;
        img.verticalAlignment = Image.VERTICAL_ALIGNMENT_TOP;
        img.leftInPixels = 25;
        img.topInPixels = 15;
        img.zIndex = 99;
    }

    public addToScene(scene: SceneBase) {
        scene.Scene?.addTexture(this.GuiTexture);
    }

    public addControl(control: Control | null) {
        if (control === null) return;

        this.GuiTexture.addControl(control);
    }

    public removeControl(control: Control | null) {
        if (control === null) return;

        this.GuiTexture.removeControl(control);
    }
}