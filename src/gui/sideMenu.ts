import {AdvancedDynamicTexture, Button, Container, Control, Image, InputText, Rectangle, StackPanel, TextBlock} from "@babylonjs/gui";
import {States} from "../scenes/sceneBase";
import {Strings} from "../utils/strings";
import {FoundPopConfigurator} from "../index";
import {Editing2dGui} from "../scenes/editing2d/editing2dGui";
import {Editing2dScene} from "../scenes/editing2d/editing2dScene";
import { Observable, Vector3 } from "@babylonjs/core";
import { PlaceableWall } from "../elements/placeableWall";
import { UnitCalculator } from "../utils/unitCalculator";
import { Constants } from "../utils/constants";
import { WallOrientation } from "../models/roomShape";

export class SideMenu {
    private _mainControls: Control[] = [];
    private _partitionControls: Control[] = [];

    private _heightInput: InputText | null = null;
    private _widthInput: InputText | null = null;
    private _lengthInput: InputText | null = null;

    public onHorizontalWallChosen: Observable<void> = new Observable<void>();
    public onVerticalWallChosen: Observable<void> = new Observable<void>();

    constructor(private _guiTexture: AdvancedDynamicTexture, private _app: FoundPopConfigurator) {
        const container = new StackPanel("SideMenuContainer");
        container.paddingTopInPixels = 0;
        container.background = "white";
        container.height = "100%";
        container.width = "15%";
        container.verticalAlignment = StackPanel.VERTICAL_ALIGNMENT_TOP;
        container.top = "9%";
        container.horizontalAlignment = StackPanel.HORIZONTAL_ALIGNMENT_LEFT;
        container.isVertical = true;
        container.zIndex = 1;

        const underline = new Rectangle("Underline");
        underline.height = "100%";
        underline.width = "2px";
        underline.background = "black";
        underline.thickness = 0;

        container.addControl(underline);
        underline.top = "0px";
        underline.left = "0px";
        underline.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_RIGHT;
        underline.verticalAlignment = Container.VERTICAL_ALIGNMENT_TOP;   

        this._addMainButtons(container);
        this._addPartitionControls(container);

        this._guiTexture.addControl(container);
    }

    private _addPartitionControls(container: StackPanel) {
        const back = Button.CreateSimpleButton("BackBtn", Strings.BACK);
        back.height = "40px";
        back.thickness = 0;
        back.background = "#C7D0D8";

        if (back.textBlock) {
            back.textBlock.fontFamily = "BasisGrotesquePro";
            back.textBlock.fontSizeInPixels = 25;
            back.textBlock.color = "black";

            back.textBlock.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;
            back.textBlock.left = "10px";
            back.textBlock.textHorizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;
        }

        back.pointerDownAnimation = () => {};
        back.pointerUpAnimation = () => {};

        back.onPointerEnterObservable.add(() => {
            this._app.setCursor("pointer");
        });
        back.onPointerOutObservable.add(() => {
            this._app.resetCursor();
        });
        back.onPointerClickObservable.add(() => {
            for (const c of this._partitionControls) {
                c.isVisible = false;
            }
            for (const c of this._mainControls) {
                c.isVisible = true;
            }
        });

        container.addControl(back);

        const wallsContainer = new Container("Partitions");
        wallsContainer.width = "100%";
        wallsContainer.height = "180px";

        const wallsLabel = new TextBlock("WallsLabel", Strings.WALL_PARTITION);
        wallsLabel.fontFamily = "BasisGrotesquePro";
        wallsLabel.fontSizeInPixels = 30;
        wallsLabel.color = "black";
        wallsLabel.height = "65px";

        wallsContainer.addControl(wallsLabel);
        wallsLabel.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_CENTER;
        wallsLabel.verticalAlignment = Container.VERTICAL_ALIGNMENT_TOP;

        const wallsPanel = new Container("WallsPartitions");
        wallsPanel.width = "100%";
        wallsPanel.height = "105px";

        const horizontalBtn = Button.CreateImageOnlyButton("HorizontalWallBtn", "assets/images/shapes/rectangle_black.png");
        horizontalBtn.height = "100%";
        horizontalBtn.width = "50%";
        horizontalBtn.thickness = 0;

        if (horizontalBtn.image)
            horizontalBtn.image.stretch = Image.STRETCH_UNIFORM;

        wallsPanel.addControl(horizontalBtn);
        horizontalBtn.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;

        horizontalBtn.onPointerEnterObservable.add(() => {
            this._app.setCursor("pointer");
        });
        horizontalBtn.onPointerOutObservable.add(() => {
            this._app.resetCursor();
        });

        const verticalBtn = Button.CreateImageOnlyButton("VerticalWallBtn", "assets/images/shapes/rectangle2_black.png");
        verticalBtn.height = "100%";
        verticalBtn.width = "50%";
        verticalBtn.thickness = 0;

        if (verticalBtn.image)
            verticalBtn.image.stretch = Image.STRETCH_UNIFORM;

        wallsPanel.addControl(verticalBtn);
        verticalBtn.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;
        verticalBtn.left = "50%";

        verticalBtn.onPointerEnterObservable.add(() => {
            this._app.setCursor("pointer");
        });
        verticalBtn.onPointerOutObservable.add(() => {
            this._app.resetCursor();
        });

        wallsContainer.addControl(wallsPanel);
        wallsPanel.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_CENTER;
        wallsPanel.verticalAlignment = Container.VERTICAL_ALIGNMENT_BOTTOM;

        container.addControl(wallsContainer);

        const sep1 = this._getSeparator();
        container.addControl(sep1);

        const height = this._getWallInput("Height", Strings.HEIGHT, "N/A");
        container.addControl(height);

        const hInput = height.getChildByName("WallHeightInput") as InputText;
        this._heightInput = hInput;

        const width = this._getWallInput("Width", Strings.WIDTH, "N/A");
        container.addControl(width);

        const wInput = width.getChildByName("WallWidthInput") as InputText;
        this._widthInput = wInput;

        const depth = this._getWallInput("Depth", Strings.DEPTH, "N/A");
        container.addControl(depth);

        const dInput = depth.getChildByName("WallDepthInput") as InputText;
        this._lengthInput = dInput;

        const sep2 = this._getSeparator();
        container.addControl(sep2);

        this._partitionControls.push(back);
        this._partitionControls.push(wallsContainer);
        this._partitionControls.push(sep1);

        this._partitionControls.push(height);
        this._partitionControls.push(width);
        this._partitionControls.push(depth);
        this._partitionControls.push(sep2);

        for (const c of this._partitionControls) {
            c.isVisible = false;
        }

        horizontalBtn.onPointerClickObservable.add(() => {
            this.onHorizontalWallChosen.notifyObservers();
        });
        verticalBtn.onPointerClickObservable.add(() => {
            this.onVerticalWallChosen.notifyObservers();
        });
    }

    private _getWallInput(name: string, label: string, val: string): Container {
        const heightStack = new Container("Wall" + name);
        heightStack.width = "100%";
        heightStack.height = "60px";

        const heightLabel = new TextBlock("Wall" + name + "Label", label);
        heightLabel.width = "45%";
        heightLabel.color = "black";
        heightLabel.fontSizeInPixels = 30;
        heightLabel.fontFamily = "BasisGrotesquePro";
        heightLabel.textHorizontalAlignment = Container.HORIZONTAL_ALIGNMENT_RIGHT;

        heightStack.addControl(heightLabel);
        heightLabel.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;

        const heightInput = new InputText("Wall" + name + "Input", val);
        heightInput.width = "35%";
        heightInput.color = "black";
        heightInput.background = "transparent";
        heightInput.focusedBackground = "transparent";
        heightInput.textHighlightColor = "black";
        heightInput.fontSizeInPixels = 25;
        heightInput.fontFamily = "BasisGrotesquePro";
        heightInput.thickness = 0;
        heightInput.left = "-20%";
        heightInput.isEnabled = false;
        heightInput.disabledColor = "transparent";

        heightStack.addControl(heightInput);
        heightInput.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_RIGHT;

        const unitLabel = new TextBlock("UnitLabel", "mm");
        unitLabel.width = "20%";
        unitLabel.color = "black";
        unitLabel.fontSizeInPixels = 20;
        unitLabel.fontFamily = "BasisGrotesquePro";
        unitLabel.textHorizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;

        heightStack.addControl(unitLabel);
        unitLabel.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_RIGHT;

        return heightStack;
    }

    private _getSeparator(): Rectangle {
        const grayLine = new Rectangle("GrayLine1");
        grayLine.width = "98%";
        grayLine.height = "4px";
        grayLine.background = "#C7D0D8";

        return grayLine;
    }

    private _addMainButtons(container: StackPanel) {
        const walls = Button.CreateSimpleButton("AddWallsBtn", Strings.ADD_WALLS);
        walls.height = "110px";
        walls.thickness = 0;
        
        if (walls.textBlock) {
            walls.textBlock.fontFamily = "BasisGrotesquePro";
            walls.textBlock.fontSizeInPixels = 30;
            walls.textBlock.color = "black";
        }

        //elements.isEnabled = false;

        walls.onPointerClickObservable.add(() => {
            for (const c of this._mainControls) {
                c.isVisible = false;
            }
            for (const c of this._partitionControls) {
                c.isVisible = true;
            }

            if (this._heightInput) {
                this._heightInput.onTextChangedObservable.clear();

                this._heightInput.text = "N/A";
                this._heightInput.isEnabled = false;
            }
            if (this._widthInput) {
                this._widthInput.onTextChangedObservable.clear();

                this._widthInput.text = "N/A";
                this._widthInput.isEnabled = false;
            }
            if (this._lengthInput) {
                this._lengthInput.onTextChangedObservable.clear();

                this._lengthInput.text = "N/A";
                this._lengthInput.isEnabled = false;
            }
        });
        walls.onPointerEnterObservable.add(() => {
            this._app.setCursor("pointer");
            walls.background = "#C7D0D8";
        });
        walls.onPointerOutObservable.add(() => {
            this._app.resetCursor();
            walls.background = "transparent";
        });

        container.addControl(walls);

        walls.verticalAlignment = Control.VERTICAL_ALIGNMENT_CENTER;
        walls.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_CENTER;

        const grayLine = this._getSeparator();

        const elements = Button.CreateSimpleButton("AddElementsBtn", Strings.ADD_ELEMENTS);
        elements.height = "70px";
        elements.thickness = 0;

        if (elements.textBlock) {
            elements.textBlock.fontFamily = "BasisGrotesquePro";
            elements.textBlock.fontSizeInPixels = 30;
            elements.textBlock.color = "black";
        }

        elements.onPointerClickObservable.add(() => {
            const gui = this._app.getSceneGUI();
            (gui as Editing2dGui).showElements();
        });
        elements.onPointerEnterObservable.add(() => {
            this._app.setCursor("pointer");
            elements.background = "#C7D0D8";

            grayLine.isVisible = false;
        });
        elements.onPointerOutObservable.add(() => {
            this._app.resetCursor();
            elements.background = "transparent";

            grayLine.isVisible = true;
        });

        container.addControl(elements);
        container.addControl(grayLine);

        this._mainControls.push(walls);
        this._mainControls.push(elements);
        this._mainControls.push(grayLine);
    }

    public goToWallPanel(wall: PlaceableWall, events: boolean = true) {
        for (const c of this._mainControls) {
            c.isVisible = false;
        }
        for (const c of this._partitionControls) {
            c.isVisible = true;
        }

        if (this._heightInput) {
            this._heightInput.onTextChangedObservable.clear();
            this._heightInput.text = (wall.HeightInUnits * 1000).toFixed(0);
            this._heightInput.isEnabled = true;

            if (events) 
                this._heightInput.onTextChangedObservable.add(() => {
                    let h = parseInt(this._heightInput!.text) / 1000;
                    h = UnitCalculator.clamp(h, Constants.MIN_WALL_SIZE, Constants.MAX_WALL_SIZE);

                    wall.HeightInUnits = h;
                    //this._heightInput!.text = (h * 1000).toFixed(0);

                    const scene = this._app.getScene();
                    if (scene)
                        wall.render2D(scene);
                });
        }
        if (this._widthInput) {
            const width = wall.StartingOrientation == WallOrientation.HORIZONTAL ? wall.LengthInUnits : wall.WidthInUnits;

            this._widthInput.onTextChangedObservable.clear();
            this._widthInput.text = (width * 1000).toFixed(0);
            this._widthInput.isEnabled = true;

            if (events)
                this._widthInput.onTextChangedObservable.add(() => {
                    let w = parseInt(this._widthInput!.text) / 1000;
                    w = UnitCalculator.clamp(w, Constants.MIN_WALL_SIZE, Constants.MAX_WALL_SIZE);

                    if (wall.StartingOrientation == WallOrientation.HORIZONTAL) {
                        wall.LengthInUnits = w;
                    } else {
                        wall.WidthInUnits = w;
                    }

                    //this._widthInput!.text = (w * 1000).toFixed(0);

                    const scene = this._app.getScene();
                    if (scene)
                        wall.render2D(scene);
                });
        }
        if (this._lengthInput) {
            const length = wall.StartingOrientation == WallOrientation.HORIZONTAL ? wall.WidthInUnits : wall.LengthInUnits;

            this._lengthInput.onTextChangedObservable.clear();
            this._lengthInput.text = (length * 1000).toFixed(0);
            this._lengthInput.isEnabled = true;

            if (events)
                this._lengthInput.onTextChangedObservable.add(() => {
                    let l = parseInt(this._lengthInput!.text) / 1000;
                    l = UnitCalculator.clamp(l, Constants.MIN_WALL_SIZE, Constants.MAX_WALL_SIZE);

                    if (wall.StartingOrientation == WallOrientation.HORIZONTAL) {
                        wall.WidthInUnits = l;
                    } else {
                        wall.LengthInUnits = l;
                    }

                    //this._lengthInput!.text = (l * 1000).toFixed(0);

                    const scene = this._app.getScene();
                    if (scene)
                        wall.render2D(scene);
                });
        }
    }

    public goToMainPanel() {
        for (const c of this._partitionControls) {
            c.isVisible = false;
        }
        for (const c of this._mainControls) {
            c.isVisible = true;
        }
    }

    public enable() {
        for (const e of this._partitionControls) {
            e.isVisible = false;
        }
           
        for (const e of this._mainControls) {
            e.isVisible = true;

            const btn = e as Button;
            if (!btn) continue;

            btn.isEnabled = true;
            if (btn.textBlock)
                btn.textBlock.color = "black";

            btn.background = "transparent";
        }
    }

    public disable() {
        for (const e of this._partitionControls) {
            e.isVisible = false;
        }
           
        for (const e of this._mainControls) {
            e.isVisible = true;

            const btn = e as Button;
            if (!btn) continue;

            btn.isEnabled = false;
            if (btn.textBlock)
                btn.textBlock.color = "#C7D0D8";

            btn.background = "transparent";
        }
    }
}