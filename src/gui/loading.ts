import { ILoadingScreen } from "@babylonjs/core";

export class FoundPopLoadingScreen implements ILoadingScreen {
    public loadingUIBackgroundColor: string = "white";
    public loadingUIText: string = "";

    private _div: HTMLDivElement;

    constructor() { 
        this._div = document.getElementById("loadingScreen") as HTMLDivElement;
    }

    public displayLoadingUI() {
        this._div.classList.remove("fadeOut");
        this._div.classList.add("fadeIn");
    }

    public hideLoadingUI() {
        this._div.classList.remove("fadeIn");
        this._div.classList.add("fadeOut");
    }
}