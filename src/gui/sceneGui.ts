import { Control, Container } from "@babylonjs/gui";
import {SceneBase} from "../scenes/sceneBase";
import {FoundPopConfigurator} from "../index";

export class SceneGUI {
    protected _container: Container;

    constructor(protected _scene: SceneBase, protected _app: FoundPopConfigurator) {
        this._container = new Container("SceneGuiContainer");
        this._container.height = "100%";
        this._container.width = "20%";

        if (_scene.hasSideMenu())
            this._container.left = "25%";
        else
            this._container.left = "0";
    }

    public getContainer() : Control {
        return this._container;
    }

    public onUpdate() {

    }

    protected resetCursor() {
        document.body.style.cursor = "";
    }

    protected setCursor(type: string) {
        document.body.style.cursor = type;
    }
}