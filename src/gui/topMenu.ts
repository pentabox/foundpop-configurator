import { AdvancedDynamicTexture, Button, Container, Rectangle, TextBlock } from "@babylonjs/gui";
import { FoundPopConfigurator } from "..";
import { Room, RoomData } from "../models/room";
import { Editing2dScene } from "../scenes/editing2d/editing2dScene";
import { States } from "../scenes/sceneBase";
import { Endpoints } from "../utils/constants";
import { Strings } from "../utils/strings";
import { ButtonType, FieldValidationType, FormButtonConfig, FormFieldConfig, FormWindow } from "./formWindow";
import { ModalWindow } from "./modalWindow";
import { Buffer } from 'buffer';

export class TopMenu {
    private _topMenu: Container;
    private _planNumberText: TextBlock;

    private _saveForm: FormWindow | null = null;
    private _quoteForm: FormWindow | null = null;

    private _planSavedWindow: ModalWindow | null = null;
    private _savedPlanNumberText: TextBlock | null = null;

    private _congratulationsWindow: ModalWindow | null = null;

    private _lastJson: RoomData | undefined = undefined;

    constructor(private _guiTexture: AdvancedDynamicTexture, private _app: FoundPopConfigurator) {
        const topMenu = new Container("TopMenu");
        topMenu.width = "100%";
        topMenu.height = "9%";
        topMenu.background = "white";

        this._guiTexture.addControl(topMenu);
        topMenu.top = "0px";
        topMenu.left = "0px";
        topMenu.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;
        topMenu.verticalAlignment = Container.VERTICAL_ALIGNMENT_TOP;

        const underline = new Rectangle("Underline");
        underline.width = "100%";
        underline.height = "2px";
        underline.background = "black";
        underline.thickness = 0;

        topMenu.addControl(underline);
        underline.top = "0px";
        underline.left = "0px";
        underline.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;
        underline.verticalAlignment = Container.VERTICAL_ALIGNMENT_BOTTOM;
        
        this._topMenu = topMenu;
        topMenu.zIndex = 10;

        const planNumber = new TextBlock("PlanNumberText", "");
        planNumber.fontSize = 25;
        planNumber.fontFamily = "BasisGrotesquePro";
        planNumber.color = "black";
        planNumber.height = "100%";
        planNumber.width = "30%";
        planNumber.left = "15%";

        this._planNumberText = planNumber;
        this._topMenu.addControl(planNumber);
        planNumber.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;
        planNumber.textHorizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;

        this._setupButtons();

        setTimeout(() => {
            this._postInit();
        }, 100);
    }

    public setRoomData(room: RoomData) {
        this._lastJson = room;
    }

    private async _saveRoom(values: Map<string, string>) {
        let room = this._app.CreatedRoom!.toJson();
        room.Contact = {
            Name: values.get("Name") ?? "",
            Company: values.get("Company") ?? "",
            Email: values.get("Email") ?? ""
        };

        this._app.CreatedRoom!.Contact = room.Contact;

        this._quoteForm?.setValue("Name", room.Contact.Name);
        this._quoteForm?.setValue("Company", room.Contact.Company);
        this._quoteForm?.setValue("Email", room.Contact.Email);

        const roomJson = {
            "Room": room
        };
        this._lastJson = room;

        let url = Endpoints.SAVE_ROOM;
        url = url.replace("{plan}", this._app.CreatedRoom!.PlanNumber);
        const auth = Buffer.from("admin:TDFDhu4POIIGDl573cYC").toString("base64");

        fetch(url, {
            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": "Basic " + auth
            },
            body: JSON.stringify(roomJson)
        })
        .then(result => result.json())
        .then(json => {
            if (json["success"]) {
                this._savedPlanNumberText!.text = Strings.PLAN_SAVED.replace("{plan}", this._app.CreatedRoom!.PlanNumber);
                this._planNumberText.text = this._app.CreatedRoom!.PlanNumber;

                const quoteBtn = this._topMenu.getChildByName("QuoteBtn") as Button;
                if (quoteBtn) {
                    quoteBtn.isEnabled = true;

                    if (quoteBtn.textBlock) {
                        quoteBtn.textBlock.color = "black";
                    }
                }

                this._planSavedWindow?.show();
            } else {
                console.log(json["error"]);
                // TODO error window
            }

            this._saveForm?.hide();
        })
        .catch(ex => {
            console.log(ex);
            //TODO error window
        });
    }

    private _postInit() {
        this._setupPlanSavedWindow();
        this._setupCongratulationsWindow();

        if (this._app.CreatedRoom?.Completed) {
            this._planNumberText.text = this._app.CreatedRoom.PlanNumber;
        }

        this._saveForm = new FormWindow(this._app, this._guiTexture, {
            height: "40%",
            width: "650px",
            spacing: "20px",
            
            buttons: [
                new FormButtonConfig(
                    ButtonType.CENTER,
                    Strings.SAVE,
                    (values: Map<string, string>) => {
                        this._saveRoom(values);
                    },
                    true
                )
            ],
            fields: [
                new FormFieldConfig(
                    Strings.NAME + ": ",
                    "Name",
                    "",
                    "25%",
                    "80%",
                    "white",
                    "BasisGrotesquePro",
                    30,
                    FieldValidationType.NOTEMPTY
                ),
                new FormFieldConfig(
                    Strings.COMPANY + ": ",
                    "Company",
                    "",
                    "35%",
                    "70%",
                    "white",
                    "BasisGrotesquePro",
                    30,
                    FieldValidationType.NOTEMPTY
                ),
                new FormFieldConfig(
                    Strings.EMAIL + ": ",
                    "Email",
                    "",
                    "50%",
                    "55%",
                    "white",
                    "BasisGrotesquePro",
                    30,
                    FieldValidationType.EMAIL
                )
            ]
        });
        this._saveForm.setContentTop("30px");
        this._saveForm.hide();
        
        this._quoteForm = new FormWindow(this._app, this._guiTexture, {
            height: "80%",
            width: "650px",
            spacing: "15px",

            buttons: [
                new FormButtonConfig(
                    ButtonType.CENTER,
                    Strings.REQUEST_QUOTE,
                    (values: Map<string, string>) => {
                        this._onQuoteRequested(values);
                    },
                    true
                )
            ],
            fields: [
                new FormFieldConfig(
                    Strings.NAME + ": ",
                    "Name",
                    "",
                    "25%",
                    "80%",
                    "white",
                    "BasisGrotesquePro",
                    30,
                    FieldValidationType.NOTEMPTY
                ),
                new FormFieldConfig(
                    Strings.EMAIL + ": ",
                    "Email",
                    "",
                    "50%",
                    "55%",
                    "white",
                    "BasisGrotesquePro",
                    30,
                    FieldValidationType.EMAIL
                ),
                new FormFieldConfig(
                    Strings.COMPANY + ": ",
                    "Company",
                    "",
                    "35%",
                    "70%",
                    "white",
                    "BasisGrotesquePro",
                    30,
                    FieldValidationType.NOTEMPTY
                ),
                new FormFieldConfig(
                    Strings.PHONE + ": ",
                    "Phone",
                    "",
                    "50%",
                    "55%"
                ),
                new FormFieldConfig(
                    Strings.LOCATION + ": ",
                    "Location",
                    "",
                    "33%",
                    "70%"
                ),
                new FormFieldConfig(
                    Strings.RENTAL_START + ": ",
                    "RentalStart",
                    "",
                    "59%",
                    "45%"
                ),
                new FormFieldConfig(
                    Strings.RENTAL_END + ": ",
                    "RentalEnd",
                    "",
                    "55%",
                    "50%"
                ),
                new FormFieldConfig(
                    Strings.NOTES + ": ",
                    "Notes",
                    "",
                    "52%",
                    "52%"
                )
            ]
        });
        this._quoteForm.hide();
    }

    private _onQuoteRequested(values: Map<string, string>) {
        if (!this._lastJson)
            this._lastJson = this._app.CreatedRoom?.toJson();

        this._lastJson!.Quote = {
            Name: values.get("Name")!,
            Company: values.get("Company")!,
            Email: values.get("Email")!,
            Notes: values.get("Notes") ?? "",
            Phone: values.get("Phone") ?? "",
            RentalStart: values.get("RentalStart")!,
            RentalEnd: values.get("RentalEnd")!,
            Location: values.get("Location")!
        };

        this._app.CreatedRoom!.Quote = this._lastJson?.Quote;

        const roomJson = {
            "Room": this._lastJson,
            "Completed": true
        };

        let url = Endpoints.REQUEST_QUOTE;
        url = url.replace("{plan}", this._app.CreatedRoom!.PlanNumber);
        const auth = Buffer.from("admin:TDFDhu4POIIGDl573cYC").toString("base64");

        fetch(url, {
            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": "Basic " + auth
            },
            body: JSON.stringify(roomJson)
        })
        .then(result => result.json())
        .then(json => {
            if (json["success"]) {
                this._quoteForm?.hide();
                this._congratulationsWindow?.show();
            } else {
                console.log(json["error"]);
                // TODO error window
            }
        })
        .catch(ex => {
            console.log(ex);
            //TODO error window
        });
    }

    public onRoomSaved(room: Room) {
        if (!this._savedPlanNumberText) return;

        this._savedPlanNumberText.text = Strings.PLAN_SAVED.replace("{plan}", room.PlanNumber);
        this._planSavedWindow?.show();

        this._app.CreatedRoom!.Completed = true;
    }

    private _setupPlanSavedWindow() {
        this._topMenu.getChildByName("QuoteBtn")!.isEnabled = true;

        const content = new Container("PlanSavedContent");
        content.width = "80%";
        content.height = "100%";
        content.background = "transparent";

        const planText = new TextBlock("PlanSavedText", Strings.PLAN_SAVED);
        planText.color = "white";
        planText.width = "100%";
        planText.height = "100%";
        planText.textWrapping = true;
        planText.fontFamily = "BasisGrotesquePro";
        planText.fontSizeInPixels = 35;

        content.addControl(planText);
        this._savedPlanNumberText = planText;

        this._planSavedWindow = new ModalWindow(content, this._app, this._guiTexture, "35%", "25%");
        const lb = this._planSavedWindow.addLeftButton(Strings.DOWNLOAD_PLAN, () => {
            this._app.createRoomPDF();
        });
        lb.width = "325px";

        const rb = this._planSavedWindow.addRightButton(Strings.SUBMIT_QUOTE, () => {
            this._planSavedWindow?.hide();

            let room = this._app.CreatedRoom!.toJson();
            if (room.Contact)
            {
                this._quoteForm?.setValue("Name", room.Contact.Name);
                this._quoteForm?.setValue("Company", room.Contact.Company);
                this._quoteForm?.setValue("Email", room.Contact.Email);
            }

            this._quoteForm?.show();
        });
        rb.width = "325px";

        this._planSavedWindow.setButtonContainerTop("16.3%");
        this._planSavedWindow.hide();
    }

    private _setupCongratulationsWindow() {
        const content = new Container("CongratulationsContent");
        content.width = "80%";
        content.height = "100%";
        content.background = "transparent";

        const planText = new TextBlock("CongratulationsText", Strings.CONGRATULATIONS);
        planText.color = "white";
        planText.width = "100%";
        planText.height = "100%";
        planText.textWrapping = true;
        planText.fontFamily = "BasisGrotesquePro";
        planText.fontSizeInPixels = 35;

        content.addControl(planText);

        this._congratulationsWindow = new ModalWindow(content, this._app, this._guiTexture, "45%", "30%");
        this._congratulationsWindow.hide();
    }

    private _setupButtons() {
        const confPlan = Button.CreateSimpleButton("ConfPlanBtn", Strings.CONFIGURE_PLAN);
        this._setupButton(confPlan);

        this._topMenu.addControl(confPlan);
        confPlan.verticalAlignment = Container.VERTICAL_ALIGNMENT_CENTER;
        confPlan.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;
        confPlan.left = "35%";
        confPlan.width = "20%";
        //confPlan.thickness = 3;

        confPlan.onPointerClickObservable.add(() => {
            this._app.State = States.EDITING_2D;
            (this._app.getScene() as Editing2dScene).to2d();
        });

        const visual = Button.CreateSimpleButton("Visual3DBtn", Strings.VISUAL_3D);
        this._setupButton(visual);

        this._topMenu.addControl(visual);
        visual.verticalAlignment = Container.VERTICAL_ALIGNMENT_CENTER;
        visual.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_LEFT;
        visual.left = "57%";
        visual.width = "20%";

        visual.onPointerClickObservable.add(() => {
            this._app.State = States.VIEW_3D;
            (this._app.getScene() as Editing2dScene).to3d();
        });

        const save = Button.CreateSimpleButton("SaveBtn", Strings.SAVE);
        this._setupButton(save);

        this._topMenu.addControl(save);
        save.verticalAlignment = Container.VERTICAL_ALIGNMENT_CENTER;
        save.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_RIGHT;
        save.left = "-9%";
        save.width = "5%";

        save.onPointerClickObservable.add(() => {
            let room = this._app.CreatedRoom!.toJson();
            if (room.Contact)
            {
                this._saveForm?.setValue("Name", room.Contact.Name);
                this._saveForm?.setValue("Email", room.Contact.Email);
                this._saveForm?.setValue("Company", room.Contact.Company);
            }

            this._app.CreatedRoom?.clearSelection();
            this._saveForm?.show();
        });

        const quote = Button.CreateSimpleButton("QuoteBtn", Strings.QUOTE);
        this._setupButton(quote, "#C7D0D8");

        quote.onPointerClickObservable.add(() => {
            if (!this._app.CreatedRoom?.Completed)
                return;

            this._quoteForm?.show();
        });

        this._topMenu.addControl(quote);
        quote.verticalAlignment = Container.VERTICAL_ALIGNMENT_CENTER;
        quote.horizontalAlignment = Container.HORIZONTAL_ALIGNMENT_RIGHT;
        quote.left = "-0.5%";
        quote.width = "10%";
        quote.isEnabled = false;

        if (this._app.CreatedRoom?.Completed) {
            quote.isEnabled = true;

            if (quote.textBlock) {
                quote.textBlock.color = "black";
            }
        }
    }

    private _setupButton(btn: Button, colorOverride: string = "") {
        btn.thickness = 0;

        if (btn.textBlock) {
            btn.textBlock.color = colorOverride !== "" ? colorOverride : "black";
            btn.textBlock.fontFamily = "BasisGrotesquePro";
            btn.textBlock.fontSizeInPixels = 33;
        }

        btn.onPointerEnterObservable.add(() => {
            this._app.setCursor("pointer");
        });
        btn.onPointerOutObservable.add(() => {
            this._app.resetCursor();
        });
    }
}