import { Button, Container, Control, Ellipse, Image, Rectangle, TextBlock } from "@babylonjs/gui";
import { TextBlockPropertyGridComponent } from "@babylonjs/inspector/components/actionTabs/tabs/propertyGrids/gui/textBlockPropertyGridComponent";
import { FoundPopConfigurator } from "..";

export interface CircleButtonOptions {
    width?: string;
    height?: string;
    background?: string;
    color?: string;
    thickness?: number;
    image: string;
    imageWidth?: string;
    imageHeight?: string;
    scaleOnHover?: number;

    text?: string;
    fontFamily?: string;
    fontSize?: number;
    textTop?: string;
    textColor?: string;
}

export class CircleButton {
    private _circle: Ellipse;
    private _rectangle: Rectangle;
    private _image: Image;
    private _button: Button;
    private _text: TextBlock | null = null;

    constructor(private _name: string,
                private _parent: Container, 
                private _app: FoundPopConfigurator,
                private _options: CircleButtonOptions) {
        this._options = this._getOptions(_options);

        const circle = new Ellipse(_name + "Circle");
        circle.width = this._options.width!;
        circle.height = this._options.height!;
        circle.background = this._options.background!;
        circle.color = this._options.color!;
        circle.thickness = this._options.thickness!;

        this._circle = circle;

        const rectangle = new Rectangle(_name + "Parent");
        rectangle.width = this._options.width!;
        rectangle.height = this._options.height!;
        rectangle.thickness = 0;
        rectangle.background = "transparent";
        rectangle.isPointerBlocker = false;

        this._rectangle = rectangle;

        const image = new Image(_name + "Image", this._options.image);
        image.width = this._options.imageWidth!;
        image.height = this._options.imageHeight!;
        image.isPointerBlocker = false;

        this._image = image;

        const btn = Button.CreateSimpleButton(_name + "Btn", "");
        btn.width = this._options.width!;
        btn.height = this._options.height!;
        btn.background = "transparent";
        btn.color = "transparent";
        btn.zIndex = 15;

        if (this._options.text) {
            const text = new TextBlock(_name + "Text", this._options.text!);
            text.fontFamily = this._options.fontFamily!;
            text.fontSizeInPixels = this._options.fontSize!;
            text.top = this._options.textTop!;
            text.color = this._options.textColor!;
            text.width = "100%";
            text.height = (this._options.fontSize! + 15) + "px";

            this._text = text;
        }
        
        btn.onPointerEnterObservable.add(() => {
            this._app.setCursor("pointer");
        });
        btn.onPointerOutObservable.add(() => {
            this._app.resetCursor();
        });
        btn.onPointerDownObservable.add(() => {
            image.scaleX = this._options.scaleOnHover!;
            image.scaleY = this._options.scaleOnHover!;

            circle.scaleX = this._options.scaleOnHover!;
            circle.scaleY = this._options.scaleOnHover!;
        });
        btn.onPointerUpObservable.add(() => {
            image.scaleX = 1;
            image.scaleY = 1;

            circle.scaleX = 1;
            circle.scaleY = 1;
        });

        this._button = btn;

        rectangle.addControl(image);

        _parent.addControl(circle);
        _parent.addControl(rectangle);
        _parent.addControl(btn);

        if (this._text)
            _parent.addControl(this._text);
    }

    public reset() {
        this._rectangle.left = "0px";
        this._button.left = "0px";
        this._circle.left = "0px";

        if (this._text)
            this._text.left = "0px";
    }

    private _getOptions(options: CircleButtonOptions): CircleButtonOptions {
        if (!options.width)
            options.width = "55px";
        if (!options.height)
            options.height = "55px";
        if (!options.background)
            options.background = "white";
        if (!options.color)
            options.color = "black";
        if (!options.thickness)
            options.thickness = 2;
        if (!options.scaleOnHover)
            options.scaleOnHover = 0.9;
        if (!options.imageWidth)
            options.imageWidth = "40px";
        if (!options.imageHeight)
            options.imageHeight = "40px";
        if (!options.fontFamily)
            options.fontFamily = "BasisGrotesquePro";
        if (!options.fontSize)
            options.fontSize = 12;
        if (!options.textTop)
            options.textTop = "0px";
        if (!options.textColor)
            options.textColor = "black";

        return options;
    }

    public setTop(top: string) {
        this._circle.top = top;
        this._rectangle.top = top;
        this._button.top = top;
    }

    public setLeft(left: string) {
        this._circle.left = left;
        this._rectangle.left = left;
        this._button.left = left;

        if (this._text)
            this._text.left = left;
    }

    public setAlignment(horizontal: number, vertical: number) {
        this._circle.horizontalAlignment = horizontal;
        this._circle.verticalAlignment = vertical;

        this._rectangle.horizontalAlignment = horizontal;
        this._rectangle.verticalAlignment = vertical;

        this._button.horizontalAlignment = horizontal;
        this._button.verticalAlignment = vertical;

        if (this._text)
        {
            this._text.horizontalAlignment = horizontal;
            this._text.verticalAlignment = vertical;
        }
    }

    public getButton() : Button {
        return this._button;
    }

    public getText(): TextBlock | null {
        return this._text;
    }
}